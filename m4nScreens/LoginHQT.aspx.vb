Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class LoginHQT
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            DO1.Value("MHEID") = ""
            'Set the MHType drop down
            Dim dd As Made4Net.WebControls.MobileDropDown
            dd = DO1.Ctrl("MHType")
            dd.AllOption = False
            dd.TableName = "HANDLINGEQUIPMENT"
            dd.ValueField = "HANDLINGEQUIPMENT"
            dd.TextField = "HANDLINGEQUIPMENT"
            dd.DataBind()
            'Set the Terminal Type drop down
            Dim dd1 As Made4Net.WebControls.MobileDropDown
            dd1 = DO1.Ctrl("TEMINALTYPE")
            dd1.AllOption = False
            dd1.TableName = "CODELISTDETAIL"
            dd1.ValueField = "CODE"
            dd1.TextField = "DESCRIPTION"
            dd1.Where = "CODELISTCODE = 'TERMINALTYPE'"
            dd1.DataBind()
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("MHEID")
        DO1.AddDropDown("MHType")
        DO1.AddDropDown("TEMINALTYPE")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        If e.CommandText.ToLower = "mhtypeselect" Then
            Dim oWHActivity As New WMS.Logic.WHActivity
            oWHActivity.ACTIVITY = "Login"
            oWHActivity.LOCATION = Session("LoginLocation")
            oWHActivity.USERID = Logic.GetCurrentUser
            ' Check if the MHEID is scaned and get the Type from MHE table
            If DO1.Value("MHEID") = "" Then
                oWHActivity.HETYPE = DO1.Value("MHType")
            Else
                If DataInterface.ExecuteScalar("SELECT COUNT(1) FROM MHE WHERE MHEID='" & DO1.Value("MHEID") & "'") > 0 Then
                    oWHActivity.HETYPE = DataInterface.ExecuteScalar("SELECT ISNULL(MHETYPE,'') AS MHETYPE FROM MHE WHERE MHEID='" & DO1.Value("MHEID") & "'")
                Else
                    oWHActivity.HETYPE = DO1.Value("MHType")
                End If
            End If

            oWHActivity.HANDLINGEQUIPMENTID = DO1.Value("MHEID")
            oWHActivity.TERMINALTYPE = DO1.Value("TEMINALTYPE")
            oWHActivity.ACTIVITYTIME = DateTime.Now
            oWHActivity.ADDDATE = DateTime.Now
            oWHActivity.EDITDATE = DateTime.Now
            oWHActivity.ADDUSER = Logic.GetCurrentUser
            oWHActivity.EDITUSER = Logic.GetCurrentUser
            Session("ActivityID") = oWHActivity.Post()
            Session("MHEID") = DO1.Value("MHEID")
            Session("MHType") = DO1.Value("MHType")
            Session("MHEIDLabelPrinter") = WMS.MobileWebApp.MobileUtils.GetMHEDefaultPrinter(DO1.Value("MHEID"))

            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/Main.aspx"))
        End If
    End Sub

End Class
