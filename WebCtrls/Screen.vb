Imports Made4Net.WebControls
Imports Made4Net.Mobile
Imports Made4Net.Mobile.WebCtrls
Imports System.ComponentModel

Namespace WebCtrls

    <CLSCompliant(False)> Public Class Screen
        Inherits WMS.WebCtrls.WebCtrls.Screen

        Protected _ShowMainMenu As Boolean
        Protected _ShowScreenMenu As Boolean

        Public Property ShowMainMenu() As Boolean
            Get
                Return _ShowMainMenu
            End Get
            Set(ByVal Value As Boolean)
                _ShowMainMenu = Value
            End Set
        End Property

        Public Property ShowScreenMenu() As Boolean
            Get
                Return _ShowScreenMenu
            End Get
            Set(ByVal Value As Boolean)
                _ShowScreenMenu = Value
            End Set
        End Property

        Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
            EnforeProperties()
            MyBase.OnInit(e)
        End Sub

        Protected Sub EnforeProperties()
            Me.HideBanner = True
            Me.HideMenu = True
            Me.EnableKeyboardManager = False
            Me.EnableRadAjax = False
            Me.EnableRadAjaxLoadingPanel = False
            Me.EnableRadAjaxManager = False
            Me.EnableRadScriptManager = False
            Me.EnableRadWindowManager = False
            Me.EnableDummyValidator = False
        End Sub

        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
            MyBase.OnLoad(e)
            EnforeProperties()
        End Sub
        Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
            Dim disableAjaxScripting As String
            disableAjaxScripting = ("<script language=JavaScript>function m4nRegisterScript(scriptUrl) {}</script>")
            If (Not Page.ClientScript.IsClientScriptIncludeRegistered("m4nRegisterScriptFake")) Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "m4nRegisterScriptFake", disableAjaxScripting)
            End If
            OverrideRegisterClientScripts()
            MyBase.OnPreRender(e)
        End Sub
        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            If Made4Net.Mobile.MessageQue.Count > 0 Then
                Dim s As String
                s = String.Format("<script language=JavaScript>alert('{0}');</script>", Made4Net.Shared.Strings.PSQ(Made4Net.Mobile.MessageQue.Dequeue(), "\"))
                writer.Write(s)
            End If
        End Sub
        Protected Sub OverrideRegisterClientScripts()
            'was changed due to obsolete in .NET 2.0
            If (Not Page.ClientScript.IsClientScriptIncludeRegistered("m4nCommon")) Then
                'Page.ClientScript.RegisterClientScriptInclude("m4nCommon", "m4nClientScripts/js.js")
                Page.ClientScript.RegisterClientScriptInclude("m4nCommon", Made4Net.Shared.Web.MapVirtualPath(System.IO.Path.Combine(Made4Net.Shared.Web.GetScriptDirectory(), "m4nCommon.js")).Replace("\", "/"))
            End If
            If (Not Page.ClientScript.IsClientScriptIncludeRegistered("m4nDisplayType")) Then
                'Page.ClientScript.RegisterClientScriptInclude("m4nDisplayType", "m4nClientScripts/js.js")
                Page.ClientScript.RegisterClientScriptInclude("m4nDisplayType", Made4Net.Shared.Web.MapVirtualPath(System.IO.Path.Combine(Made4Net.Shared.Web.GetScriptDirectory(), "m4nCommon.js")).Replace("\", "/"))
            End If
            If (Not Page.ClientScript.IsClientScriptIncludeRegistered("m4nDisplayTypeInstanceManager")) Then
                'Page.ClientScript.RegisterClientScriptInclude("m4nDisplayTypeInstanceManager", "m4nClientScripts/js.js")
                Page.ClientScript.RegisterClientScriptInclude("m4nDisplayTypeInstanceManager", Made4Net.Shared.Web.MapVirtualPath(System.IO.Path.Combine(Made4Net.Shared.Web.GetScriptDirectory(), "m4nCommon.js")).Replace("\", "/"))
            End If
            If (Not Page.ClientScript.IsClientScriptIncludeRegistered("m4nDisplayTypeInformation")) Then
                'Page.ClientScript.RegisterClientScriptInclude("m4nDisplayTypeInformation", "m4nClientScripts/js.js")
                Page.ClientScript.RegisterClientScriptInclude("m4nDisplayTypeInformation", Made4Net.Shared.Web.MapVirtualPath(System.IO.Path.Combine(Made4Net.Shared.Web.GetScriptDirectory(), "m4nCommon.js")).Replace("\", "/"))
            End If
            'If (Not Page.ClientScript.IsClientScriptBlockRegistered("radWinCallback")) Then
            '    Page.ClientScript.RegisterClientScriptBlock(Page."radWinCallback", "")
            'End If
            'Telerik.Web.UI.RadScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "radWinCallback", radWinCallback, True)
        End Sub



    End Class

End Namespace
