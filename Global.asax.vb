Imports System.Web
Imports System.Web.SessionState

<CLSCompliant(False)> Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        '''''''''''''''''''''''''''''''
        ' Handles the license manager '
        '''''''''''''''''''''''''''''''
        Dim appId As String = System.Configuration.ConfigurationManager.AppSettings().Get("Made4NetLicensing_ApplicationId")
        'Dim SQL As String = "select param_value from sys_param where param_code = 'ApplicationId'"
        'Dim appId As String = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL, "Made4NetSchema")
        Application("Made4NetLicensing_ApplicationId") = appId
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        '' Fires when an error occurs
        If TypeOf Server.GetLastError.InnerException Is Made4Net.DataAccess.InvalidLicenseException Then
            Context.ClearError()
            Made4Net.Mobile.MessageQue.Enqueue("No Licence found!")
            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx"))
        Else

        End If
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        'remove the current user + session from license server
        Dim userid, appId, sessionid, ipAddress, conn As String
        appId = Application("Made4NetLicensing_ApplicationId")
        userid = Me.Session().Item("Made4NetLoggedInUserName")
        sessionid = Me.Session().SessionID
        ipAddress = Me.Session().Item("Made4NetLoggedInUserAddress")
        conn = Made4Net.DataAccess.DataInterface.ConnectionName

        WMS.Logic.WHActivity.Delete(userid)

        Dim Session_Id As String = ipAddress & "_" & sessionid
        Dim key As String = "DisConnect" & "@" & userid & "@" & Session_Id & "@" & appId & "@" & conn
        Dim SQL As String = "select param_value from sys_param where param_code = 'LicenseServer'"
        Dim server As String = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL, "Made4NetSchema")
        SQL = "select param_value from sys_param where param_code = 'LicenseServerPort'"
        Dim port As Int32 = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL, "Made4NetSchema")
        Dim tcpClient As New Made4Net.Net.TCPIP.Client(server, port)
        Dim ret As Boolean = Convert.ToBoolean(tcpClient.SendRequest(key))

        'close all connections to the DB and release memory
        Try
            Made4Net.DataAccess.DataInterface.CloseAllConnections(Me.Session)
        Catch ex As Exception
        End Try
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub


End Class
