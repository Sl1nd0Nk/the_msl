﻿<%@ Register TagPrefix="cc1" Namespace="WMS.MobileWebApp.WebCtrls" Assembly="WMS.MobileWebApp" %>
<%@ Register TagPrefix="cc2" Namespace="Made4Net.Mobile.WebCtrls" Assembly="Made4Net.Mobile" %>
<%@ Register TagPrefix="cc3" Namespace="Made4Net.WebControls" Assembly="Made4Net.WebControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EmptyScreen.aspx.vb" Inherits="WMS.MobileWebApp.EmptyScreen" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
    <script type="text/javascript">
        <link href="../Content/bootstrap.css" rel="stylesheet" />
    </script>
	<HEAD>
		<title>CLD1</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="../Content/bootstrap.css" rel="stylesheet" />
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<div align="center" ><cc1:screen id="Screen1" runat="server" title="Empty Screen" ScreenID="emptyscreen" Visible="True" EnableTheming="False"></cc1:screen></div>
            <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <asp:LinkButton id="btnLogout" Text="Logout" OnClick="Logout_Click" runat="server" CssClass="nav-link" Font-Size="Small"/>
                </li>
              </ul>
            </nav>            
            <div align="center" ><br />
                <asp:Label ID="PackStn" runat="server" Text="Pack Station 1" Font-Size="XX-Large" ForeColor="#FF3300"></asp:Label>
                <br /><br />
                <asp:panel runat="server" Width="70%">
                    <br />
                    <asp:panel runat="server" ID="ScannedTotePanel" Visible="False">
                        <asp:Label ID="lblScannedTote" runat="server" Text="SCANNED TOTE ID:" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699" Font-Bold="True"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Label ID="ScannedTote" runat="server" Text="0000101" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699" Font-Bold="True"></asp:Label>
                        <br /><br />
                    </asp:panel>
                    <asp:panel runat="server" ID="AttribPanel" Visible="False">
                    </asp:panel>
                    <asp:panel runat="server" ID="InputPanel" Visible="False">
                        <asp:TextBox ID="Input" runat="server" Visible="True" AutoPostBack="True"></asp:TextBox>
                        <br />
                        <asp:Label ID="lblInput" runat="server" Font-Names="Calibri" Font-Size="Large" ForeColor="#000099"></asp:Label>
                    </asp:panel>
                    <br />
                </asp:panel>
                <br /><br />
                <asp:panel runat="server" ID="DetailPanel" CssClass="container">
                    <asp:panel runat="server" ID="CurrentOrderPanel" CssClass="container" Visible="False">
                        <asp:Label ID="lblCurrentOrder" runat="server" Text="ORDER ID:" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699" Font-Bold="True"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Label ID="CurrentOrder" runat="server" Text="Ord123" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699" Font-Bold="True"></asp:Label>
                    </asp:panel>
                    <br />
                    <div align="center" ><asp:PlaceHolder ID="ContentPH" runat="server"></asp:PlaceHolder></div>  
                    <br /><br />
                    <asp:panel runat="server" ID="FooterPanel" CssClass="container"> 
                        <div align="right" ><asp:Button ID="BtnAbort" runat="server" Text="Abort Process" Visible="False" class="btn btn-primary" onclick="Abort_Click" UseSubmitBehavior="False"/></div>
                    </asp:panel>
                </asp:panel>
            </div>
		</form>
	</body>
</HTML>