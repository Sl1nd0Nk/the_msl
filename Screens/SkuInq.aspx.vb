Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class SkuInq
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("SELECTEDSKU") <> "" Then
            DO1.Value("SKU") = Session("SELECTEDSKU")
            ' Add all controls to session for restoring them when we back from that sreen

            DO1.Value("CONSIGNEE") = Session("SELECTEDCONSIGNEE") 'Session("SKUSEL_CONSIGNEE")

            'Session.Remove("SKUSEL_CONSIGNEE")
            Session.Remove("SELECTEDCONSIGNEE")
            Session.Remove("SELECTEDSKU")
            doNext()
        End If
    End Sub

    Private Sub doMenu()
        Session.Remove("SKUInqConsingee")
        Session.Remove("SKUInqSKU")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doNext()

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If String.IsNullOrEmpty(DO1.Value("SKU")) Then
            MessageQue.Enqueue(t.Translate("Can not leave SKU field empty"))
            Return
        End If

        ' Check for sku
        Dim sql As String = String.Format("SELECT DISTINCT CONSIGNEE,SKU FROM vSKUCODE where (SKUCODE LIKE '{0}' OR SKU LIKE '{0}') AND CONSIGNEE LIKE '{1}%'", _
        DO1.Value("SKU"), DO1.Value("CONSIGNEE"))
        Dim dt As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
        If dt.Rows.Count = 0 Then
            MessageQue.Enqueue(t.Translate("SKU not found"))
            Return
        End If
        If dt.Rows.Count > 1 Then
            redirectToMultiSelectForm(DO1.Value("CONSIGNEE"), DO1.Value("SKU"))
        Else
            Session("SKUInqConsingee") = dt.Rows(0)("CONSIGNEE")
            Session("SKUInqSKU") = dt.Rows(0)("SKU")
            Response.Redirect(MapVirtualPath("Screens/SKUInq2.aspx"))
        End If

        'If DataInterface.ExecuteScalar("SELECT COUNT(DISTINCT (CONSIGNEE + SKU)) FROM vSKUCODE WHERE (SKUCODE LIKE '" & DO1.Value("SKU") & "' OR SKU LIKE '" & DO1.Value("SKU") & "') AND CONSIGNEE LIKE '" & DO1.Value("CONSIGNEE") & "%'") > 1 Then
        '    ' Go to Sku select screen
        '    Session("FROMSCREEN") = "SkuInq"
        '    Session("SKUCODE") = DO1.Value("SKU")
        '    ' Add all controls to session for restoring them when we back from that sreen
        '    Session("SKUSEL_CONSIGNEE") = DO1.Value("CONSIGNEE")
        '    Response.Redirect(MapVirtualPath("Screens/MultiSelectForm.aspx")) ' Changed
        'ElseIf DataInterface.ExecuteScalar("SELECT COUNT(DISTINCT SKU) FROM vSKUCODE WHERE SKUCODE LIKE '" & DO1.Value("SKU") & "'") = 1 Then
        '    DO1.Value("SKU") = DataInterface.ExecuteScalar("SELECT SKU FROM vSKUCODE WHERE SKUCODE LIKE '" & DO1.Value("SKU") & "'")
        'End If

        'Try
        '    dt = New DataTable
        '    Made4Net.DataAccess.DataInterface.FillDataset(String.Format("Select top 1 * from sku where sku = '{0}' and consignee like '{1}%'", DO1.Value("Sku"), DO1.Value("Consignee")), dt)
        '    If dt.Rows.Count = 0 Then
        '        Throw New ApplicationException("Sku not found")
        '    End If
        '    Session("SKUInqConsingee") = dt.Rows(0)("CONSIGNEE")
        '    Session("SKUInqSKU") = dt.Rows(0)("SKU")
        'Catch ex As System.Threading.ThreadAbortException
        'Catch m4nEx As Made4Net.Shared.M4NException
        '    MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        '    Return
        'Catch ex As Exception
        '    MessageQue.Enqueue(t.Translate(ex.Message))
        '    Return
        'End Try
        'Response.Redirect(MapVirtualPath("Screens/SKUInq2.aspx"))
        ''End If
    End Sub

    Private Sub redirectToMultiSelectForm(ByVal pConsignee As String, ByVal pSKU As String)
        Session("FROMSCREEN") = "SkuInq"
        Session("SKUCODE") = pSKU
        Session("SKUSEL_CONSIGNEE") = pConsignee
        Response.Redirect(MapVirtualPath("Screens/MultiSelectForm.aspx"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("Consignee")
        DO1.AddTextboxLine("SKU")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
