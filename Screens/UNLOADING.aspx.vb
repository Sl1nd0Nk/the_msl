Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

Partial Public Class UNLOADING
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub doNext()
        Try
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Dim oTask As UnloadingTask = New UnloadingTask
            oTask = oTask.AssignTask(WMS.Logic.Common.GetCurrentUser)
            If oTask Is Nothing Then
                MessageQue.Enqueue("No task available")
                Made4Net.Mobile.Common.GoToMenu()
            End If
            If Not WMS.Logic.Location.Exists(DO1.Value("DOOR")) Then
                MessageQue.Enqueue("Door is not a valid location")
                Return
            End If
            If DO1.Value("DOOR") <> oTask.FROMLOCATION Then
                MessageQue.Enqueue("Door confirmation incorrect")
                Return
            End If
            oTask.EDITUSER = WMS.Logic.GetCurrentUser
            oTask.Complete()
            DO1.Value("DOOR") = ""
        Catch ex As Threading.ThreadAbortException
            'Do Nothing
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
    End Sub

    Private Sub doMenu()
        Dim oUnloadingTask As UnloadingTask = New UnloadingTask
        oUnloadingTask.ReleaseTask(WMS.Logic.Common.GetCurrentUser)
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddSpacer()
        DO1.AddTextboxLine("DOOR")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

End Class