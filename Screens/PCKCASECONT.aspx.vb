Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared

Partial Public Class PCKCASECONT
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            'f Not Request.QueryString("sourcescreen") Is Nothing Then
            'Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
            'End If

            NewProcessScanContainers()
            'ProcessScanContainers()
        End If
    End Sub

    Private Sub NewProcessScanContainers()
        Dim ContPickDetail As PicklistDetail
        Dim SourceScreen As String = Session("JOB")
        If String.IsNullOrEmpty(SourceScreen) Then
            SourceScreen = "PCKPART"
        End If

        If Not Session("CaseContScanPickList") Is Nothing And Not Session("CaseContPline") Is Nothing Then
            ContPickDetail = New PicklistDetail(Session("CaseContScanPickList"), Convert.ToInt32(Session("CaseContPline")))

            If Not ContPickDetail Is Nothing Then
                If String.IsNullOrEmpty(ContPickDetail.ToContainer) Then

                    DO1.FocusField = "CaseContainerScan"

                    If Not String.IsNullOrEmpty(ContPickDetail.Consignee) And Not String.IsNullOrEmpty(ContPickDetail.SKU) Then
                        Dim oSKU As SKU = New SKU(ContPickDetail.Consignee, ContPickDetail.SKU)
                        DO1.Value("SKU") = ContPickDetail.SKU
                        DO1.Value("SKUDESC") = oSKU.SKUSHORTDESC
                        DO1.Value("LOCATION") = ContPickDetail.FromLocation
                        DO1.Value("UOMUNITS") = ContPickDetail.UOM
                    End If

                    Dim NoOfCases As Integer = 1
                    If Not Session("NoOfCasesDisp") Is Nothing Then
                        NoOfCases = Convert.ToInt32(Session("NoOfCasesDisp"))
                    End If

                    Dim CaseNo As Integer = 1
                    If Not Session("CaseNo") Is Nothing Then
                        CaseNo = Convert.ToInt32(Session("CaseNo"))
                    End If

                    DO1.Value("CaseContainerMsg") = "Attach A Label Onto A Case"
                    DO1.Value("CaseContainerProgress") = "Scan Case Container No. " & CaseNo & " Of " & NoOfCases
                Else
                    'Try
                    '    ContPickDetail.CloseContainer(WMS.Logic.GetCurrentUser)
                    'Catch ex As Made4Net.Shared.M4NException
                    'Catch ex As Exception
                    'End Try
                    Dim pck As Picklist = New Picklist(Session("CaseContScanPickList"), True)

                    Session.Remove("MobileSourceScreen")
                    Session.Remove("CaseContScanPickList")
                    Session.Remove("CaseContPline")
                    Session.Remove("CaseContLoadCont")
                    Session("PCKPicklist") = pck

                    'Session.Remove("PCKPicklistActiveContainerID")

                    If Session("NoOfCases") Then
                        Dim NoOfCases As Integer = Convert.ToInt32(Session("NoOfCases"))
                        NoOfCases -= 1
                        If NoOfCases <= 0 Then
                            Session.Remove("NoOfCases")
                            Session.Remove("NoOfCasesDisp")
                            Session.Remove("CaseNo")
                            Session.Remove("CaseOttrib")
                        Else
                            Session("NoOfCases") = NoOfCases.ToString()
                            Session("CaseNo") = (Convert.ToInt32(Session("CaseNo")) + 1).ToString()
                        End If
                    End If
                    Response.Redirect(MapVirtualPath("screens/" & SourceScreen & ".aspx"))
                End If
            End If
        Else
            Response.Redirect(MapVirtualPath("screens/" & SourceScreen & ".aspx"))
        End If
    End Sub

    Public Sub ProcessScanContainers()

        Dim checkForCaseCont As Boolean = True

        Dim NoOfContainer As Integer = 0
        Dim ContainerNo As Integer = 0
        Dim CaseContScan As Boolean = False
        Dim FromLoc As String
        Dim SKU As String
        Dim UOM As String
        Dim Consignee As String
        If checkForCaseCont = True Then
            NoOfContainer = GetCaseContainerNumToScan(Session("CaseContScanPickList"), Session("CaseContLoadCont"), FromLoc, SKU, UOM, Consignee)

            If NoOfContainer > 0 Then
                ContainerNo = GetNextContNoToScan(Session("CaseContScanPickList"), Session("CaseContLoadCont"))
                If ContainerNo <= NoOfContainer Then
                    CaseContScan = True
                End If
            End If
        End If

        If CaseContScan = True Then
            'ToggleVisibility(False, True)
            DO1.FocusField = "CaseContainerScan"

            If Not String.IsNullOrEmpty(Consignee) And Not String.IsNullOrEmpty(SKU) Then
                Dim oSKU As SKU = New SKU(Consignee, SKU)
                DO1.Value("SKU") = SKU
                DO1.Value("SKUDESC") = oSKU.SKUSHORTDESC
                DO1.Value("LOCATION") = FromLoc
                DO1.Value("UOMUNITS") = UOM
            End If

            DO1.Value("CaseContainerMsg") = "Attach a lable onto a case and scan"
            DO1.Value("CaseContainerProgress") = "Case Container " & ContainerNo & " of " & NoOfContainer
        Else
            Dim SourceScreen As String = Session("JOB")
            If String.IsNullOrEmpty(SourceScreen) Then
                SourceScreen = "PCK"
            End If
            Session.Remove("MobileSourceScreen")
            Dim pcklist As Picklist = New Picklist(Session("CaseContScanPickList"), True)
            If Not Session("CaseContScanPickList") Is Nothing Then
                pcklist.CloseContainer(Session("CaseContLoadCont"), True, WMS.Logic.GetCurrentUser)
            End If
            Session.Remove("CaseContScanPickList")
            Session.Remove("CaseContLoadCont")
            Session.Remove("PCKPicklistActiveContainerID")
            Response.Redirect(MapVirtualPath("screens/" & SourceScreen & ".aspx"))
        End If
    End Sub

    Private Function GetCaseContainerNumToScan(ByVal pickList As String, ByVal loadContainer As String, ByRef FromLoc As String, ByRef SKU As String, ByRef UOM As String, ByRef Consignee As String) As Integer
        Dim Count As Integer = 0
        FromLoc = ""
        SKU = ""
        UOM = ""
        Consignee = ""
        Dim SQL As String = String.Format("Select * from apx_case_container_header where pickList = {0} and loadContainer = {1}", FormatField(pickList), FormatField(loadContainer))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            If Not IsDBNull(dtLines.Rows(0)("NumOfCases")) Then
                Count = dtLines.Rows(0)("NumOfCases")
            End If

            If Not IsDBNull(dtLines.Rows(0)("FromLocation")) Then
                FromLoc = dtLines.Rows(0)("FromLocation")
            End If

            If Not IsDBNull(dtLines.Rows(0)("SKU")) Then
                SKU = dtLines.Rows(0)("SKU")
            End If

            If Not IsDBNull(dtLines.Rows(0)("UOM")) Then
                UOM = dtLines.Rows(0)("UOM")
            End If

            If Not IsDBNull(dtLines.Rows(0)("Consignee")) Then
                Consignee = dtLines.Rows(0)("Consignee")
            End If
        End If

        Return Count
    End Function

    Private Function GetNextContNoToScan(ByVal pickList As String, ByVal loadContainer As String) As Integer
        Dim Count As Integer = 0
        Dim SQL As String = String.Format("Select * from apx_case_container_detail where pickList = {0} and loadContainer = {1}", FormatField(pickList), FormatField(loadContainer))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        Count = dtLines.Rows.Count + 1

        Return Count
    End Function

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doBack()
        Dim srcScreen As String
        Try
            srcScreen = Session("MobileSourceScreen")
        Catch ex As Exception

        End Try

        If srcScreen.Equals("pckpart", StringComparison.OrdinalIgnoreCase) Then
            Session("MobileSourceScreen") = "TaskManager"
            Response.Redirect(MapVirtualPath("screens/pck.aspx"))
        End If

        Session.Remove("PCKPicklist")
        Dim deltsk As DeliveryTask = Session("PCKDeliveryTask")
        If Not deltsk Is Nothing Then
            If deltsk.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                'If deltsk.ShouldPrintShipLabelOnPicking Then
                '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx"))
                'Else
                If srcScreen = "" Or srcScreen Is Nothing Then
                    Response.Redirect(MapVirtualPath("screens/Main.aspx"))
                Else
                    Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
                End If
                'End If
            ElseIf deltsk.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.AUTOMATIC Then
                'If deltsk.ShouldPrintShipLabelOnPicking Then
                '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx"))
                'Else
                Response.Redirect(MapVirtualPath("screens/TaskManager.aspx"))
                'End If
            End If
        Else
            If srcScreen = "" Or srcScreen Is Nothing Then
                Response.Redirect(MapVirtualPath("screens/Main.aspx"))
            Else
                Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
            End If
        End If
        If srcScreen = "" Or srcScreen Is Nothing Then
            'If deltsk.ShouldPrintShipLabelOnPicking Then
            '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx"))
            'Else
            Response.Redirect(MapVirtualPath("screens/Main.aspx"))
            'End If
        Else
            Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
        End If
    End Sub

    Private Function AddContainerToDB(ByVal pickList As String, ByVal loadContainer As String, ByVal caseContainer As String) As String
        Dim Result As String = ""
        Dim SqlQuery As String = String.Format("Select * from apx_case_container_detail where caseContainer = {0}", FormatField(caseContainer))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SqlQuery, dtLines)

        If dtLines.Rows.Count > 0 Then
            Result = "Scanned ContainerID is not unique"
        Else
            Dim SQL As String = String.Format("Insert into apx_case_container_detail (pickList, loadContainer, caseContainer, Packed) values ({0},{1},{2},{3})", FormatField(pickList), FormatField(loadContainer), FormatField(caseContainer), FormatField(False))
            DataInterface.RunSQL(SQL)
        End If

        Return Result
    End Function

    Private Sub doNext(Optional ByVal pDoOverride As Boolean = False)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser

        If Not Session("CaseContScanPickList") Is Nothing Then
            If Not String.IsNullOrEmpty(DO1.Value("CaseContainerScan")) Then
                'Dim Result As String = AddContainerToDB(Session("CaseContScanPickList"), Session("CaseContLoadCont"), DO1.Value("CaseContainerScan"))

                'If Not String.IsNullOrEmpty(Result) Then
                '    MessageQue.Enqueue(t.Translate(Result))
                '    DO1.Value("CaseContainerScan") = ""
                '    Return
                'Else
                '    DO1.Value("CaseContainerScan") = ""
                '    ProcessScanContainers()
                'End If

                If Container.Exists(DO1.Value("CaseContainerScan")) Then
                    MessageQue.Enqueue(t.Translate("Scanned Container ID is not unique"))
                    DO1.Value("CaseContainerScan") = ""
                    Return
                End If

                If Not Session("CaseContScanPickList") Is Nothing And Not Session("CaseContPline") Is Nothing Then
                    Dim Ottrib As AttributesCollection = Nothing

                    If Not Session("CaseOttrib") Is Nothing Then
                        Ottrib = Session("CaseOttrib")
                    End If
                    Dim ContPickDetail As PicklistDetail = New PicklistDetail(Session("CaseContScanPickList"), Convert.ToInt32(Session("CaseContPline")))

                    If Not ContPickDetail Is Nothing Then
                        If String.IsNullOrEmpty(ContPickDetail.ToContainer) Then
                            ContPickDetail.SetContainer(DO1.Value("CaseContainerScan"), WMS.Logic.GetCurrentUser)

                            Dim container As Container = New Container()
                            container.ContainerId = ContPickDetail.ToContainer
                            container.UsageType = "PICKCONT"
                            container.Location = ContPickDetail.FromLocation
                            container.Post(WMS.Logic.GetCurrentUser)
                            container = Nothing

                            ContPickDetail.Pick(WMS.Logic.GetCurrentUser, Ottrib, False)

                            'Dim deliveryTask As DeliveryTask = New DeliveryTask()
                            'Dim container As Container = New Container(DO1.Value("CaseContainerScan"), True)
                            'container.SetDestinationLocation(ContPickDetail.ToLocation, WMS.Logic.GetCurrentUser)
                            'deliveryTask.CreateContainerDeliveryTask(DO1.Value("CaseContainerScan"), ContPickDetail.ToLocation, WMS.Logic.GetCurrentUser, Session("CaseContScanPickList"), False)
                        End If
                    End If
                End If

                DO1.Value("CaseContainerScan") = ""
                NewProcessScanContainers()
                Return
            Else
                MessageQue.Enqueue(t.Translate("Container ID cannot be blank"))
                DO1.Value("CaseContainerScan") = ""
                Return
            End If
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        'DO1.AddLabelLine("Note")
        DO1.AddLabelLine("CaseContainerMsg", "")
        DO1.AddLabelLine("CaseContainerProgress", "")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("UOMUNITS")
        DO1.AddSpacer()
        DO1.AddTextboxLine("CaseContainerScan", "Scan ContainerID")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
