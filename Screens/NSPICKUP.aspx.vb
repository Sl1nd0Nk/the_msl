Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared

Partial Public Class NSPICKUP
    Inherits System.Web.UI.Page


#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("LOCATION")
        DO1.AddTextboxLine("WAREHOUSEAREA")
        DO1.AddSpacer()
        DO1.AddTextboxLine("LOADID")
        DO1.AddTextboxLine("CONTAINER")
        DO1.AddSpacer()
    End Sub

    Private Sub doNext()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            If Not ValidateForm() Then
                Return
            End If
            Dim ts As WMS.Logic.Task
            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.GetCurrentUser, WMS.Lib.TASKTYPE.NSPICKUP) Then
                ts = New WMS.Logic.Task(WMS.Logic.TaskManager.getUserAssignedTask(WMS.Logic.GetCurrentUser, WMS.Lib.TASKTYPE.NSPICKUP))
            End If
            If ts.FROMLOCATION <> DO1.Value("LOCATION") Then
                MessageQue.Enqueue(trans.Translate("Location Confirmation incorrect"))
                Return
            End If
            ts.EDITUSER = WMS.Logic.GetCurrentUser
            ts.Complete()
            If DO1.Value("CONTAINER") <> "" Then
                Dim cnt As WMS.Logic.Container = New WMS.Logic.Container(DO1.Value("CONTAINER"), True)
                cnt.RequestPickUp(WMS.Logic.GetCurrentUser)
            Else
                Dim ld As WMS.Logic.Load = New WMS.Logic.Load(DO1.Value("LOADID"))
                ld.RequestPickUp(WMS.Logic.GetCurrentUser)
            End If
        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            Return
        End Try
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

    Private Function ValidateForm() As Boolean
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If DO1.Value("LOCATION") = "" Then
            MessageQue.Enqueue(trans.Translate("Location can not be blank"))
            Return False
        End If
        If DO1.Value("LOADID") <> "" And DO1.Value("CONTAINER") <> "" Then
            MessageQue.Enqueue(trans.Translate("Must fill only one field , load or container"))
            Return False
        End If
        If DO1.Value("LOADID") <> "" Then
            If Not WMS.Logic.Load.Exists(DO1.Value("LOADID")) Then
                MessageQue.Enqueue(trans.Translate("Load does not exists"))
                Return False
            End If
        End If
        If DO1.Value("CONTAINER") <> "" Then
            If Not WMS.Logic.Container.Exists(DO1.Value("CONTAINER")) Then
                MessageQue.Enqueue(trans.Translate("Container does not exists"))
                Return False
            End If
        End If
    End Function

End Class