﻿'Silindokuhle 
'Nkabinde
'10-OCT-2020
'************************************************************************************'

Imports Made4Net.Shared.Web
Imports WMS.Logic
Imports Made4Net.Shared
Imports System.Data
Imports Made4Net.DataAccess
Imports Made4Net.Mobile
Imports System.Windows.Forms
Imports System.IO

Partial Public Class EPODGEN1

    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SetInitialState()
        ' Check if the user has work in 
        Dim dtProgress As DataTable
        dtProgress = EpodInProgress(WMS.Logic.GetCurrentUser)

        If dtProgress.Rows.Count > 0 Then
            'User has work in progress
            Session.Add("v_RegNum", dtProgress.Rows(0)("VehicleRegNo"))
            Session.Add("epodID", dtProgress.Rows(0)("epodID"))
            'Check scanned orders for display
            Dim dt As DataTable = OrderInProgress(Session("epodID"))
            If dt.Rows.Count > 0 Then
                Session.Add("v_OrdNum", dt.Rows(0)("orderID"))
            End If

        End If

        WhereToNext()

    End Sub

    '******************************* Validation Functions *********************************************'
    Private Function EpodInProgress(ByVal User As String) As DataTable

        Dim Result As Boolean
        Result = False
        Dim dtLines As DataTable = New DataTable()
        'Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True)
        Try
            Dim SQL As String = String.Format("Select * from apx_EpodHeader where edituser={0} AND lower(Status) = {1} ORDER BY createDate DESC",
                                          Made4Net.[Shared].Util.FormatField(User, "NULL", False), Made4Net.[Shared].Util.FormatField("inprogress", "NULL", False))

            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
        Catch Ex As Exception
        End Try

        Return dtLines

    End Function

    Private Function OrderInProgress(ByVal epod As String) As DataTable

        Dim Result As Boolean
        Result = False
        'Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True)

        Dim dtLines As DataTable = New DataTable()
        Try
            Dim SQL As String = String.Format("Select top 1 * from apx_EpodDetail where epodID={0} ",
                                          Made4Net.[Shared].Util.FormatField(epod, "NULL", False))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
        Catch ex As Exception

        End Try


        Return dtLines

    End Function

    Private Function GetTotalScanned(ByVal epod As String) As DataTable

        Dim Result As Boolean
        Result = False
        ' Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True

        Dim dtLines As DataTable = New DataTable()

        Try
            Dim SQL As String = String.Format("SELECT COUNT (DISTINCT ORDERID) As totalScanned FROM apx_EpodDetail WHERE epodID = {0} ",
                                          Made4Net.[Shared].Util.FormatField(epod, "NULL", False))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)

        Catch ex As Exception

        End Try

        Return dtLines

    End Function

    Sub SetTotalScanned(ByVal epod As String, ByVal total As String)

        Dim Result As Boolean
        Result = False
        ' Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True

        Dim dtLines As DataTable = New DataTable()

        Try
            Dim SQL As String = String.Format("UPDATE apx_EpodHeader SET totalScannedOrders = {1} WHERE epodID = {0} ",
                               FormatField(epod), FormatField(total))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)

        Catch ex As Exception

        End Try

    End Sub

    Private Function CheckPrevCompleted(ByVal User As String, ByVal registration As String) As DataTable

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Result As Boolean
        Result = False
        Dim dtLines As DataTable = New DataTable()
        'Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True)
        Try
            Dim SQL As String = String.Format("Select * from apx_EpodHeader where edituser={0} AND lower(Status) = {1} and epodID = {2} + '_' + CAST(CAST(createDate AS DATE) AS VARCHAR) AND createDate >= CAST(GETDATE() AS DATE) ORDER BY createDate DESC ",
                                          FormatField(User), FormatField("complete"), FormatField(registration))

            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
            'Dim flag2 As Boolean = dtData.Rows.Count > 0
            '' MsgBox(dtLines.Rows(0)("Status"))
            'DO1.Value("vehicleReg") = ""
        Catch Ex As Exception
            'MessageQue.Enqueue(t.Translate(Ex.Message))
            'DO1.Value("vehicleReg") = ""
        End Try

        Return dtLines

    End Function

    Private Function CheckRegistrationValid(ByVal User As String, ByVal reg As String) As Boolean

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Result As Boolean
        Result = False
        Dim dtLines As DataTable = New DataTable()
        Dim SQL As String = String.Format("Select top 1 * from apx_EpodHeader where lower(Status) = {1} and epodID = {2} + '_' + CAST(CAST(createDate AS DATE) AS VARCHAR) ORDER BY createDate DESC ",
                                          FormatField(User), FormatField("complete"), FormatField(reg))
        Try
            'MessageQue.Enqueue(t.Translate(SQL))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
            If dtLines.Rows.Count > 0 Then
                Result = True
            End If
        Catch Ex As Exception
            Result = False
            'MessageQue.Enqueue(t.Translate(Ex.Message))
            'DO1.Value("prevReg") = ""
        End Try

        Return Result

    End Function

    Private Function CheckIfHasOrders(ByVal User As String, ByVal reg As String) As Boolean

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Result As Boolean
        Result = False
        Dim dtLines As DataTable = New DataTable()
        Dim SQL As String = String.Format("Select top 1 * from apx_EpodDetail A, apx_EpodHeader B WHERE A.epodID = B.epodID AND B.epodID = {2} + '_' + CAST(CAST(createDate AS DATE) AS VARCHAR) ",
                                          FormatField(User), FormatField("complete"), FormatField(reg))
        'MessageQue.Enqueue(t.Translate(SQL))
        Try

            'MessageQue.Enqueue(t.Translate(SQL))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
            If dtLines.Rows.Count > 0 Then
                Result = True
            End If
        Catch Ex As Exception
            Result = False
            'MessageQue.Enqueue(t.Translate(Ex.Message))
            'DO1.Value("prevReg") = ""
        End Try

        Return Result

    End Function


    Private Function CheckIfHasOrdersByEpod(ByVal epod As String) As Boolean

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Result As Boolean
        Result = False
        Dim dtLines As DataTable = New DataTable()
        Dim SQL As String = String.Format("Select top 1 * from apx_EpodDetail A, apx_EpodHeader B WHERE A.epodID = B.epodID AND B.epodID = {0} ",
                                          FormatField(epod))
        'MessageQue.Enqueue(t.Translate(SQL))
        Try
            'MessageQue.Enqueue(t.Translate(SQL))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
            If dtLines.Rows.Count > 0 Then
                Result = True
            End If
        Catch Ex As Exception
            Result = False
            'MessageQue.Enqueue(t.Translate(Ex.Message))
            'DO1.Value("prevReg") = ""
        End Try

        Return Result

    End Function

    Private Function OrdersInProgress(ByVal EpodId As String, ByRef dt As DataTable) As DataTable

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Result As Boolean
        Result = False
        'Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True)
        Dim dtLines As DataTable = New DataTable()

        Try
            Dim SQL As String = String.Format("Select top 2 (*) from apx_EpodDetail where epodID = {0} ",
                                           FormatField(EpodId))


            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
            'Dim flag2 As Boolean = dtData.Rows.Count > 0
            '' MsgBox(dtLines.Rows(0)("Status"))
            Result = dtLines.Rows.Count > 0
        Catch Ex As Exception
            'MessageQue.Enqueue(t.Translate(Ex.Message))
            Result = False
        End Try

        Return dtLines

    End Function


    Private Function GetPrevScanned(ByVal epod As String, ByVal myDate As String, ByVal User As String) As DataTable

        Dim Result As Boolean
        Result = False
        Dim dtLines As DataTable = New DataTable()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Try
            Dim SQL As String = String.Format("Select * from apx_EpodHeader where epodID={0} AND CreateDate >= {1} and EditUser = {2} ORDER BY createDate DESC",
                                           FormatField(epod), FormatField(myDate), FormatField(User))
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)
        Catch Ex As Exception
            'MessageQue.Enqueue(t.Translate(SQL))
        End Try

        Return dtLines
    End Function

    Private Function GetScannedData(ByVal epodID As String) As DataTable

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        ' Dim Sql As String = " select distinct * FROM apexvDelivery A, apx_EpodDetail B WHERE A.CustomerOrderNumber IN (SELECT distinct orderID FROM apx_EpodDetail WHERE epodID = {0}) AND A.CustomerOrderNumber = B.orderID"
        Dim SQL As String = String.Format(" select distinct * FROM apexvDelivery A, apx_EpodDetail B WHERE A.CustomerOrderNumber IN (SELECT distinct orderID FROM apx_EpodDetail WHERE epodID = {0}) AND A.CustomerOrderNumber = B.orderID AND B.epodID = {0} ",
                                          Made4Net.[Shared].Util.FormatField(epodID, "NULL", False))
        Dim sDtLines As DataTable = New DataTable()

        Made4Net.DataAccess.DataInterface.FillDataset(SQL, sDtLines, False, Nothing)

        Return sDtLines

    End Function

    Private Function GetScannedDataInProgress(ByVal UserID As String) As DataTable

        Dim Sql As String = " select distinct * FROM apexvDelivery A, apx_EpodDetail B WHERE A.CustomerOrderNumber IN (SELECT distinct orderID FROM apx_EpodDetail) AND A.CustomerOrderNumber = B.orderID"
        Dim sDtLines As DataTable = New DataTable()

        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        Return sDtLines

    End Function


    Private Function CheckOrderInView(ByVal OrderID As String) As Boolean

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Result As Boolean
        Result = False
        'Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True)

        Dim SQL As String = String.Format("Select * from apexvDelivery where CustomerOrderNumber={0}",
                                         Made4Net.[Shared].Util.FormatField(OrderID, "NULL", False))
        Dim dtLines As DataTable = New DataTable()
        DataInterface.FillDataset(SQL, dtLines, False, Nothing)

        If dtLines.Rows.Count > 0 Then
            Result = True
        End If

        Return Result

    End Function

    Private Function checkIfAlreadyScanned(ByVal OrderID As String, ByVal User As String) As Boolean

        Dim Result As Boolean

        Try
            Dim SQL As String = String.Format("Select * from apx_EpodDetail A, apx_EpodHeader B where A.orderID={0} and B.epodID = A.epodID",
                                FormatField(OrderID), FormatField(User))

            Dim dtLines As DataTable = New DataTable()
            DataInterface.FillDataset(SQL, dtLines, False, Nothing)

            If dtLines.Rows.Count > 0 Then
                Result = True
            End If
        Catch ex As Exception
            Result = False
        End Try

        Return Result
    End Function

    Private Function InsertOrderIntoDetails(ByVal OrderID As String, ByVal epodID As String) As Boolean
        ' MsgBox("Inserting Into Apx_det " + epodID)

        Dim Result As Boolean
        Result = False

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Try

            Dim SQL As String = String.Format("Insert into apx_EpodDetail (epodID, orderID) values({0},{1})", FormatField(epodID), FormatField(OrderID))
            DataInterface.RunSQL(SQL)
            Result = True
        Catch Ex As Exception
            'DO1.Value("OrderID") = ""
            Result = False
        End Try

        Return Result

    End Function

    Private Function CheckIfExist(ByVal User As String, ByRef dt As DataTable) As Boolean

        Dim Result As Boolean
        Result = False
        'Dim toLd As Load = New Load(Conversions.ToString(dt.Rows(0)("ToLoad")), True)

        Dim SQL As String = String.Format("Select * from apx_EpodHeader where edituser={0} AND lower(Status) = {1}",
                                          Made4Net.[Shared].Util.FormatField(User, "NULL", False), Made4Net.[Shared].Util.FormatField("inprogress", "NULL", False))
        Dim dtLines As DataTable = New DataTable()
        DataInterface.FillDataset(SQL, dtLines, False, Nothing)

        If dtLines.Rows.Count > 0 Then
            Result = True
        End If

        Return Result

    End Function


    Private Function SetVehicleRegNumber(ByVal User As String, ByVal VehicleRegNo As String) As Boolean

        Dim Result As Boolean
        Result = False
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Try

            Dim MyDateTime As DateTime = Now()
            Dim todayDate As String
            todayDate = MyDateTime.ToString("yyyy-MM-dd")
            Dim epodID As String = VehicleRegNo + "_" + todayDate
            Session.Add("epodID", epodID)

            'Return to change this
            Dim insertEpod As String = String.Concat(New String() {"INSERT INTO [apx_EpodHeader]([epodID], [VehicleRegNo], [createDate], [totalScannedOrders], [Status], [EditUser])
            VALUES('", epodID, "','", VehicleRegNo, "',", "GETDATE()", ",", "0", ",'", "InProgress", "','", User, "')"})
            Made4Net.DataAccess.DataInterface.RunSQL(insertEpod)
            Result = True

        Catch Ex As Exception
            Result = False
            MessageQue.Enqueue(t.Translate("Scanned vehicle registration number is invalid"))
        End Try

        Return Result

    End Function


    Private Function WriteToFile() As Boolean
        Using writer As StreamWriter =
           New StreamWriter("C:\\EpodFiles\\append.csv", True)

            ' writer.Write("One ")
            'writer.WriteLine("two 2")
            writer.Write(Session("csv"))
        End Using
    End Function


    Private Function DeleteBlanks() As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim result As Boolean
        Dim dtLines As DataTable

        result = False
        result = CheckIfHasOrders(WMS.Logic.GetCurrentUser, Session("v_RegNum"))

        If result = True Then
            Dim SQL1 As String = String.Format("Delete FROM  apx_EpodDetail WHERE epodID = '{1}' ", FormatField(WMS.Logic.GetCurrentUser.ToLower()), Format(Session("epodID")))
            Dim SQL2 As String = String.Format("Delete FROM apx_EpodHeader WHERE epodID = '{1}' ", FormatField(WMS.Logic.GetCurrentUser.ToLower()), Format(Session("epodID")))

            Try
                DataInterface.FillDataset(SQL1, dtLines, False, Nothing)
                DataInterface.FillDataset(SQL2, dtLines, False, Nothing)
                'Made4Net.Mobile.Common.GoToMenu()

            Catch ex As Exception

            End Try
        End If

        Return result
    End Function

    Private Function CheckForGarbageChars(ByVal value As String) As Boolean

        Dim Result As Boolean
        Result = False

        If System.Text.RegularExpressions.Regex.IsMatch(value, "^[a-zA-Z0-9]+$") Then
            Result = True
        End If

        Return Result

    End Function

    ' *******************************Validation Functions *********************************************'

    ' *******************************Display Functions *********************************************'
    Private Function BuildDisplayItems(ByVal DtLines As DataTable) As String

        Dim StartTable As String = "<br><br><table border='1'  id='myTable'><tr><th nowrap>epodID</th> <th nowrap>CustomerOrderNumber</th>
        <th nowrap>OrderReceivedDate</th> <th nowrap>CustomerID</th> <th nowrap>CustomerName</th>  <th nowrap>ItemCode</th>  
        <th nowrap>ItemName</th>  <th nowrap>Quantity</th> </tr>"

        Dim EndTable As String = "</table><br><br>"
        Dim StartRow As String = "<tr bgcolor='#DCDCDC'>"
        Dim EndRow As String = "</tr>"

        'CustomerOrderNumber	OrderReceivedDate	CustomerID	CompanyName	ItemCode	ItemName	Quantity 
        Dim x As Integer = 1
        If DtLines.Rows.Count > 0 Then
            For Each dr As DataRow In DtLines.Rows
                ' epodID	VehicleRegNo	createDate	totalScannedOrders	Status	EditUser
                StartTable = StartTable & StartRow & "<td nowrap>" & dr("epodID") & "</td>" & "<td nowrap>" & dr("CustomerOrderNumber") & "</td>" & "<td nowrap>" & dr("OrderReceivedDate") & "</td>" & "<td nowrap>" & dr("CustomerID") & "</td>" & "<td nowrap>" & dr("CustomerName") & "</td>" & "<td nowrap>" & dr("ItemCode") & "</td>" & "<td nowrap>" & dr("ItemName") & "</td>" & "<td nowrap>" & dr("Quantity") & "</td>" & EndRow
            Next
        End If

        Return StartTable & EndTable
    End Function

    Private Function BuildCsvData(ByVal DtLines As DataTable) As String

        Dim openQuote As String = """"
        Dim closeQuote As String = """"
        Dim headers As String = "Customer Order Number,Order Received Date (DD/MM/YYYY),Customer ID,Customer Name,Item Code,Item Name,Quantity"
        Dim StartTable As String = """" & "epodID," & """" & "CustomerOrderNumber," & """" & "OrderReceivedDate," & """" & "CustomerID," & """" & "CompanyName," & """" & "ItemCode," & """" & "ItemName," & """" & "Quantity"

        Dim EndTable As String = "\n"
        Dim data As String

        Dim x As Integer = 1
        Dim fileName As String
        fileName = "C:\\EpodFiles\\" & Session("epodID") & ".csv"
        Using writer As StreamWriter = New StreamWriter(fileName, False)
            writer.WriteLine(headers)
            If DtLines.Rows.Count > 0 Then
                For Each dr As DataRow In DtLines.Rows
                    StartTable = StartTable & dr("CustomerOrderNumber") & "," & dr("OrderReceivedDate") & "," & dr("CustomerID") & "," & dr("CustomerName") & "," & dr("ItemCode") & "," & dr("ItemName") & "," & dr("Quantity") & "\n"
                    data = """" & dr("CustomerOrderNumber") & """" & "," & """" & dr("OrderReceivedDate") & """" & "," & """" & dr("CustomerID") & """" & "," & """" & dr("CustomerName") & """" & "," & """" & dr("ItemCode") & """" & "," & """" & dr("ItemName") & """" & "," & """" & dr("Quantity") & """"
                    'data = """" & dr("epodID") & """" & "," & """" & dr("CustomerOrderNumber") & """" & "," & """" & dr("OrderReceivedDate") & """" & "," & """" & dr("CustomerID") & """" & "," & """" & dr("CompanyName") & """" & "," & """" & dr("ItemCode") & """" & "," & """" & dr("ItemName") & """" & "," & """" & dr("Quantity") & """"
                    writer.WriteLine(data)
                Next
                MessageQue.Enqueue(Session("epodID") & ".csv file has been created in path C:\\EpodFiles ")
            End If
        End Using

        Return StartTable

    End Function

    Private Sub SetInitialState()
        DO1.setVisibility("vehicle", False)
        DO1.setVisibility("orderLabel", False)
        DO1.setVisibility("Screen", False)
        DO1.setVisibility("TotalScanned", False)
        DO1.setVisibility("OrderID", False)
        DO1.setVisibility("prevOrderID", False)
        DO1.setVisibility("vehicleReg", True)
        DO1.setVisibility("regDate", False)
        DO1.Button(1).Visible = False
    End Sub
    '**************************************************Display Functions ***************************************************'

    '*********************************************** Routing and  displays***************************************************'
    Private Sub WhereToNext()

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        If Not Session("v_RegNum") Is Nothing Then
            'Set visible and invisible 
            DO1.setVisibility("vehicle", True)
            DO1.setVisibility("OrderID", True)
            DO1.setVisibility("vehicleReg", False)
            DO1.setVisibility("regDate", False)

            'Set values
            DO1.Value("vehicle") = Session("v_RegNum")

            'Check the ordernumber
            If Not Session("v_OrdNum") Is Nothing Then
                'Set visible and invisible 
                DO1.setVisibility("vehicle", True)
                DO1.setVisibility("OrderID", True)
                DO1.setVisibility("vehicleReg", False)
                DO1.setVisibility("regDate", False)

                'Get the display data for the epod 
                Dim DT As DataTable = GetScannedData(Session("epodID"))
                Dim DtTot As DataTable = GetTotalScanned(Session("epodID"))
                'Set total scanned orders in DB
                SetTotalScanned(Session("epodID"), DtTot.Rows(0)("totalScanned"))

                If DT.Rows.Count > 0 Then
                    ' Set totalScanned orders
                    DO1.setVisibility("TotalScanned", True)
                    DO1.Value("TotalScanned") = DtTot.Rows(0)("totalScanned")
                End If
            End If

        ElseIf Not Session("PrevReg") Is Nothing Then

            'Set visible and invisible 
            DO1.setVisibility("vehicle", True)
            DO1.setVisibility("OrderID", False)
            DO1.setVisibility("vehicleReg", False)
            DO1.setVisibility("regDate", False)
            'Set values 

            'Set visible and invisible 
            DO1.setVisibility("vehicle", True)
            DO1.setVisibility("OrderID", False)
            DO1.setVisibility("prevOrderID", False)
            DO1.setVisibility("vehicleReg", False)
            DO1.setVisibility("regDate", False)
            DO1.Button(1).Visible = True
            'Set values
            DO1.Value("vehicle") = Session("PrevReg")

            'Create Epod if not there
            Dim MyDateTime As DateTime = Now()
            Dim todayDate As String
            todayDate = MyDateTime.ToString("yyyy-MM-dd")
            Dim epodID As String = Session("PrevReg") + "_" + todayDate
            Session.Add("epodID", epodID)
            'Create Epod if not there

            Session.Add("epodID", epodID)

            Dim DT As DataTable = GetScannedData(Session("epodID"))
            Dim DtTot As DataTable = GetTotalScanned(Session("epodID"))

            If DT.Rows.Count > 0 Then
                ' Set totalScanned orders
                DO1.setVisibility("TotalScanned", True)
                DO1.Value("TotalScanned") = DtTot.Rows(0)("totalScanned")
            End If
        End If

    End Sub
    '*********************************************** Routing and  displays***************************************************'

    '*********************************************** Get inputs and validate ***************************************************'
    Private Sub DoNext()

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        If Not String.IsNullOrEmpty(DO1.Value("vehicleReg")) Then

            Try
                Dim noInvalidCharacters As Boolean
                Dim vehicleSetResults As Boolean
                Dim dtComp As DataTable

                noInvalidCharacters = CheckForGarbageChars(DO1.Value("vehicleReg"))

                If noInvalidCharacters Then

                    dtComp = CheckPrevCompleted(WMS.Logic.GetCurrentUser, DO1.Value("vehicleReg"))

                    If dtComp.Rows.Count <= 0 Then
                        vehicleSetResults = SetVehicleRegNumber(WMS.Logic.GetCurrentUser, DO1.Value("vehicleReg"))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Please scan a valid vehicle registration number"))
                End If

                If dtComp.Rows.Count > 0 Then
                    Session.Add("PrevReg", DO1.Value("vehicleReg"))
                    DO1.Value("vehicleReg") = ""
                ElseIf vehicleSetResults Then
                    Session.Add("v_RegNum", DO1.Value("vehicleReg"))
                    DO1.Value("vehicleReg") = ""
                End If

            Catch Ex As Exception
                DO1.Value("vehicleReg") = ""
            End Try

        ElseIf Not String.IsNullOrEmpty(DO1.Value("OrderID")) Then

            Try
                'Validate If Valid
                If CheckForGarbageChars(DO1.Value("OrderID")) Then
                    'Validate if in Roy View
                    If CheckOrderInView(DO1.Value("OrderID")) Then
                        'Validate if previously scanned
                        If checkIfAlreadyScanned(DO1.Value("OrderID"), WMS.Logic.GetCurrentUser) Then
                            MessageQue.Enqueue(t.Translate("Scanned order has been scanned before"))
                        Else
                            'Create Epod if not there
                            Dim MyDateTime As DateTime = Now()
                            Dim todayDate As String
                            todayDate = MyDateTime.ToString("yyyy-MM-dd")
                            Dim epodID As String = Session("v_RegNum") + "_" + todayDate
                            Session.Add("epodID", epodID)
                            'Create Epod if not there

                            'Insert if Ok
                            If InsertOrderIntoDetails(DO1.Value("OrderID"), epodID) Then
                                Session.Add("v_OrdNum", DO1.Value("OrderID"))
                                DO1.Value("OrderID") = ""
                            Else
                                MessageQue.Enqueue(t.Translate("Something went wrong inserting the order number"))
                            End If
                        End If
                    Else
                        MessageQue.Enqueue(t.Translate("Scanned order is invalid"))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Please scan a valid vehicle order number"))
                End If
            Catch Ex As Exception
                DO1.Value("OrderID") = ""
            End Try
        End If

        WhereToNext()

    End Sub

    '*********************************************** Get inputs and validate ***************************************************'

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("vehicle")
        DO1.AddLabelLine("orderLabel", "")
        DO1.AddLabelLine("Screen", "")

        '****** Useful ******************************
        DO1.AddTextboxLine("vehicleReg", "Scan vehicle registration number:")
        DO1.AddTextboxLine("regDate", "Scan the date for old epod:")
        DO1.AddLabelLine("TotalScanned")
        DO1.AddTextboxLine("OrderID", "Scan the order number")
        DO1.AddTextboxLine("prevOrderID", "Scan the order number") 
    End Sub

    Private Sub DoClose()

        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        'If it has no orders, It should be removable ....Session
        If Session("PrevReg") Is Nothing And Not Session("v_OrdNum") Is Nothing Then
            Dim DT As DataTable = GetScannedData(Session("epodID"))
            BuildCsvData(DT)
        End If

        If Not Session("epodID") Is Nothing Then
            MessageQue.Enqueue(t.Translate("Closing session"))
        End If

        If CheckIfHasOrdersByEpod(Session("epodID")) = False Then

            Dim SQL1 As String = String.Format("Delete FROM  apx_EpodDetail WHERE epodID = '{1}' ", FormatField(WMS.Logic.GetCurrentUser.ToLower()), Format(Session("epodID")))
            Dim SQL2 As String = String.Format("Delete FROM apx_EpodHeader WHERE epodID = '{1}' ", FormatField(WMS.Logic.GetCurrentUser.ToLower()), Format(Session("epodID")))

            Try
                DataInterface.RunSQL(SQL1)
                DataInterface.RunSQL(SQL2)
                Session.Remove("v_RegNum")
                Session.Remove("v_OrdNum")
                Session.Remove("epodID")
                Session.Remove("PrevReg")
                Session.Remove("dateEpod")
            Catch ex As Exception

            End Try
        Else
            Dim SQL As String = String.Format("UPDATE apx_EpodHeader set Status = 'Complete' where Lower(EditUser) ={0} and Lower(Status) = 'inprogress' and epodID = '{1}' ", FormatField(WMS.Logic.GetCurrentUser.ToLower()), Format(Session("epodID")))
            Try
                DataInterface.RunSQL(SQL)
                Session.Remove("v_RegNum")
                Session.Remove("v_OrdNum")
                Session.Remove("epodID")
                Session.Remove("PrevReg")
                Session.Remove("dateEpod")
            Catch ex As Exception

            End Try
        End If
        'Refresh
        Response.Redirect(MapVirtualPath("Screens/EPODGEN.aspx"))
    End Sub

    Private Sub DoMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DoRegenerateFile()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim DT As DataTable = GetScannedData(Session("epodID"))
        BuildCsvData(DT)
        Session.Remove("v_RegNum")
        Session.Remove("v_OrdNum")
        Session.Remove("epodID")
        Session.Remove("PrevReg")
        Session.Remove("dateEpod")
    End Sub

    '*********************************************** Click Events***************************************************'
    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Select Case e.CommandText.ToLower
            Case "next"
                DoNext()
            Case "menu"
                DoMenu()
            Case "close"
                DoClose()
            Case "csvregenerate"
                DoRegenerateFile()
        End Select

    End Sub
    '*********************************************** Click Events***************************************************'

End Class