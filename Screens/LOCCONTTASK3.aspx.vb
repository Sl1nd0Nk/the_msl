Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class LOCCONTTASK3
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    'Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DO1.Value("LOCATION") = Session("TaskLocationCNTLocationId")
    End Sub

    Private Sub doBack()
        Session.Remove("LocationCNTLoadId")
        Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
    End Sub

    Private Sub doEndCount()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim toqty, fromQty As Decimal
            CalcQty(fromQty, toqty)
            Dim oCntTask As WMS.Logic.CountTask
            If Not Session("LocationBulkCountTask") Is Nothing Then
                oCntTask = Session("LocationBulkCountTask")
            Else
                oCntTask = Session("LocationCountTask")
            End If
            Dim oCounting As New WMS.Logic.Counting(oCntTask.COUNTID)
            'Build and fill count job object
            Dim oCountJob As WMS.Logic.CountingJob = oCntTask.getCountJob(oCounting)
            oCountJob.ExpectedQty = fromQty
            oCountJob.CountedQty = toqty
            oCountJob.CountedLoads = Session("TaskLocationCNTLoadsDT")
            oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser)
            Session.Remove("LocationCNTLoadId")
            Session.Remove("TaskLocationCNTLocationId")
            Session.Remove("TSKTaskId")
            Session.Remove("LocationBulkCountTask")
            Session.Remove("LocationCountTask")
            Session("TaskID") = oCntTask.TASK
            MessageQue.Enqueue(trans.Translate("Location Count Completed"))
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
        End Try
        If MobileUtils.ShouldRedirectToTaskSummary() Then
            If Request.QueryString("countsourcescreen") <> "" Then
                ' Response.Redirect(MapVirtualPath("Screens/" & Request.QueryString("countsourcescreen") & ".aspx"))
                MobileUtils.RedirectToTaskSummary(Request.QueryString("countsourcescreen"), Session("TaskID"))
            Else
                ' Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSrcScreen") & ".aspx"))
                MobileUtils.RedirectToTaskSummary(Session("countsourcescreen"), Session("TaskID"))
            End If
        Else
            If Request.QueryString("countsourcescreen") <> "" Then
                Response.Redirect(MapVirtualPath("Screens/" & Request.QueryString("countsourcescreen") & ".aspx"))
            Else
                Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSrcScreen") & ".aspx"))
            End If
        End If

    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "endcount"
                doEndCount()
        End Select
    End Sub

    Private Function CreateLimboLoadCollection() As WMS.Logic.LoadsCollection
        Dim ldColl As New WMS.Logic.LoadsCollection
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            If dr("counted") = 0 Then
                ldColl.Add(New WMS.Logic.Load(Convert.ToString(dr("loadid"))))
            End If
        Next
        Return ldColl
    End Function

    Private Sub CalcQty(ByRef pFromqty As Decimal, ByRef pToQty As Decimal)
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            pFromqty = pFromqty + Convert.ToDecimal(dr("fromqty"))
            pToQty = pToQty + Convert.ToDecimal(dr("toqty"))
        Next
        Session("TaskLocationCNTLoadsDT") = dt
    End Sub

End Class
