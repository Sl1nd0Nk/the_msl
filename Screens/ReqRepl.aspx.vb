Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared

Partial Public Class ReqRepl
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("SELECTEDSKU") <> "" Then
            DO1.Value("SKU") = Session("SELECTEDSKU")
            ' Add all controls to session for restoring them when we back from that sreen
            DO1.Value("PICKLOC") = Session("SKUSEL_LOCATION")
            DO1.Value("CONSIGNEE") = Session("SELECTEDCONSIGNEE") 'Session("SKUSEL_CONSIGNEE")

            Session.Remove("SKUSEL_LOCATION")
            'Session.Remove("SKUSEL_CONSIGNEE")
            Session.Remove("SELECTEDCONSIGNEE")
            Session.Remove("SELECTEDSKU")
            doNext()
        End If
    End Sub

    Private Sub doMenu()
        Session.Remove("RelpTaskID")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim sql As String
            sql = String.Format("select consignee,sku,location from pickloc where consignee like '{0}%' and sku like '{1}%' and location like '{2}%'", _
            DO1.Value("CONSIGNEE"), DO1.Value("SKU"), DO1.Value("PICKLOC"))
            'Dim numOfPickLoadsFound As Integer = Made4Net.DataAccess.DataInterface.ExecuteScalar(sql)
            Dim dt As New DataTable()
            Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
            If dt.Rows.Count = 1 Then
                'If WMS.Logic.PickLoc.Exists(DO1.Value("PICKLOC")) Then
                Dim oRepl As New NormalReplenish()
                oRepl.NormalReplenish(dt.Rows(0)("LOCATION"), dt.Rows(0)("CONSIGNEE"), dt.Rows(0)("SKU"), "")
                MessageQue.Enqueue(t.Translate("Replenishment Request For Picking Location Sent"))
            ElseIf dt.Rows.Count > 1 Then
                Dim oRepl As New NormalReplenish()
                oRepl.NormalReplenish(dt.Rows(0)("LOCATION"), dt.Rows(0)("CONSIGNEE"), dt.Rows(0)("SKU"), "", True)
                MessageQue.Enqueue(t.Translate("Replenishment Request For Picking Location Sent"))
            Else
                MessageQue.Enqueue(t.Translate("Picking Location does not exists"))
            End If
            DO1.Value("PICKLOC") = ""
            DO1.Value("CONSIGNEE") = ""
            DO1.Value("SKU") = ""
        Catch ex As Threading.ThreadAbortException
        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
        End Try
    End Sub

    Private Sub doNext2()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            'Dim sql As String
            'sql = String.Format("select consignee,sku,location from pickloc where consignee like '{0}%' and sku like '{1}%' and location like '{2}%'", _
            'DO1.Value("CONSIGNEE"), DO1.Value("SKU"), DO1.Value("PICKLOC"))
            ''Dim numOfPickLoadsFound As Integer = Made4Net.DataAccess.DataInterface.ExecuteScalar(sql)
            'Dim dt As New DataTable()
            'Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
            Dim dt As DataRow() = getPickLocDT()
            If dt.Length = 0 Then
                MessageQue.Enqueue(t.Translate("Picking location does not exist"))
                Return
            Else
                Dim oRepl As New NormalReplenish()
                For Each dr As DataRow In dt
                    oRepl.NormalReplenish(dr("LOCATION"), dr("CONSIGNEE"), dr("SKU"), "")
                Next
                MessageQue.Enqueue(t.Translate("Replenishment request for picking location sent"))
            End If
            'If dt.Rows.Count = 1 Then
            '    'If WMS.Logic.PickLoc.Exists(DO1.Value("PICKLOC")) Then
            '    Dim oRepl As New NormalReplenish()
            '    oRepl.NormalReplenish(dt.Rows(0)("LOCATION"), dt.Rows(0)("CONSIGNEE"), dt.Rows(0)("SKU"), "")
            '    MessageQue.Enqueue(t.Translate("Replenishment Request For Picking Location Sent"))
            'ElseIf dt.Rows.Count > 1 Then
            '    Dim oRepl As New NormalReplenish()
            '    oRepl.NormalReplenish(dt.Rows(0)("LOCATION"), dt.Rows(0)("CONSIGNEE"), dt.Rows(0)("SKU"), "", True)
            '    MessageQue.Enqueue(t.Translate("Replenishment Request For Picking Location Sent"))
            'Else
            '    MessageQue.Enqueue(t.Translate("Picking Location does not exists"))
            'End If
            DO1.Value("PICKLOC") = ""
            DO1.Value("CONSIGNEE") = ""
            DO1.Value("SKU") = ""
        Catch ex As Threading.ThreadAbortException
        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
        End Try
    End Sub

    Private Function getPickLocDT() As DataRow()
        Dim pickLocStr As String = DO1.Value("PICKLOC")
        Dim skuStr As String = DO1.Value("SKU")
        Dim consigneeStr As String = DO1.Value("CONSIGNEE")
        If String.IsNullOrEmpty(pickLocStr) AndAlso String.IsNullOrEmpty(skuStr) Then
            Throw New Made4Net.Shared.M4NException(New Exception(), "Can not leave both pickloc and SKU fields empty", "Can not leave both pickloc and SKU fields empty")
        End If



        Dim sql As String
        If String.IsNullOrEmpty(skuStr) Then
            Dim dt As New DataTable
            sql = String.Format("select consignee,sku,location from pickloc where location = {0}", FormatField(pickLocStr))
            Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
            Return dt.Select()
        End If

        Dim dtSKU As DataTable = MobileUtils.GetSKUsDTFromSKUCODE(consigneeStr, skuStr)
        If dtSKU.Rows.Count = 0 Then
            Throw New M4NException(New Exception(), "SKU does not exist", "SKU does not exist")
        End If

        sql = String.Format("select consignee,sku,location from vpickloc ")
        If Not String.IsNullOrEmpty(pickLocStr) Then
            sql = String.Format("{0} where location = {1} ", sql, FormatField(pickLocStr))
        End If
        Dim dtPickLocs As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtPickLocs)
        If dtPickLocs.Rows.Count = 0 Then
            Throw New M4NException(New Exception(), "Picking location does not exist", "Picking location does not exist")
        End If


        Dim matchingConsignee As String = ""
        Dim matchingSKU As String = ""
        Dim matchingLocation As String = ""

        Dim rowsFound As DataRow()
        For Each dr As DataRow In dtSKU.Rows
            rowsFound = dtPickLocs.Select(String.Format("CONSIGNEE={0} AND SKU={1}", FormatField(dr("CONSIGNEE")), FormatField(dr("SKU"))))
            If rowsFound.Length = 1 Then
                If String.IsNullOrEmpty(matchingSKU) Then
                    matchingConsignee = rowsFound(0)("CONSIGNEE")
                    matchingSKU = rowsFound(0)("SKU")
                    matchingLocation = rowsFound(0)("LOCATION")
                Else
                    redirectToSKUMultiSelectForm(pickLocStr, consigneeStr, skuStr)
                End If
            ElseIf rowsFound.Length > 1 Then
                'Multiple picklocs with the same SKU
                If String.IsNullOrEmpty(matchingSKU) Then
                    matchingConsignee = rowsFound(0)("CONSIGNEE")
                    matchingSKU = rowsFound(0)("SKU")
                Else
                    redirectToSKUMultiSelectForm(pickLocStr, consigneeStr, skuStr)
                End If
            End If
        Next
        '' Means we couldn't find a line with the sku entered
        If String.IsNullOrEmpty(matchingSKU) Then
            Throw New M4NException(New Exception(), "No line was found with given SKU", "No line was found with given SKU")
        End If

        Return dtPickLocs.Select(String.Format("CONSIGNEE={0} AND SKU={1}", FormatField(matchingConsignee), FormatField(matchingSKU)))











        'sql = String.Format("SELECT vSC.CONSIGNEE, vSC.SKU, PL.LOCATION FROM vSKUCODE vSC INNER JOIN PICKLOC PL ON vSC.CONSIGNEE=PL.CONSIGNEE AND " & _
        '"vSC.SKU=PL.SKU WHERE (SKUCODE = {0} OR vSC.SKU = {0}) AND PL.LOCATION like {1}", _
        'FormatField(skuStr), FormatField(pickLocStr))
        'DataInterface.FillDataset(sql, dt)
        'If dt.Rows.Count = 0 Then
        '    Throw New Made4Net.Shared.M4NException(New Exception(), "Could not find matching picklocs", "Could not find matching picklocs")
        'ElseIf dt.Rows.Count > 1 Then
        '    ' Go to Sku select screen
        '    Session("FROMSCREEN") = "ReqRepl"
        '    Session("SKUCODE") = DO1.Value("SKU")
        '    ' Add all controls to session for restoring them when we back from that sreen
        '    Session("SKUSEL_LOCATION") = DO1.Value("PICKLOC")
        '    Session("SKUSEL_CONSIGNEE") = DO1.Value("CONSIGNEE")
        '    Response.Redirect(MapVirtualPath("Screens/MultiSelectForm.aspx?key1=" & pickLocStr))
        'End If

        'Return dt
    End Function

    Private Sub redirectToSKUMultiSelectForm(ByVal pLocation As String, ByVal pConsignee As String, ByVal pSKU As String)
        Session("FROMSCREEN") = "ReqRepl"
        Session("SKUCODE") = pSKU
        ' Add all controls to session for restoring them when we back from that sreen
        Session("SKUSEL_LOCATION") = pLocation
        Session("SKUSEL_CONSIGNEE") = pConsignee
        Response.Redirect(MapVirtualPath("Screens/MultiSelectForm.aspx?key1=" & pLocation))
    End Sub



    Private Function buildPickLocSQLQuery(ByVal pPickLoc As String, ByVal pConsignee As String, ByVal pSKU As String) As String

    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("PICKLOC")
        DO1.AddTextboxLine("CONSIGNEE")
        DO1.AddTextboxLine("SKU")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                'doNext()
                doNext2()
            Case "menu"
                doMenu()
        End Select
    End Sub

End Class
