Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class LOADING0
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    'Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

#Region "Bussiness Logic"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Session.Remove("LoadingJob")
            Session.Remove("DOOR")
            Session.Remove("SHIPMENT")
        End If
    End Sub

    Private Sub doNext()
        Try
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            ' Shipment and door scanned, verify than they are the same
            Dim SLSql As String
            Dim SLCheckDT As DataTable = New DataTable
            If DO1.Value("DOOR") <> "" And DO1.Value("SHIPMENT") <> "" Then
                SLSql = "SELECT SHIPMENT FROM SHIPMENT WHERE STATUS<>'SHIPPED' AND DOOR = '" & DO1.Value("DOOR") & "' AND SHIPMENT='" & DO1.Value("SHIPMENT") & "'"
                DataInterface.FillDataset(SLSql, SLCheckDT)
                If SLCheckDT.Rows.Count = 0 Then
                    Throw New Made4Net.Shared.M4NException("No shipments found for this door")
                End If
            ElseIf DO1.Value("DOOR") <> "" Then
                SLSql = "SELECT SHIPMENT FROM SHIPMENT WHERE STATUS<>'SHIPPED' AND DOOR = '" & DO1.Value("DOOR") & "'"
                DataInterface.FillDataset(SLSql, SLCheckDT)
                If SLCheckDT.Rows.Count = 0 Then
                    Throw New Made4Net.Shared.M4NException("No shipments found for this door")
                ElseIf SLCheckDT.Rows.Count = 1 Then
                    DO1.Value("SHIPMENT") = SLCheckDT.Rows(0)("SHIPMENT")
                Else
                    Throw New Made4Net.Shared.M4NException("More than one shippment found for this door")
                End If
            ElseIf DO1.Value("SHIPMENT") <> "" Then
                SLSql = "SELECT DOOR FROM SHIPMENT WHERE STATUS<>'SHIPPED' AND SHIPMENT='" & DO1.Value("SHIPMENT") & "'"
                DataInterface.FillDataset(SLSql, SLCheckDT)
                If SLCheckDT.Rows.Count = 0 Then
                    Throw New Made4Net.Shared.M4NException("No door found for this shipment")
                ElseIf SLCheckDT.Rows.Count = 1 Then
                    DO1.Value("DOOR") = SLCheckDT.Rows(0)("DOOR")
                Else
                    Throw New Made4Net.Shared.M4NException("More than one door found for this shipment")
                End If
            Else
                Throw New Made4Net.Shared.M4NException("No shipments or door is scanned")
            End If

            Session("LOADINGDOOR") = DO1.Value("DOOR")
            Session("LOADINGSHIPMENT") = DO1.Value("SHIPMENT")

        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        Response.Redirect(MapVirtualPath("Screens/LOADING.aspx"))
    End Sub

    Private Sub doMenu()
        Session.Remove("LoadingJob")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddSpacer()
        DO1.AddTextboxLine("DOOR")
        DO1.AddTextboxLine("SHIPMENT")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

#End Region

End Class
