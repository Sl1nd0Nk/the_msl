Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

Partial Public Class PCKNPP
    Inherits System.Web.UI.Page
#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(WMS.Logic.GetCurrentUser) Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not Request.QueryString("sourcescreen") Is Nothing Then
                Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
            ElseIf Session("MobileSourceScreen") Is Nothing Then
                Session("MobileSourceScreen") = "PCK"
            ElseIf Not Session("MobileSourceScreen").ToString.ToLower() = "taskmanager" Then
                Session("MobileSourceScreen") = "PCK"
            End If

            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PICKING) Then
                Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
            End If

            Dim pcks As Picklist = Session("PCKPicklist")
            Dim pck As PickJob
            Dim tm As New WMS.Logic.TaskManager(pcks.PicklistID, True)
            Try
                pck = PickTask.getNextPick(pcks)
            Catch ex As Exception
            End Try
            setPick(pck)
        End If
    End Sub


    Private Sub doBack()
        'Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
        Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
    End Sub

    Private Sub setPick(ByVal pck As PickJob)
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
            Response.Redirect(MapVirtualPath("screens/PCKNPPDEL.aspx?sourcescreen=PCKNPP_" & Session("MobileSourceScreen")))
        Else
            Session("PCKPicklistPickJob") = pck
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Dim pcks As Picklist = Session("PCKPicklist")
            DO1.Value("LOADID") = pck.fromload
            DO1.Value("LOCATION") = pck.fromlocation
            DO1.Value("PICKMETHOD") = pcks.PickMethod
            DO1.Value("PICKTYPE") = pcks.PickType
            DO1.Value("SKU") = pck.sku
            DO1.Value("SKUDESC") = pck.skudesc
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            'DO1.Value("UOMUNITS") = "(" & pck.units & ")" & " " & trans.Translate(pck.uom) & " " & pck.uomunits
            'If pck.SystemPickShort Then
            '    MessageQue.Enqueue(trans.Translate("System Pick Short- Quantity available is less than quantity required / Wrong Location!"))
            'End If
            ''Label Printing
            'If MobileUtils.LoggedInMHEID <> String.Empty And MobileUtils.GetMHEDefaultPrinter <> String.Empty Then
            '    DO1.setVisibility("PRINTER", False)
            'Else
            '    DO1.setVisibility("PRINTER", True)
            'End If
        End If
    End Sub

    Private Sub doNext()
        Dim pck As PickJob
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim pcklst As Picklist = Session("PCKPicklist")
        pck = Session("PCKPicklistPickJob")
        Try
            pck.pickedqty = pck.units
            Dim tm As New WMS.Logic.TaskManager(pck.picklist, True)
            If MobileUtils.LoggedInMHEID.Trim <> String.Empty Then
                pck.LabelPrinterName = MobileUtils.GetMHEDefaultPrinter
            End If
            pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            'pck = PickTask.Pick(DO1.Value("CONFIRM").Trim(), pcklst, pck, WMS.Logic.Common.GetCurrentUser)
            pck.TaskConfirmation.Confirm()
            'pck = PickTask.Pick(pcklst, pck, WMS.Logic.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        End Try
        Response.Redirect(MapVirtualPath("screens/PCKNPPDEL.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        'setPick(pck)
    End Sub
    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
        End Select
    End Sub

    Private Sub addConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.AddTextboxLine("CONFIRM(LOAD)")
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.AddTextboxLine("CONFIRM(SKU)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.AddTextboxLine("CONFIRM(UPC)")
        End Select
    End Sub

    Private Sub clearConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.Value("CONFIRM(LOAD)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.Value("CONFIRM(SKU)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.Value("CONFIRM(UPC)") = ""
        End Select
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("PickMethod")
        DO1.AddLabelLine("PickType")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LoadId")
        DO1.AddLabelLine("Location")
        'DO1.AddLabelLine("UomUnits")
        DO1.AddSpacer()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        addConfirmationFields(pck.TaskConfirmation.ConfirmationType)
        'DO1.AddTextboxLine("SubtituteLoad")
        'DO1.AddTextboxLine("PRINTER")
    End Sub

End Class