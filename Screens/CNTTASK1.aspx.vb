Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class CNTTASK1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("TaskLoadCNTLoadId") Is Nothing Then
                doBack()
            End If
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
            DO1.Value("CONSIGNEE") = ld.CONSIGNEE
            DO1.Value("SKU") = ld.SKU
            DO1.Value("LOCATION") = ld.LOCATION
            DO1.Value("LOADID") = ld.LOADID
            If Not String.IsNullOrEmpty(Session("PCKSOURCESCREEN")) Then
                DO1.setVisibility("TOLOCATION", False)
                DO1.Value("TOLOCATION") = ld.LOCATION
            End If
            Dim ctrl As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("UOM")

            ctrl.AllOption = False
            ctrl.TableName = "SKUUOMDESC"
            ctrl.ValueField = "UOM"
            ctrl.TextField = "DESCRIPTION"
            ctrl.Where = String.Format("CONSIGNEE = '{0}' and SKU = '{1}'", ld.CONSIGNEE, ld.SKU)
            ctrl.DataBind()
            Try
                ctrl.SelectedValue = ld.LOADUOM
            Catch ex As Exception
            End Try
            Try
                DO1.Value("SKUDESC") = Made4Net.DataAccess.DataInterface.ExecuteScalar(String.Format("select skudesc from sku where sku = '{0}' and consignee = '{1}'", ld.SKU, ld.CONSIGNEE))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub doMenu()
        Dim oCntTask As WMS.Logic.CountTask
        If Not Session("LoadCountTask") Is Nothing Then
            oCntTask = Session("LoadCountTask")
            Session.Remove("LoadCountTask")
        End If
        Session.Remove("TaskLoadCNTLoadId")
        Session.Remove("TSKTaskId")
        If Not String.IsNullOrEmpty(Session("PCKSOURCESCREEN")) Then
            If Not oCntTask Is Nothing AndAlso _
            oCntTask.STATUS <> WMS.Lib.Statuses.Task.CANCELED AndAlso _
             oCntTask.STATUS <> WMS.Lib.Statuses.Task.COMPLETE Then
                oCntTask.Cancel()
            End If
            Dim srcScreen As String = Session("PCKSOURCESCREEN")
            Session.Remove("PCKSOURCESCREEN")
            Response.Redirect(MapVirtualPath("Screens/" + srcScreen + ".aspx"))
            Return
        End If

        If Not oCntTask Is Nothing Then
            oCntTask.ExitTask()
        End If

        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doNext()
        'If Page.IsValid Then
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim toUnits As Decimal
            Try
                toUnits = Decimal.Parse(DO1.Value("TOUNITS"))
                If toUnits < 0 Then
                    Throw New Exception()
                End If
            Catch ex As Exception
                MessageQue.Enqueue(t.Translate("Number of units must be a non negative number."))
                DO1.Value("TOUNITS") = ""
                DO1.FocusField = "TOUNITS"
                Return
            End Try
            If String.IsNullOrEmpty(DO1.Value("TOLOCATION")) Then
                MessageQue.Enqueue(t.Translate("Location can not be empty."))
                DO1.FocusField = "TOLOCATION"
                Return
            End If
            If Not WMS.Logic.Location.Exists(DO1.Value("TOLOCATION")) Then
                MessageQue.Enqueue(t.Translate("Location does not exist."))
                DO1.FocusField = "TOLOCATION"
                Return
            End If

            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
            If Not Session("LoadCountTask") Is Nothing Then
                Dim oCntTask As WMS.Logic.CountTask = Session("LoadCountTask")
                Session("TaskId") = oCntTask.TASK
                Dim oCounting As New Counting(oCntTask.COUNTID)
                'Build and fill count job object
                Dim oCountJob As CountingJob = oCntTask.getCountJob(oCounting)
                oCountJob.CountedQty = DO1.Value("TOUNITS")
                oCountJob.UOM = DO1.Value("UOM")
                oCountJob.Location = DO1.Value("TOLOCATION")
                oCountJob.LoadCountVerified = False
                oCountJob.LocationCountingLoadCounted = True
                Try
                    oCountJob.CountingAttributes = ExtractAttributeValues()
                Catch m4nEx As Made4Net.Shared.M4NException
                    Dim coll As New Made4Net.DataAccess.Collections.GenericCollection()
                    coll.Add("param0", m4nEx.Params("attribute"))
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Attribute #param0# is not Valid", coll))
                    Return
                Catch ex As Exception
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.ToString()))
                    Return
                End Try
                'If (ShouldVerifyCounting(oCountJob.CountedQty, oCountJob.UOM, ld) Or _
                '        Not ShouldVerifyCountingAttributes(oCountJob.LoadId, oCountJob.CountingAttributes)) _
                '        And Not oCountJob.LoadCountVerified Then
                '    oCountJob.LoadCountVerified = False
                'Else
                '    oCountJob.LoadCountVerified = True
                'End If
                If Not oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser) Then
                    MessageQue.Enqueue(t.Translate("There is a difference between the entered values to the current load values. Please approve"))
                    Session("CountedUnitsForVerification") = oCountJob.CountedQty
                    Session("UOMForVerification") = oCountJob.UOM
                    Session("ToLocationForVerification") = DO1.Value("TOLOCATION")
                    Session("AttributesForVerification") = oCountJob.CountingAttributes
                    Dim SrcScreen As String
                    If Session("CountingSourceScreen") Is Nothing Then
                        SrcScreen = "CNTTASK"
                    Else
                        SrcScreen = Session("CountingSourceScreen")
                    End If
                    Response.Redirect(MapVirtualPath("Screens/CNTTASKVERFICATION.aspx?SRCSCREEN=" & SrcScreen))
                End If
            End If
        Catch ex As Threading.ThreadAbortException
            Return
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        Session.Remove("TSKTaskId")
        Session.Remove("TaskLoadCNTLoadId")

        If Not Session("CountingSourceScreen") Is Nothing Then
            If MobileUtils.ShouldRedirectToTaskSummary Then
                MobileUtils.RedirectToTaskSummary(Session("CountingSourceScreen"), Session("TaskId"))
            Else
                Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSourceScreen") & ".aspx"))
            End If
        Else
            If MobileUtils.ShouldRedirectToTaskSummary() Then
                MobileUtils.RedirectToTaskSummary("CNTTASK")
            Else
                Response.Redirect(MapVirtualPath("Screens/CNTTASK.aspx"))
            End If
        End If
        'End If
    End Sub

    Private Sub doBack()
        Session.Remove("TSKTaskId")
        Session.Remove("TaskLoadCNTLoadId")
        Dim oCntTask As WMS.Logic.CountTask
        oCntTask = Session("LoadCountTask")
        Session.Remove("LoadCountTask")
        If Not String.IsNullOrEmpty(Session("PCKSOURCESCREEN")) Then
            Dim srcScreen As String = Session("PCKSOURCESCREEN")
            If Not oCntTask Is Nothing Then
                oCntTask = New WMS.Logic.CountTask(oCntTask.TASK)
                If oCntTask.STATUS <> WMS.Lib.Statuses.Task.CANCELED AndAlso _
                oCntTask.STATUS <> WMS.Lib.Statuses.Task.COMPLETE Then
                    oCntTask.Cancel()
                End If
            End If
            Session.Remove("PCKSOURCESCREEN")
            Response.Redirect(MapVirtualPath("Screens/" + srcScreen + ".aspx"))
        End If

        'Response.Redirect(MapVirtualPath("Screens/CNTTASK.aspx"))
        If Not Session("CountingSourceScreen") Is Nothing Then
            Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSourceScreen") & ".aspx"))
        Else
            Response.Redirect(MapVirtualPath("Screens/CNTTASK.aspx"))
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        DO1.AddDropDown("UOM")
        DO1.AddTextboxLine("TOUNITS")
        DO1.AddTextboxLine("TOLOCATION")
        setAttributes()
    End Sub

    Private Sub setAttributes()
        Dim ld As New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
        Dim oSku As String = ld.SKU  '= Session("CreateLoadSKU")
        Dim oConsignee As String = ld.CONSIGNEE  'Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If Not objSkuClass Is Nothing Then
            If objSkuClass.CaptureAtCountingLoadAttributesCount > 0 Then
                For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
                    If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required OrElse _
                    oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                        Try
                            If oAtt.Type = AttributeType.DateTime Then
                                DO1.AddTextboxLine(oAtt.Name, oAtt.Name) ', CType(ld.LoadAttributes.Attribute(oAtt.Name), DateTime).ToString(Made4Net.Shared.GetSystemParameterValue("System_DateFormat"))) '.ToString("dd/MM/yyyy"))
                            Else
                                DO1.AddTextboxLine(oAtt.Name, oAtt.Name) ', ld.LoadAttributes.Attribute(oAtt.Name))
                            End If
                        Catch ex As Exception
                            DO1.AddTextboxLine(oAtt.Name, oAtt.Name)
                        End Try
                    End If
                Next
            End If
        End If
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim ld As New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
        Dim oSku As String = ld.SKU  'DO1.Value("SKU") ' Session("CreateLoadSKU")
        Dim oConsignee As String = ld.CONSIGNEE ' DO1.Value("CONSIGNEE") 'Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return Nothing
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required OrElse _
                        oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            If String.IsNullOrEmpty(DO1.Value(oAtt.Name)) Then
                                val = DateTime.MinValue
                            Else
                                val = DateTime.ParseExact(DO1.Value(oAtt.Name), Made4Net.Shared.AppConfig.DateFormat, Nothing)
                                'val = CType(DO1.Value(oAtt.Name), DateTime)
                            End If
                        Case Logic.AttributeType.Decimal
                            If String.IsNullOrEmpty(DO1.Value(oAtt.Name)) Then
                                val = Decimal.MinValue
                            Else
                                val = CType(DO1.Value(oAtt.Name), Decimal)
                            End If
                        Case Logic.AttributeType.Integer
                            If String.IsNullOrEmpty(DO1.Value(oAtt.Name)) Then
                                val = Integer.MinValue
                            Else
                                val = CType(DO1.Value(oAtt.Name), Int32)
                            End If
                        Case Logic.AttributeType.String
                            val = DO1.Value(oAtt.Name)
                    End Select
                Catch ex As Exception
                    val = Nothing
                End Try
                If Not validateAttributeValue(oAtt.Type, val, oAtt.CaptureAtCounting) Then
                    Dim m4nExc As New Made4Net.Shared.M4NException(New Exception(), "Invalid attribute value", "Invalid attribute value")
                    m4nExc.Params.Add("attribute", oAtt.Name)
                    Throw m4nExc
                End If
                oAttCol.Add(oAtt.Name, val)
            End If
        Next
        Return oAttCol
    End Function

    Private Function validateAttributeValue(ByVal pType As WMS.Logic.AttributeType, ByVal pValue As Object, ByVal pCaptureType As WMS.Logic.SkuClassLoadAttribute.CaptureType) As Boolean
        'Try
        If pValue Is Nothing Then
            Return False
        End If
        If pCaptureType = SkuClassLoadAttribute.CaptureType.Capture Then
            Return True
        End If
        If pType = AttributeType.Boolean Then
            Try
                Dim boolVal As Boolean = CType(pValue, Boolean)
            Catch ex As Exception
                Return False
            End Try
        ElseIf pType = AttributeType.DateTime Then
            Try
                Dim dateVal As DateTime = pValue
                If dateVal = DateTime.MinValue Then Return False
            Catch ex As Exception
                Return False
            End Try
        ElseIf pType = AttributeType.Decimal Then
            Try
                Dim decVal As Decimal = Decimal.Parse(pValue)
                If decVal = Decimal.MinValue Then Return False
            Catch ex As Exception
                Return False
            End Try
        ElseIf pType = AttributeType.Integer Then
            Try
                Dim intVal As Integer = Integer.Parse(pValue)
                If intVal = Integer.MinValue Then Return False
            Catch ex As Exception
                Return False
            End Try
        ElseIf pType = AttributeType.String Then
            Try
                If pValue Is Nothing Or Convert.ToString(pValue) = "" Then
                    If pCaptureType = SkuClassLoadAttribute.CaptureType.Capture Then
                        Return True
                    Else
                        Return False
                    End If
                End If
                Return True
            Catch ex As Exception
                Return False
            End Try
        End If
        Return True
    End Function

#Region "Validations"

    Public Shared Function ShouldVerifyCountingAttributes(ByVal pLoadId As String, ByVal oAttributesCollection As WMS.Logic.AttributesCollection) As Boolean
        Dim ld As New WMS.Logic.Load(pLoadId, True)
        Dim oSku As String = ld.SKU
        Dim oConsignee As String = ld.CONSIGNEE
        Dim oSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        ' No attributes , return validated
        If oSkuClass Is Nothing Then Return True
        Try
            For Each oLoadAtt As SkuClassLoadAttribute In oSkuClass.LoadAttributes
                Dim typeValidationResult As Int32

                If oLoadAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Capture Then
                    If oAttributesCollection Is Nothing Then Continue For
                    If oAttributesCollection(oLoadAtt.Name) Is Nothing OrElse oAttributesCollection(oLoadAtt.Name) Is DBNull.Value OrElse (oLoadAtt.Type = AttributeType.String AndAlso oAttributesCollection(oLoadAtt.Name) = "") Then Continue For
                ElseIf oLoadAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Required Then
                    ' Validate for required values
                    If oAttributesCollection Is Nothing Then Throw New ApplicationException(String.Format("Attribute {0} value was not supplied", oLoadAtt.Name))
                    If oAttributesCollection(oLoadAtt.Name) Is Nothing Or oAttributesCollection(oLoadAtt.Name) Is DBNull.Value Then
                        Throw New ApplicationException(String.Format("Attribute {0} value was not supplied", oLoadAtt.Name))
                    End If
                End If
                If oLoadAtt.CaptureAtCounting <> SkuClassLoadAttribute.CaptureType.NoCapture Then
                    ' Validator
                    If Not oLoadAtt.CountingValidator Is Nothing Then
                        'New Validation with expression evaluation
                        Dim vals As New Made4Net.DataAccess.Collections.GenericCollection
                        vals.Add(oLoadAtt.Name, oAttributesCollection(oLoadAtt.Name))
                        vals.Add("LOADID", pLoadId)
                        Dim ret As String = oLoadAtt.Evaluate(SkuClassLoadAttribute.EvaluationType.Counting, vals)
                        Dim returnedResponse() As String = ret.Split(";")
                        'If ret = "-1" Then
                        If returnedResponse(0) = "-1" Then
                            If returnedResponse.Length > 1 Then
                                Throw New Made4Net.Shared.M4NException(New Exception, "Invalid Attribute Value " & oLoadAtt.Name & ". " & returnedResponse(1), "Invalid Attribute Value " & oLoadAtt.Name & "." & returnedResponse(1))
                            Else
                                Throw New Made4Net.Shared.M4NException(New Exception, "Invalid Attribute Value " & oLoadAtt.Name, "Invalid Attribute Value " & oLoadAtt.Name)
                            End If
                        Else
                            oAttributesCollection(oLoadAtt.Name) = ret
                        End If
                    End If

                    'Now need to match the values of the attribues

                    Dim Value As Object
                    Value = oAttributesCollection(oLoadAtt.Name)
                    If Not Value Is Nothing And Not Value Is System.DBNull.Value Then
                        Try
                            If TypeOf Value Is System.DateTime Then
                                If Convert.ToDateTime(oAttributesCollection(oLoadAtt.Name)).Date <> Convert.ToDateTime(ld.LoadAttributes.Attributes(oLoadAtt.Name)).Date Then
                                    Return False
                                End If
                            Else
                                If oAttributesCollection(oLoadAtt.Name) <> ld.LoadAttributes.Attributes(oLoadAtt.Name) Then
                                    Return False
                                End If
                            End If
                        Catch ex As Exception
                            If oLoadAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Required Then
                                Return False
                            End If
                        End Try
                    End If
                End If
            Next
        Catch ex As Made4Net.Shared.M4NException
            Throw ex
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Private Shared Function ValidateAttributeByType(ByVal oAtt As SkuClassLoadAttribute, ByVal oAttVal As Object) As Int32
        Select Case oAtt.Type
            Case Logic.AttributeType.DateTime
                Dim Val As DateTime
                Try
                    If oAttVal Is Nothing Then
                        If oAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Capture Then
                            Return 0
                        Else
                            Return -1
                        End If
                    End If
                    Val = CType(oAttVal, DateTime)
                    Return 1
                Catch ex As Exception
                    Return -1
                End Try
            Case Logic.AttributeType.Decimal
                Dim Val As Decimal
                Try
                    If oAttVal Is Nothing Then
                        If oAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Capture Then
                            Return 0
                        Else
                            Return -1
                        End If
                    End If
                    Val = CType(oAttVal, Decimal)
                    Return 1
                Catch ex As Exception
                    Return -1
                End Try
            Case Logic.AttributeType.Integer
                Dim Val As Int32
                Try
                    If oAttVal Is Nothing Then
                        If oAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Capture Then
                            Return 0
                        Else
                            Return -1
                        End If
                    End If
                    Val = CType(oAttVal, Int32)
                    Return 1
                Catch ex As Exception
                    Return -1
                End Try
            Case Logic.AttributeType.String
                Try
                    If oAttVal Is Nothing Or Convert.ToString(oAttVal) = "" Then
                        If oAtt.CaptureAtCounting = SkuClassLoadAttribute.CaptureType.Capture Then
                            Return 0
                        Else
                            Return -1
                        End If
                    End If
                    Return 1
                Catch ex As Exception
                    Return -1
                End Try
        End Select
    End Function

    Public Function ShouldVerifyCounting(ByVal pCountedUnits As Decimal, ByVal pCountedUom As String, ByVal oLoad As WMS.Logic.Load) As Boolean
        Dim oSku As New SKU(oLoad.CONSIGNEE, oLoad.SKU)
        Try
            pCountedUnits = oSku.ConvertToUnits(pCountedUom) * pCountedUnits
        Catch ex As Exception
            Dim m4nEx As New Made4Net.Shared.M4NException(New Exception, "Invalid load UOM", "Invalid load UOM")
            Throw m4nEx
        End Try
        If Math.Abs(oLoad.UNITS - pCountedUnits) > oSku.COUNTTOLERANCE Then
            Return True
        End If
        Return False
    End Function

#End Region

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
