Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class PCKORD1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PICKING) Then
                Response.Redirect(MapVirtualPath("Screens/PCKORD.aspx"))
            End If

            Dim pcks As Picklist = Session("PCKPicklist")
            Dim pck As PickJob
            Try
                Dim tm As New WMS.Logic.TaskManager(pcks.PicklistID, True)
                pck = PickTask.getNextPick(pcks)
            Catch ex As Exception

            End Try

            setPick(pck)
        End If
    End Sub

    Private Sub setPick(ByVal pck As PickJob)
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If pck Is Nothing Then
            Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCKORD"))
        Else
            Session("PCKPicklistPickJob") = pck
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Dim pcks As Picklist = Session("PCKPicklist")
            DO1.Value("Picklist") = pck.picklist
            DO1.Value("LOADID") = pck.fromload
            DO1.Value("LOCATION") = pck.fromlocation
            DO1.Value("PICKMETHOD") = pcks.PickMethod
            DO1.Value("PICKTYPE") = pcks.PickType
            DO1.Value("SKU") = pck.sku
            DO1.Value("SKUDESC") = pck.skudesc
            DO1.Value("UOM") = pck.uom
            Dim sqluom As String = " SELECT DESCRIPTION FROM CODELISTDETAIL " & _
                          " WHERE CODELISTCODE = 'UOM' AND CODE = '" & pck.uom & "'"
            DO1.Value("UOMDesc") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sqluom)
            DO1.Value("UOMUNITS") = pck.uomunits

            If pck.SystemPickShort Then
                MessageQue.Enqueue(trans.Translate("System Pick Short- Quantity available is less than quantity required / Wrong Location!"))
            End If
        End If
    End Sub

    Private Sub doNext()
        Dim pck As PickJob
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim pcklst As Picklist = Session("PCKPicklist")
        pck = Session("PCKPicklistPickJob")
        If pck Is Nothing Then
            Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCKORD"))
        End If

        Try
            Dim sku As New sku(pck.consingee, pck.sku)
            pck.pickedqty = sku.ConvertToUnits(pck.uom) * Convert.ToDecimal(DO1.Value("UOMUNITS"))
            sku = Nothing
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate("Error cannot get units"))
            Return
        End Try

        Try
            Dim tm As New WMS.Logic.TaskManager(pcklst.PicklistID, True)
            ExtractAttributes()
            pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            pck.container = Session("PCKPicklistActiveContainerID")
            pck = PickTask.Pick(pcklst, pck, WMS.Logic.GetCurrentUser)
            'If Session("CONFTYPE") = WMS.Lib.Release.CONFIRMATIONTYPE.SKULOCATION Then
            'pck = PickTask.Pick(DO1.Value("CONFIRM"), pcklst, pck, WMS.Logic.Common.GetCurrentUser, True, DO1.Value("CONFIRMSKU"))
            'Else
            'pck = PickTask.Pick(DO1.Value("CONFIRM"), pcklst, pck, WMS.Logic.Common.GetCurrentUser)
            'End If
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        End Try

        ClearAttributes()
        DO1.Value("CONFIRM") = ""
        DO1.Value("CONFIRMSKU") = ""
        setPick(pck)
    End Sub

    Private Sub doBack()
        Session.Remove("CONFTYPE")
        Response.Redirect(MapVirtualPath("Screens/PCKORD.aspx"))
    End Sub

    Private Sub doMenu()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.PICKING) Then
            Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PICKING)
            tm.ExitTask()
        End If
        Session.Remove("CONFTYPE")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("Picklist")
        DO1.AddLabelLine("PickMethod")
        DO1.AddLabelLine("PickType")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UOMDesc")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    DO1.AddTextboxLine(pck.oAttributeCollection.Keys(idx))
                Next
            End If
        End If

        addConfirmationFields(pck.TaskConfirmation.ConfirmationType)

        'DO1.AddTextboxLine("CONFIRM")
        'DO1.AddTextboxLine("CONFIRMSKU")
        DO1.AddTextboxLine("UOMUNITS")

        DO1.setVisibility("UOMDesc", True)

        'Added by udi 01/01/2006
        'Dim pcklist As Picklist = New Picklist(pck.picklist)
        'Dim relStrat As ReleaseStrategyDetail

        'relStrat = pcklist.getReleaseStrategy()
        'If Not relStrat Is Nothing Then
        '    Session("CONFTYPE") = relStrat.ConfirmationType
        '    If relStrat.ConfirmationType = WMS.Lib.CONFIRMATIONTYPE.SKULOCATION Then
        '        DO1.setVisibility("CONFIRMSKU", True)
        '    Else
        '        DO1.setVisibility("CONFIRMSKU", False)
        '    End If
        'End If
    End Sub


    Private Sub addConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.AddTextboxLine("CONFIRM(LOAD)")
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.AddTextboxLine("CONFIRM(SKU)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.AddTextboxLine("CONFIRM(UPC)")
        End Select
    End Sub

    Private Sub clearConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.Value("CONFIRM(LOAD)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.Value("CONFIRM(SKU)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.Value("CONFIRM(UPC)") = ""
        End Select
    End Sub


    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
            Case "closecontainer"
                doCloseContainer()
        End Select
    End Sub

    Private Function ExtractAttributes() As AttributesCollection
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        Dim Val As Object
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    Val = DO1.Value(pck.oAttributeCollection.Keys(idx))
                    If Val = "" Then Val = Nothing
                    pck.oAttributeCollection(idx) = Val
                Next
                Return pck.oAttributeCollection
            End If
        End If
        Return Nothing
    End Function

    Private Sub ClearAttributes()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    DO1.Value(pck.oAttributeCollection.Keys(idx)) = ""
                Next
            End If
        End If
    End Sub

    Private Sub doCloseContainer()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Session("PCKPicklistActiveContainerID") Is Nothing Then
            MessageQue.Enqueue(trans.Translate("Cannot Close Cotnainer - Container is blank"))
        End If
        Dim pcklist As Picklist = Session("PCKPicklist")
        Dim relStrat As ReleaseStrategyDetail
        relStrat = pcklist.getReleaseStrategy()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        Try
            If relStrat Is Nothing Then
                'Can it happen?
                Return
            End If

            If relStrat.DeliverContainerOnClosing Then
                'Should deliver the container now
                pcklist.CloseContainer(Session("PCKPicklistActiveContainerID"), True, WMS.Logic.GetCurrentUser)
                Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=PCKORD"))
            Else
                'Should close the container - go back to PCK to open a new one
                pcklist.CloseContainer(Session("PCKPicklistActiveContainerID"), False, WMS.Logic.GetCurrentUser)
                Session.Remove("PCKPicklistActiveContainerID")
                Response.Redirect(MapVirtualPath("screens/PCKORD.aspx"))
            End If
        Catch ex As Threading.ThreadAbortException
            'Do Nothing.
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            Return
        End Try
    End Sub

End Class
