Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class RPK2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            Session.Remove("pwjob")
            Session.Remove("pwtask")
            Session.Remove("MobileSourceScreen")
            Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
            checkPutaway()
        End If
    End Sub

    Private Sub checkPutaway()
        If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY) Then
            goBack()
        End If
        Dim pwtask As PutawayTask
        Dim tm As New WMS.Logic.TaskManager
        pwtask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY)
        ' Check if the containes Putaway jobs, if no then close task and go bak
        pwtask.EDITUSER = WMS.Logic.GetCurrentUser
        If Session("LOADIDONCONTAINER") Is Nothing Then
            If pwtask.getPutawayJob Is Nothing Then
                pwtask.Complete()
                goBack()
            End If
        Else
            If pwtask.getPutawayJob(Session("LOADIDONCONTAINER")) Is Nothing Then
                pwtask.Complete()
                goBack()
            End If
        End If

        If Not pwtask Is Nothing Then
            'if we came from RPK1 - check if we have the multiple loads collection and remove current load from it..
            Dim oLoadCollection As ArrayList = Session("RPKLoadsCollection")
            If Not oLoadCollection Is Nothing Then
                If oLoadCollection.Count > 0 Then
                    Dim strLoadid As String = pwtask.FROMLOAD
                    oLoadCollection.Remove(strLoadid)
                    Session("RPKLoadsCollection") = oLoadCollection
                End If
            End If
            Session("pwtask") = pwtask
            If Session("LOADIDONCONTAINER") Is Nothing Then
                setPutaway(pwtask.getPutawayJob)
            Else
                setPutaway(pwtask.getPutawayJob(Session("LOADIDONCONTAINER")))
            End If
        End If
    End Sub

    Public Sub setPutaway(ByVal pwjob As PutawayJob)
        Session("pwjob") = pwjob
        If pwjob.isContainer Then
            DO1.setVisibility("CONSIGNEE", False)
            DO1.setVisibility("SKU", False)
            DO1.setVisibility("SKUDESC", False)
            DO1.setVisibility("UOM", False)
            DO1.setVisibility("UOMUNITS", False)
            DO1.setVisibility("UNITS", False)
        Else
            DO1.setVisibility("CONSIGNEE", True)
            DO1.setVisibility("SKU", True)
            DO1.setVisibility("SKUDESC", True)
            DO1.setVisibility("UOM", True)
            DO1.setVisibility("UOMUNITS", True)
            DO1.setVisibility("UNITS", True)
            DO1.setVisibility("HANDLINGUNIT", True)
            DO1.setVisibility("HANDLINGUNITTYPE", True)

            DO1.Value("CONSIGNEE") = pwjob.Consignee
            DO1.Value("SKU") = pwjob.Sku
            DO1.Value("SKUDESC") = pwjob.skuDesc
            DO1.Value("UOM") = pwjob.UOM
            DO1.Value("UOMUNITS") = pwjob.UOMUnits
            DO1.Value("UNITS") = pwjob.Units
            Dim dd As Made4Net.WebControls.MobileDropDown
            dd = DO1.Ctrl("HANDLINGUNITTYPE")
            dd.AllOption = False
            dd.TableName = "handelingunittype"
            dd.ValueField = "container"
            dd.TextField = "containerdesc"
            dd.DataBind()
        End If

        If pwjob.IsHandOff Then
            'Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            'DO1.setVisibility("Note", True)
            'DO1.Value("Note") = trans.Translate("Task Destination Location is an Hand Off Location!")
            'Instead - go to the container pw screen and deliver the who;e container
        Else
            'DO1.setVisibility("Note", False)
        End If
        DO1.Value("LOADID") = pwjob.LoadId
        DO1.Value("LOCATION") = pwjob.toLocation
    End Sub

    Private Sub goBack()
        Dim srcScreen As String
        Try
            srcScreen = Session("MobileSourceScreen")
            Dim pwtask As PutawayTask
            Dim tm As New WMS.Logic.TaskManager
            pwtask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY)
            If Not pwtask Is Nothing Then
                pwtask.ExitTask()
            End If
            Dim cntId As String = Session("CONTAINERID")

            Session.Remove("CONTAINERID")
        Catch ex As Exception
        End Try
        If srcScreen = "" Or srcScreen Is Nothing Then
            Response.Redirect(MapVirtualPath("screens/RPK.aspx"))
        ElseIf srcScreen.ToLower() = "ldpwcnf" Then
            Response.Redirect(MapVirtualPath("screens/TaskManager.aspx"))
        Else
            Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
        End If
    End Sub

    Private Sub doNext()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim pwjob As PutawayJob = Session.Item("pwjob")
        Dim oCont As WMS.Logic.Container
        Dim sTaskType As String

        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If pwjob.toLocation.ToLower() <> DO1.Value("CONFIRM").ToLower() Then
            MessageQue.Enqueue(trans.Translate("Location confirmation incorrect. Use override button instead."))
            Return
        End If

        Try
            If Not pwjob.isContainer Then
                If WMS.Logic.Container.Exists(DO1.Value("HANDLINGUNIT")) Then
                    oCont = New WMS.Logic.Container(DO1.Value("HANDLINGUNIT"), True)
                Else
                    If DO1.Value("HANDLINGUNIT") <> "" Then
                        oCont = New WMS.Logic.Container
                        oCont.ContainerId = DO1.Value("HANDLINGUNIT")
                        oCont.HandlingUnitType = DO1.Value("HANDLINGUNITTYPE")
                        oCont.Location = pwjob.toLocation
                        oCont.Post(WMS.Logic.Common.GetCurrentUser)
                    End If
                End If
            End If
            'Check if the location is correct , if not print an error
            Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PUTAWAY)
            sTaskType = CType(tm.Task, WMS.Logic.PutawayTask).TASKTYPE
            CType(tm.Task, WMS.Logic.PutawayTask).Put(pwjob, DO1.Value("CONFIRM"), "", oCont)
            Session.Remove("LOADIDONCONTAINER")
            DO1.Value("CONFIRM") = ""
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            Return
        End Try
        If Session("MobileSourceScreen") = "RPKC1" AndAlso Not sTaskType.Equals(WMS.Lib.TASKTYPE.CONTLOADPUTAWAY, StringComparison.OrdinalIgnoreCase) Then
            goBack()
        End If
        checkPutaway()
    End Sub

    Private Sub doOverride()
        Session("Mode") = "Override"
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim pwjob As PutawayJob = Session.Item("pwjob")
        Try
            'Check if the location is correct , if not print an error
            If DO1.Value("CONFIRM").ToLower <> pwjob.toLocation.ToLower Then
                Dim ld As New WMS.Logic.Load(pwjob.LoadId)
                Dim lc As New WMS.Logic.Location(pwjob.toLocation)
                '   lc.CancelPut(ld, WMS.Logic.Common.GetCurrentUser)

                Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PUTAWAY)

                CType(tm.Task, WMS.Logic.PutawayTask).Put(pwjob, DO1.Value("CONFIRM"), ld.SUBLOCATION)
                DO1.Value("CONFIRM") = ""
            Else
                MessageQue.Enqueue("Location Confirmation correct , use next to put in prepeared location")
                Return
            End If
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            Return
        End Try
        If Session("MobileSourceScreen") = "RPKC1" Then
            goBack()
        End If
        checkPutaway()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        'DO1.AddLabelLine("Note")
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UNITS")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        DO1.AddTextboxLine("HANDLINGUNIT")
        DO1.AddDropDown("HANDLINGUNITTYPE")
        DO1.AddTextboxLine("CONFIRM")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "override"
                doOverride()
            Case "back"
                goBack()
        End Select
    End Sub
End Class
