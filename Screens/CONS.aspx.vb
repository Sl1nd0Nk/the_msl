Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class CONS
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then

        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("FROMLOAD")
        DO1.AddTextboxLine("FROMCONTAINER")
        DO1.AddTextboxLine("TOCONTAINER")
        DO1.AddSpacer()
    End Sub

    Private Sub doNext()

        Dim tocnt As WMS.Logic.Container
        Dim fromcont As WMS.Logic.Container
        Dim fromload As WMS.Logic.Load
        Dim cons As WMS.Logic.Consolidation
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            tocnt = New WMS.Logic.Container(DO1.Value("TOCONTAINER"), True)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(t.Translate(ex.Message))
            Return
        End Try

        If Not DO1.Value("FROMLOAD") = String.Empty Then
            Try
                fromload = New WMS.Logic.Load(DO1.Value("FROMLOAD"))
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Return
            Catch ex As Exception
                MessageQue.Enqueue(t.Translate(ex.Message))
                Return
            End Try

            Try
                cons = New WMS.Logic.Consolidation
                cons.Consolidate(fromload, tocnt, WMS.Logic.Common.GetCurrentUser)
                MessageQue.Enqueue(t.Translate("Load Consolidated"))
            Catch ex As Exception
                Return
            End Try
        ElseIf Not DO1.Value("FROMCONTAINER") Is String.Empty Then
            Try
                fromcont = New WMS.Logic.Container(DO1.Value("FROMCONTAINER"), True)
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Return
            Catch ex As Exception
                MessageQue.Enqueue(t.Translate(ex.Message))
                Return
            End Try

            Try
                cons = New WMS.Logic.Consolidation
                cons.Consolidate(fromcont, tocnt, WMS.Logic.Common.GetCurrentUser)
                MessageQue.Enqueue(t.Translate("Container Consolidated"))
            Catch ex As Exception
                Return
            End Try
        Else
            MessageQue.Enqueue(t.Translate("Load or Container not entered"))
            Return
        End If
        DO1.Value("FROMLOAD") = ""
        DO1.Value("FROMCONTAINER") = ""
    End Sub

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
