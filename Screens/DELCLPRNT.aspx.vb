Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

'Packing List Print Screen
Public Class DELPLPRNT
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If Session("PCKPicklist") Is Nothing And Session("PARPCKPicklist") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/DEL.aspx?sourcescreen=" & Session("MobileSourceScreen") & "&printed=1"))
            End If
            If Not Session("PCKPicklist") Is Nothing Then
                Dim oPicklist As WMS.Logic.Picklist = Session("PCKPicklist")
                If Not oPicklist.ShouldPrintContentList Then
                    leaveScreen()
                End If
            ElseIf Not Session("PARPCKPicklist") Is Nothing Then
                Dim pck As ParallelPicking = Session("PARPCKPicklist")
                If Not pck.ShouldPrintContentList Then
                    'Response.Redirect(MapVirtualPath("Screens/DEL.aspx?sourcescreen=" & Session("MobileSourceScreen") & "&printed=1"))
                    leaveScreen()
                End If
            End If
        End If
    End Sub

    Private Sub leaveScreen()
        If Not Session("MobileSourceScreen").ToString().StartsWith("PCKNPP_", StringComparison.OrdinalIgnoreCase) Then
            Response.Redirect(MapVirtualPath("Screens/DEL.aspx?sourcescreen=" & Session("MobileSourceScreen") & "&printed=1"))
        Else
            Session("MobileSourceScreen") = Session("MobileSourceScreen").ToString().Substring("PCKNPP_".Length)

            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.GetCurrentUser, WMS.Lib.TASKTYPE.REPLENISHMENT) Then
                Session("NppRepl") = 1
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/REPL.aspx?sourcescreen=" & Session("MobileSourceScreen")))
            Else
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/RPK2.aspx?sourcescreen=" & Session("MobileSourceScreen")))
            End If
        End If
    End Sub


    Private Sub doNext()
        Dim prntr As String
        Try
            prntr = DO1.Value("ContentListPrinter")
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.ToString())
            Return
        End Try
        If Not Session("PCKPicklist") Is Nothing Then
            Dim oPicklist As WMS.Logic.Picklist = Session("PCKPicklist")
            oPicklist.PrintContentList(prntr, WMS.Logic.GetCurrentUser, Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            If oPicklist.isCompleted Then
                Session.Remove("PCKPicklist")
            End If
        ElseIf Not Session("PARPCKPicklist") Is Nothing Then
            Dim pck As ParallelPicking = Session("PARPCKPicklist")
            pck.PrintContentList(prntr, WMS.Logic.GetCurrentUser, Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            If pck.Completed Then
                Session.Remove("PARPCKPicklist")
            End If
        End If
        
        'Response.Redirect(MapVirtualPath("Screens/DEL.aspx?sourcescreen=" & Session("MobileSourceScreen") & "&printed=1"))
        leaveScreen()
    End Sub

    Private Sub doSkip()
        'Response.Redirect(MapVirtualPath("Screens/DEL.aspx?sourcescreen=" & Session("MobileSourceScreen") & "&printed=1"))
        leaveScreen()
    End Sub

    Private Sub doBack()
        Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen") & "&printed=1"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddSpacer()
        DO1.AddTextboxLine("ContentListPrinter")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "next"
                doNext()
            Case "skip"
                doSkip()
        End Select
    End Sub

End Class
