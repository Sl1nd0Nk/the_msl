Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic

Partial Public Class GateCheckInAppointmentInfo
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            SetScreen()
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("AppointmentId")
        DO1.AddLabelLine("ScheduleDate")
        DO1.AddLabelLine("Vehicle")
        DO1.AddLabelLine("Trailer")
        DO1.AddLabelLine("YardLocation")
        DO1.AddSpacer()
        DO1.AddSpacer()
        DO1.AddTextboxLine("CheckInYardLocation", "Scan Gate")
    End Sub

    Private Sub SetScreen()
        If Not Session("YardAppointment") Is Nothing Then
            Dim oApp As WMS.Logic.YardAppointment = Session("YardAppointment")
            DO1.Value("AppointmentId") = oApp.APPOINTMENTID
            DO1.Value("Vehicle") = oApp.VEHICLE
            DO1.Value("Trailer") = oApp.TRAILER
            DO1.Value("YardLocation") = oApp.DOOR
            DO1.Value("ScheduleDate") = oApp.SCHEDULEDATE.ToString(String.Format("{0} {1}", Made4Net.Shared.SysParam.Get("System_DateFormat"), Made4Net.Shared.SysParam.Get("System_TimeFormat")))
        Else
            DO1.Value("AppointmentId") = ""
            DO1.Value("Vehicle") = Session("YardGateCheckVehicleId")
            DO1.Value("Trailer") = Session("YardGateCheckTrailerId")
            DO1.Value("YardLocation") = ""
            DO1.Value("ScheduleDate") = DateTime.Now.ToString(String.Format("{0} {1}", Made4Net.Shared.SysParam.Get("System_DateFormat"), Made4Net.Shared.SysParam.Get("System_TimeFormat")))
        End If
        'DO1.Value("CheckInYardLocation") = Session("YardAppointmentCheckInLocation")
    End Sub

    Private Sub NextClicked_old()
        Try
            Dim oTranslator As New Made4Net.Shared.Translation.Translator
            If DO1.Value("CheckInYardLocation") = String.Empty Then
                MessageQue.Enqueue(oTranslator.Translate("Check in location cannot be blank"))
                Return
            End If
            If Not WMS.Logic.YardLocation.Exists(DO1.Value("CheckInYardLocation")) Then
                MessageQue.Enqueue(oTranslator.Translate("Check in location does not exist"))
                DO1.Value("CheckInYardLocation") = ""
                Return
            End If
            If Not Session("YardAppointment") Is Nothing Then
                Dim oApp As WMS.Logic.YardAppointment = Session("YardAppointment")
                If Session("YardGateCheckSourceScreen") = "GateCheckOut" Then
                    'oApp.CheckOut(DO1.Value("CheckInYardLocation"), WMS.Logic.Common.GetCurrentUser)
                Else
                    'oApp.CheckIn(DO1.Value("CheckInYardLocation"), WMS.Logic.Common.GetCurrentUser)
                End If
                'MessageQue.Enqueue(oTranslator.Translate(String.Format("Appoimtment checked in successfully")))
            ElseIf Session("YardGateCheckSourceScreen") = "GateCheckOut" Then
                'Dim oCheckOut As New WMS.Logic.YardCheckOut()
                'oCheckOut.Create("", DateTime.Now, DO1.Value("Vehicle"), DO1.Value("Trailer"), "", "", DO1.Value("CheckInYardLocation"), "", "", WMS.Logic.Common.GetCurrentUser)
            ElseIf Session("YardGateCheckSourceScreen") = "GateCheckIn" Then
                'Dim oCheckIn As New WMS.Logic.YardCheckIn()
                'oCheckIn.Create("", DateTime.Now, DO1.Value("Vehicle"), DO1.Value("Trailer"), "", "", DO1.Value("CheckInYardLocation"), "", "", WMS.Logic.Common.GetCurrentUser)
            End If
            GoBack()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
    End Sub
    Private Sub NextClicked()
        Try
            Dim oTranslator As New Made4Net.Shared.Translation.Translator
            If DO1.Value("CheckInYardLocation") = String.Empty Then
                MessageQue.Enqueue(oTranslator.Translate("Check in location cannot be blank"))
                Return
            End If
            If Not WMS.Logic.YardLocation.Exists(DO1.Value("CheckInYardLocation")) Then
                MessageQue.Enqueue(oTranslator.Translate("Check in location does not exist"))
                DO1.Value("CheckInYardLocation") = ""
                Return
            End If
            If Not Session("YardAppointment") Is Nothing Then
                Dim oApp As WMS.Logic.YardAppointment = Session("YardAppointment")
                ' oApp.CheckIn(DO1.Value("CheckInYardLocation"), WMS.Logic.Common.GetCurrentUser)
                oApp.CheckIn(DO1.Value("CheckInYardLocation"), oApp.VEHICLE, oApp.TRAILER, oApp.CARRIER, oApp.SEAL1, oApp.SEAL2, oApp.DRIVER1, oApp.DRIVER2, WMS.Logic.Common.GetCurrentUser)
                MessageQue.Enqueue(oTranslator.Translate(String.Format("Appointment checked in successfully")))
            End If
            GoBack()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
    End Sub

    Private Sub GoBack()
        If Session("yardGateCheckSourceScreen") Is Nothing Then
            Made4Net.Mobile.Common.GoToMenu()
        End If
        Response.Redirect(MapVirtualPath(String.Format("Screens/{0}.aspx", Session("YardGateCheckSourceScreen"))))
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                NextClicked()
            Case "back"
                GoBack()
        End Select
    End Sub

End Class