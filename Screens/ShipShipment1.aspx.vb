Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class ShipShipment1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If Not Session("SHIPMENTSHPOBJ") Is Nothing Then
                SetScreen()
            Else
                doBack()
            End If
        End If
    End Sub

    Private Sub doMenu()
        Session.Remove("SHIPMENTSHPOBJ")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doBack()
        Response.Redirect(MapVirtualPath("Screens/ShipShipment.aspx"))
    End Sub

    Private Sub SetScreen()
        Dim oShip As WMS.Logic.Shipment = Session("SHIPMENTSHPOBJ")
        DO1.Value("SHIPMENT") = oShip.SHIPMENT
        DO1.Value("SCHEDDATE") = oShip.SCHEDDATE
        DO1.Value("STATUS") = DataInterface.ExecuteScalar(String.Format("select [description] from codelistdetail where codelistcode = 'SHPSTAT' and code = '{0}'", oShip.STATUS))
        DO1.Value("VEHICLE") = oShip.Vehicle
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim oShip As WMS.Logic.Shipment = Session("SHIPMENTSHPOBJ")
            oShip.Ship(WMS.Logic.Common.GetCurrentUser)
            'Go Back To Scan another orderid
            doBack()
        Catch ex As System.Threading.ThreadAbortException
            'Do nothing
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            MessageQue.Enqueue(t.Translate(ex.Message))
        End Try
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("SHIPMENT")
        DO1.AddLabelLine("SCHEDDATE")
        DO1.AddLabelLine("STATUS")
        DO1.AddLabelLine("VEHICLE")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
            Case "back"
                doBack()
        End Select
    End Sub

End Class
