Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic
Imports WMS.Lib

<CLSCompliant(False)> Public Class TaskManager
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.Common.GetCurrentUser Is Nothing Or WMS.Logic.Common.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            Session.Remove("TargetScreen")
            Session.Remove("TMTask")
            Session.Remove("ShowedTaskManager")
            If Not Session("TASKMANAGERSHOULDREDIRECTTOSRC") Is Nothing Then
                If Session("TASKMANAGERSHOULDREDIRECTTOSRC") = "0" Then
                    Session("TASKMANAGERSHOULDREDIRECTTOSRC") = "1"
                Else
                    If Not Session("TASKMANAGERSOURCESCREENID") Is Nothing Then
                        Dim url As String = MapVirtualPath(Made4Net.DataAccess.DataInterface.ExecuteScalar(String.Format("select url from sys_screen where screen_id = '{0}'", Session("TASKMANAGERSOURCESCREENID")), Made4Net.Schema.CONNECTION_NAME))
                        Response.Redirect(url)
                    End If
                End If
            End If
            If Not CheckAssigned() Then
                NextClicked()
            End If
            'CheckAssigned()
        End If
        DO1.DefaultButton = DO1.LeftButtonText
    End Sub

    Private Sub MenuClick()
        'Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        'If WMS.Logic.TaskManager.isAssigned(UserId) Then
        '    Dim tm As New WMS.Logic.TaskManager(UserId, "")
        '    tm.ExitTask()
        'End If
        If Not Session("TMTask") Is Nothing Then
            Dim oTask As Task = Session("TMTask")
            oTask.ExitTask()
            Session.Remove("TMTask")
        End If
        Session.Remove("ShowedTaskManager")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub NextClicked()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        If Not String.IsNullOrEmpty(Session("TargetScreen")) Then
            Response.Redirect(MapVirtualPath(Session("TargetScreen")))
        End If
        If Not WMS.Logic.TaskManager.isAssigned(UserId) Then
            Try
                'Dim tm As New WMS.Logic.TaskManager
                'tm.RequestTask(UserId)
                Dim oTask As Task = RequestTask()
                CheckAssigned()
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Catch ex As Exception
                MessageQue.Enqueue(t.Translate(ex.Message))
            End Try
        Else
            Dim tm As New WMS.Logic.TaskManager
            Dim oTask As Task = Logic.TaskManager.getUserAssignedTask(UserId)
            If Not oTask Is Nothing Then
                Session("TMTaskId") = oTask
                Session("MobileSourceScreen") = "TaskManager"
                Session("ShowedTaskManager") = True
                Select Case oTask.TASKTYPE.ToUpper
                    Case WMS.Lib.TASKTYPE.CONSOLIDATION
                        Response.Redirect(MapVirtualPath("Screens/CONS.aspx"))
                    Case WMS.Lib.TASKTYPE.CONSOLIDATIONDELIVERY
                        Response.Redirect(MapVirtualPath("Screens/CONS.aspx"))
                    Case WMS.Lib.TASKTYPE.CONTCONTDELIVERY
                        Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
                    Case WMS.Lib.TASKTYPE.CONTDELIVERY
                        Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
                    Case WMS.Lib.TASKTYPE.CONTLOADDELIVERY
                        Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
                    Case WMS.Lib.TASKTYPE.CONTLOADPUTAWAY
                        Response.Redirect(MapVirtualPath("Screens/RPKC.aspx"))
                    Case WMS.Lib.TASKTYPE.CONTPUTAWAY
                        Response.Redirect(MapVirtualPath("Screens/CNTPWCNF.aspx"))
                    Case WMS.Lib.TASKTYPE.LOADCOUNTING
                        Response.Redirect(MapVirtualPath("Screens/CNTTASK.aspx?SourceScreen=tm"))
                    Case WMS.Lib.TASKTYPE.DELIVERY
                        Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
                    Case WMS.Lib.TASKTYPE.FULLPICKING
                        Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
                    Case WMS.Lib.TASKTYPE.PARTREPL
                        Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
                    Case WMS.Lib.TASKTYPE.LOADDELIVERY
                        Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
                    Case WMS.Lib.TASKTYPE.LOADPUTAWAY
                        Response.Redirect(MapVirtualPath("Screens/LDPWCNF.aspx"))
                    Case WMS.Lib.TASKTYPE.NEGTREPL
                        Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
                    Case WMS.Lib.TASKTYPE.FULLREPL
                        Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
                    Case WMS.Lib.TASKTYPE.PARALLELPICKING
                        Response.Redirect(MapVirtualPath("Screens/PARPCK1.aspx?SourceScreen=TaskManager"))
                    Case WMS.Lib.TASKTYPE.PARTIALPICKING
                        Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
                    Case WMS.Lib.TASKTYPE.NEGPALLETPICK
                        Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
                    Case WMS.Lib.TASKTYPE.PICKING
                        Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
                    Case WMS.Lib.TASKTYPE.PUTAWAY
                        Response.Redirect(MapVirtualPath("Screens/RPK.aspx"))
                    Case WMS.Lib.TASKTYPE.REPLENISHMENT
                        Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
                    Case WMS.Lib.TASKTYPE.LOCATIONBULKCOUNTING
                        Response.Redirect(MapVirtualPath("Screens/LOCBLKCNTTASK.aspx"))
                    Case WMS.Lib.TASKTYPE.EMPTYHUPICKUPTASK
                        Response.Redirect(MapVirtualPath("Screens/EVACHU.aspx"))
                    Case WMS.Lib.TASKTYPE.LOCATIONCOUNTING
                        Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK.aspx"))
                    Case WMS.Lib.TASKTYPE.SPICKUP
                        Response.Redirect(MapVirtualPath("Screens/SpecPickup.aspx"))
                End Select
            Else
                setNotAssigned()
            End If
        End If
    End Sub

    Private Function CheckAssigned() As Boolean
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim tm As New WMS.Logic.TaskManager

        If Not (Session("TMTask") Is Nothing) Then
            Dim oTask As Task = Session("TMTask")
            If oTask.STATUS <> WMS.Lib.Statuses.Task.COMPLETE And _
                oTask.STATUS <> WMS.Lib.Statuses.Task.CANCELED Then
                setAssigned(oTask)
                Return True
            End If
        End If


        If Logic.TaskManager.isAssigned(UserId) Then
            Dim oTask As Task
            Try
                oTask = Logic.TaskManager.getUserAssignedTask(UserId)
            Catch ex As Exception
            End Try
            If Not oTask Is Nothing Then
                Session("TMTask") = oTask
                setAssigned(oTask)
                Return True
            Else
                setNotAssigned()
                Return False
            End If
        Else
            setNotAssigned()
            Return False
        End If
    End Function

    Protected Sub setNotAssigned()
        Session("TaskId") = Nothing
        DO1.Value("Assigned") = "Not Assigned"
        DO1.setVisibility("TaskId", False)
        DO1.setVisibility("TaskType", False)
        'DO1.setVisibility("PickListId", False)
        'DO1.RightButtonText = "RequestTask"
        DO1.LeftButtonText = "RequestTask"
        If WMS.Logic.GetSysParam("ShowGoalTimeOnTaskAssignment") = 1 Then
            DO1.setVisibility("TaskSubType", False)
            DO1.setVisibility("Priority", False)
            DO1.setVisibility("FromLocation", False)
            DO1.setVisibility("AssignedTime", False)
            DO1.setVisibility("STDTIME", False)
        End If
    End Sub

    Protected Sub setAssigned(ByVal oTask As Task)
        DO1.Value("Assigned") = "Assigned"
        DO1.setVisibility("TaskId", True)
        DO1.setVisibility("TaskType", True)
        'DO1.setVisibility("PickListId", False)
        DO1.Value("TaskId") = oTask.TASK
        DO1.Value("TaskType") = oTask.TASKTYPE
        Session("TaskId") = oTask.TASK
        If WMS.Logic.GetSysParam("ShowGoalTimeOnTaskAssignment") = 1 Then
            DO1.setVisibility("TaskSubType", True)
            'DO1.Value("TaskSubType") = oTask.TASKSUBTYPE
            DO1.setVisibility("Priority", True)
            DO1.Value("Priority") = oTask.PRIORITY
            DO1.setVisibility("FromLocation", True)
            DO1.Value("FromLocation") = oTask.FROMLOCATION
            DO1.setVisibility("AssignedTime", True)
            DO1.Value("AssignedTime") = oTask.ASSIGNEDTIME
            DO1.setVisibility("STDTIME", True)

            Dim stdTimeStr As String
            Dim secondsStr As String = (oTask.STDTIME Mod 60).ToString()
            If secondsStr.Length = 1 Then
                secondsStr = secondsStr.PadLeft(2, "0")
            End If
            stdTimeStr = (Math.Floor(oTask.STDTIME / 60)).ToString() & ":" & secondsStr
            DO1.Value("STDTIME") = stdTimeStr
        End If

        DO1.LeftButtonText = "Next"

    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("Assigned")
        DO1.AddLabelLine("TaskId")
        DO1.AddLabelLine("TaskType")
        'DO1.AddTextboxLine("PickListId")
        If WMS.Logic.GetSysParam("ShowGoalTimeOnTaskAssignment") = 1 Then
            DO1.AddLabelLine("TaskSubType")
            DO1.AddLabelLine("Priority")
            DO1.AddLabelLine("FromLocation")
            DO1.AddLabelLine("AssignedTime")
            DO1.AddLabelLine("STDTIME")
        End If

    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                NextClicked()
            Case "requesttask"
                NextClicked()
            Case "menu"
                MenuClick()
        End Select
    End Sub

    Private Function RequestTask() As Task
        Dim ts As New WMS.Logic.TaskManager
        Return ts.GetTaskFromTMService(WMS.Logic.Common.GetCurrentUser)
    End Function

End Class
