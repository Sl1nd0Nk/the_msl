Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess

Imports Wms.Logic

Partial Public Class vrfi
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub DOVerify_ButtonClick(ByVal sender As System.Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DOVerify.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

    Protected Sub DOVerify_CreatedChildControls(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DOVerify.CreatedChildControls
        DOVerify.AddTextboxLine("LoadID")
        DOVerify.AddTextboxLine("ContainerID")
    End Sub

    Private Sub doNext()
        'If Page.IsValid Then
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If DOVerify.Value("LoadID") <> "" And DOVerify.Value("ContainerID") <> "" Then
            MessageQue.Enqueue(t.Translate("Both load id and container id field are filled."))
            Return
        End If

        If DOVerify.Value("LoadID") <> "" Then
            If Not WMS.Logic.Load.Exists(DOVerify.Value("LoadID")) Then
                MessageQue.Enqueue(t.Translate("The load id entered does not exist."))
                Return
            End If
            Dim lObj As New WMS.Logic.Load(DOVerify.Value("LoadID"))
            lObj.Verify(WMS.Logic.Common.GetCurrentUser())
            MessageQue.Enqueue(t.Translate("Load verified"))
        ElseIf DOVerify.Value("ContainerID") <> "" Then
            If Not WMS.Logic.Container.Exists(DOVerify.Value("ContainerID")) Then
                MessageQue.Enqueue(t.Translate("The container id entered does not exist."))
                Return
            End If
            Dim contaObj As New WMS.Logic.Container(DOVerify.Value("ContainerID"), True)
            For Each load As WMS.Logic.Load In contaObj.Loads
                load.Verify(WMS.Logic.Common.GetCurrentUser)
            Next
            MessageQue.Enqueue(t.Translate("Container verified"))
        End If
        'End If

        DOVerify.Value("ContainerID") = ""
        DOVerify.Value("LoadID") = ""
    End Sub

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

End Class