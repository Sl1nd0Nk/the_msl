Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared

<CLSCompliant(False)> Public Class DEL
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            Dim CaseDelivery As Boolean = CheckForCaseDeliveries()
            If Not Request.QueryString("sourcescreen") Is Nothing Then
                Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
            End If

            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY) Then
                doBack()
            End If

            If CaseDelivery = False Then
                Dim delobj As DeliveryJob
                Try
                    Dim tm As New WMS.Logic.TaskManager
                    Dim deltsk As DeliveryTask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY)

                    If deltsk Is Nothing Then
                        doBack()
                    Else
                        If Request.QueryString("printed") = 1 Then
                            Session("printed") = 1
                        End If
                        'If deltsk.ShouldPrintShipLabelOnPicking And deltsk.TASKTYPE = WMS.Lib.TASKTYPE.CONTCONTDELIVERY And Session("printed") = 0 Then
                        '    Session("PCKDeliveryTask") = deltsk
                        '    Session("printed") = 1
                        '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
                        'End If
                    End If
                    Session("PCKDeliveryTask") = deltsk
                    delobj = deltsk.getDeliveryJob()
                    Session("printed") = 0
                    setDelivery(delobj)
                Catch ex As Exception
                End Try
            Else
                SetupCaseDeliveryScreen()
            End If
        End If
    End Sub

    Private Function CheckForCaseDeliveries() As Boolean
        Dim Found As Boolean = False

        Dim SQL As String = String.Format("select * from tasks where userid = '{0}' and assigned = 1 and status = '{1}' and TASKTYPE='{2}' and TOLOCATION='{3}'", WMS.Logic.Common.GetCurrentUser, "ASSIGNED", "CONTDEL", "CASE CONVEYOR")
        Dim dt As New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt)

        If dt.Rows.Count > 0 Then
            Session.Add("ManualDelivery", "CASE CONVEYOR")
            Found = True
        End If

        Return Found
    End Function

    Private Function GetTaskByContainerId(ByVal CaseCont As String) As String
        Dim TaskId As String = ""

        Dim SQL As String = String.Format("select * from tasks where userid = '{0}' and assigned = 1 and status = '{1}' and TASKTYPE='{2}' and TOLOCATION='{3}' and FROMCONTAINER='{4}' and TOCONTAINER='{5}'", WMS.Logic.Common.GetCurrentUser, "ASSIGNED", "CONTDEL", "CASE CONVEYOR", CaseCont, CaseCont)
        Dim dt As New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt)

        If dt.Rows.Count > 0 Then
            TaskId = dt.Rows(0)("TASK")
        End If

        Return TaskId
    End Function

    Private Sub SetupCaseDeliveryScreen()
        ToggleVisibility(False, True)
        DO1.Value("CASE_INSTR") = "Scan Case To Confirm To " & Session("ManualDelivery")
        DO1.FocusField = "CASE_CONT"
    End Sub

    Public Sub setDelivery(ByVal delobj As DeliveryJob)
        If delobj Is Nothing Then
            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY) Then
                Dim deltask As Logic.DeliveryTask
                Dim tm As New WMS.Logic.TaskManager
                deltask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY)
                deltask.EDITUSER = WMS.Logic.GetCurrentUser
                deltask.Complete()
            End If
            doBack()
        End If
        Session.Item("PCKDeliveryJob") = delobj
        Dim deltsk As DeliveryTask = Session("PCKDeliveryTask")

        Dim ct As String = delobj.LoadId
        If CheckIFInProgress(delobj.Picklist, ct) Then
            Session.Add("JobType", "DELIVERYJOB")
            Session.Add("JOB", "PCK")
            Session.Add("CaseContScanPickList", delobj.Picklist)
            Session.Add("CaseContLoadCont", ct)
            Response.Redirect(MapVirtualPath("screens/PCKCASECONT.aspx?sourcescreen=" & "PCK"))
        End If

        ToggleVisibility(True, False)
        DO1.FocusField = "CONFIRM"

        If delobj.isContainer Then
            DO1.setVisibility("SKU", False)
            DO1.setVisibility("SKUDESC", False)
            DO1.setVisibility("UOM", False)
            DO1.setVisibility("UOMUNITS", False)
            DO1.setVisibility("HANDLINGUNIT", False)
            DO1.setVisibility("HANDLINGUNITTYPE", False)
        Else
            DO1.setVisibility("SKU", True)
            DO1.setVisibility("SKUDESC", True)
            DO1.setVisibility("UOM", True)
            DO1.setVisibility("UOMUNITS", True)

            DO1.Value("SKU") = delobj.Sku
            DO1.Value("SKUDESC") = delobj.skuDesc
            '' DO1.Value("UOM") = delobj.UOM
            ''DO1.Value("UOMUNITS") = delobj.UOMUnits
            Dim sUom As String = delobj.UOM
            DO1.Value("UOMUNITS") = SetUomUnitsText(delobj, sUom)
            DO1.Value("UOM") = sUom
            DO1.setVisibility("HANDLINGUNIT", True)
            DO1.setVisibility("HANDLINGUNITTYPE", True)
            Dim dd As Made4Net.WebControls.MobileDropDown
            dd = DO1.Ctrl("HANDLINGUNITTYPE")
            dd.AllOption = False
            dd.TableName = "handelingunittype"
            dd.ValueField = "container"
            dd.TextField = "containerdesc"
            dd.DataBind()
        End If

        If deltsk.TASKTYPE = WMS.Lib.TASKTYPE.CONTCONTDELIVERY Then
            DO1.setVisibility("SEQ", True)
            DO1.Value("SEQ") = delobj.OrderSeq
        Else
            DO1.setVisibility("SEQ", False)
        End If
        'Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If delobj.IsHandOff Then
            'DO1.setVisibility("Note", True)
            'DO1.Value("Note") = trans.Translate("Task Destination Location is a Hand Off Location!")
        Else
            'DO1.setVisibility("Note", False)
        End If

        Dim LdId As String = delobj.LoadId
        CheckIfCaseMasterContainer(LdId)

        DO1.Value("LOADID") = LdId
        DO1.Value("CTLOADID") = LdId
        DO1.Value("LOCATION") = delobj.toLocation
    End Sub

    Private Sub CheckIfCaseMasterContainer(ByRef LdId As String)
        Dim FirstRow As Boolean = False
        Dim dt As New DataTable()
        Dim SQL As String = String.Format("select * from apx_case_container_detail where loadContainer = {0}", FormatField(LdId))
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt)
        If dt.Rows.Count > 0 Then
            LdId = ""
            DO1.setVisibility("CTLOADID", True)
            DO1.setVisibility("LOADID", False)
            For Each dr As DataRow In dt.Rows
                If FirstRow = False Then
                    LdId = "[ " & dr("caseContainer") & " ] "
                    FirstRow = True
                Else
                    LdId = LdId & "[ " & dr("caseContainer") & " ] "
                End If
            Next
        Else
            DO1.setVisibility("CTLOADID", False)
            DO1.setVisibility("LOADID", True)
        End If
    End Sub

    Private Function CheckIFInProgress(ByVal PicklistID As String, ByRef ct As String) As Boolean
        Dim dt As New DataTable()
        Dim SQL As String = String.Format("select * from apx_case_container_header where pickList = {0}", FormatField(PicklistID))
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                SQL = String.Format("select * from apx_case_container_detail where pickList = {0} and loadContainer = {1}", FormatField(PicklistID), FormatField(ct))
                Dim dt1 As New DataTable()
                Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt1)
                If dt1.Rows.Count > 0 Then
                    If dt1.Rows.Count < CInt(dr("NumOfCases")) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return True
                End If
            Next
        Else
            Return False
        End If

    End Function

    Private Function GetCaseContainerNumToScan(ByVal pickList As String, ByVal loadContainer As String) As Integer
        Dim Count As Integer = 0
        Dim SQL As String = String.Format("Select * from apx_case_container_header where pickList = {0} and loadContainer = {1}", FormatField(pickList), FormatField(loadContainer))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            If Not IsDBNull(dtLines.Rows(0)("NumOfCases")) Then
                Count = dtLines.Rows(0)("NumOfCases")
            End If
        End If

        Return Count
    End Function

    Private Function GetNextContNoToScan(ByVal pickList As String, ByVal loadContainer As String) As Integer
        Dim Count As Integer = 0
        Dim SQL As String = String.Format("Select * from apx_case_container_detail where pickList = {0} and loadContainer = {1}", FormatField(pickList), FormatField(loadContainer))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        Count = dtLines.Rows.Count + 1

        Return Count
    End Function

    Private Function SetUomUnitsText(ByVal del As WMS.Logic.DeliveryJob, ByRef sUom As String) As String
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim sUomUnits As String = String.Empty
        Dim oSku As New WMS.Logic.SKU(del.Consignee, del.Sku, True)
        Dim sWantedUom As String = del.UOM
        Dim uomunits As Int32 = 0
        Dim dLowerUomUnits As Decimal = 0

        Integer.TryParse(Int(del.UOMUnits).ToString(), uomunits)
        If (uomunits = del.UOMUnits) OrElse String.Equals(del.UOM, oSku.LOWESTUOM, StringComparison.CurrentCultureIgnoreCase) Then '
            If String.Equals(del.UOM, oSku.LOWESTUOM, StringComparison.CurrentCultureIgnoreCase) Then
                sUomUnits = String.Format("{0} {1} ", SetUnitsDisplay(del.Units, del.UOM, oSku.LOWESTUOM), trans.Translate(del.UOM))
            Else
                sUomUnits = String.Format("{2} {3} ({0} {1}) ", SetUnitsDisplay(del.Units, oSku.LOWESTUOM, oSku.LOWESTUOM), oSku.LOWESTUOM, Integer.Parse(Int(del.UOMUnits).ToString()), trans.Translate(del.UOM))
            End If

        Else 'need to find lower suitable uom

            'Dim sUom As String = pck.uom
            Dim currUnits As Decimal
            Dim currUnitsInt As Integer
            Dim continueloop As Boolean = True

            While (continueloop)
                If String.IsNullOrEmpty(WMS.Logic.SKU.SKUUOM.GetSKUUOM(oSku.CONSIGNEE, oSku.SKU, sUom).LOWERUOM) Then
                    sUomUnits = String.Format("{0} {1} ", SetUnitsDisplay(del.Units, sUom, oSku.LOWESTUOM), trans.Translate(sUom))
                    continueloop = False
                Else
                    currUnits = oSku.ConvertUnitsToUom(sUom, del.Units)
                    Integer.TryParse(Int(currUnits), currUnitsInt)
                    If currUnits = currUnitsInt Then
                        sUomUnits = String.Format("{2} {3} ({0} {1}) ", SetUnitsDisplay(del.Units, oSku.LOWESTUOM, oSku.LOWESTUOM), oSku.LOWESTUOM, currUnitsInt, trans.Translate(sUom))
                        continueloop = False
                    Else
                        sUom = WMS.Logic.SKU.SKUUOM.GetSKUUOM(oSku.CONSIGNEE, oSku.SKU, sUom).LOWERUOM
                    End If
                End If

            End While


        End If
        Return sUomUnits
    End Function

    Private Function SetUnitsDisplay(ByVal pUnits As Decimal, ByVal pUom As String, ByVal pLowsetUom As String) As String
        If Not String.Equals(pUom, pLowsetUom, StringComparison.CurrentCultureIgnoreCase) Then
            Return Math.Round(pUnits, 2).ToString()
        End If
        Dim intUnits As Integer = 0 'only for display
        Int32.TryParse(Int(pUnits), intUnits)
        If intUnits = pUnits Then
            Return intUnits.ToString()
        End If
        Return Math.Round(pUnits, 2).ToString()
    End Function

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doBack()
        Dim srcScreen As String
        Try
            srcScreen = Session("MobileSourceScreen")
        Catch ex As Exception

        End Try

        Session.Remove("ManualDelivery")

        If srcScreen.Equals("pckpart", StringComparison.OrdinalIgnoreCase) Then
            Session("MobileSourceScreen") = "TaskManager"
            Response.Redirect(MapVirtualPath("screens/pck.aspx"))
        End If

        Session.Remove("PCKPicklist")
        Dim deltsk As DeliveryTask = Session("PCKDeliveryTask")
        If Not deltsk Is Nothing Then
            If deltsk.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                'If deltsk.ShouldPrintShipLabelOnPicking Then
                '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx"))
                'Else
                If srcScreen = "" Or srcScreen Is Nothing Then
                    Response.Redirect(MapVirtualPath("screens/Main.aspx"))
                Else
                    Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
                End If
                'End If
            ElseIf deltsk.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.AUTOMATIC Then
                'If deltsk.ShouldPrintShipLabelOnPicking Then
                '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx"))
                'Else
                Response.Redirect(MapVirtualPath("screens/TaskManager.aspx"))
                'End If
            End If
        Else
            If srcScreen = "" Or srcScreen Is Nothing Then
                Response.Redirect(MapVirtualPath("screens/Main.aspx"))
            Else
                Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
            End If
        End If
        If srcScreen = "" Or srcScreen Is Nothing Then
            'If deltsk.ShouldPrintShipLabelOnPicking Then
            '    Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx"))
            'Else
            Response.Redirect(MapVirtualPath("screens/Main.aspx"))
            'End If
        Else
            Response.Redirect(MapVirtualPath("screens/" & srcScreen & ".aspx"))
        End If
    End Sub

    Private Sub doNext(Optional ByVal pDoOverride As Boolean = False)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser

        If Session("ManualDelivery") Is Nothing Then
            Dim del As DeliveryJob = Session.Item("PCKDeliveryJob")
            Dim oCont As WMS.Logic.Container
            Dim inputLoc As String
            Try
                If pDoOverride Then
                    If String.Equals(DO1.Value("CONFIRM"), del.toLocation, StringComparison.OrdinalIgnoreCase) Then
                        MessageQue.Enqueue(t.Translate("Confirmation location equals to job target location, use next button instead"))
                        Return
                    End If
                Else
                    If Not String.Equals(DO1.Value("CONFIRM"), del.toLocation, StringComparison.OrdinalIgnoreCase) Then
                        inputLoc = Location.CheckForCheckDigitMatching(del.toLocation, DO1.Value("CONFIRM"))
                        If String.IsNullOrEmpty(inputLoc) Then
                            MessageQue.Enqueue(t.Translate("Confirmation location not equals to job target location, use override button instead"))
                            Return
                        End If
                    End If
                End If
                If Not del.isContainer Then
                    If WMS.Logic.Container.Exists(DO1.Value("HANDLINGUNIT")) Then
                        oCont = New WMS.Logic.Container(DO1.Value("HANDLINGUNIT"), True)
                    Else
                        If DO1.Value("HANDLINGUNIT") <> "" Then
                            oCont = New WMS.Logic.Container
                            oCont.ContainerId = DO1.Value("HANDLINGUNIT")
                            oCont.HandlingUnitType = DO1.Value("HANDLINGUNITTYPE")
                            oCont.Location = del.toLocation
                            oCont.Post(WMS.Logic.Common.GetCurrentUser)
                        End If
                    End If
                End If
                Dim tm As New WMS.Logic.TaskManager(del.TaskId)
                CType(tm.Task, WMS.Logic.DeliveryTask).Deliver(DO1.Value("CONFIRM"), del.IsHandOff, oCont)
                Dim assignType As String = CType(tm.Task, WMS.Logic.DeliveryTask).ASSIGNMENTTYPE
                If assignType = WMS.Lib.TASKASSIGNTYPE.AUTOMATIC Then
                    DO1.Value("CONFIRM") = ""
                    If Not Session("JobType") Is Nothing Then
                        Dim SourceScreen As String = Session("JOB")
                        Response.Redirect(MapVirtualPath("screens/" & SourceScreen & ".aspx"))
                    Else
                        Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
                    End If
                Else
                    If Not Session("JobType") Is Nothing Then
                        Dim SourceScreen As String = Session("JOB")
                        Response.Redirect(MapVirtualPath("screens/" & SourceScreen & ".aspx"))
                    Else
                        Response.Redirect(MapVirtualPath("screens/" & "PCK" & ".aspx"))
                    End If
                End If
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Return
            Catch ex As Exception
                Return
            End Try

            DO1.Value("CONFIRM") = ""
            Dim delobj As DeliveryJob
            Try
                Dim tm As New WMS.Logic.TaskManager
                Dim deltsk As DeliveryTask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY)
                If deltsk Is Nothing Then
                    doBack()
                End If
                Session("PCKDeliveryTask") = deltsk
                delobj = deltsk.getDeliveryJob()
                setDelivery(delobj)
            Catch ex As Exception

            End Try
        Else
            Dim CaseCont As String = DO1.Value("CASE_CONT")
            DO1.Value("CASE_CONT") = ""
            If Not String.IsNullOrEmpty(CaseCont) Then
                Dim TaskId As String = GetTaskByContainerId(CaseCont)

                If Not String.IsNullOrEmpty(TaskId) Then
                    Dim tm As New WMS.Logic.TaskManager(TaskId)
                    CType(tm.Task, WMS.Logic.DeliveryTask).Deliver(Session("ManualDelivery"))
                    Dim assignType As String = CType(tm.Task, WMS.Logic.DeliveryTask).ASSIGNMENTTYPE
                    Session.Remove("ManualDelivery")
                    If assignType = WMS.Lib.TASKASSIGNTYPE.AUTOMATIC Then
                        Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
                    Else
                        Response.Redirect(MapVirtualPath("screens/" & "PCK" & ".aspx"))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Unable to find a task for case container"))
                End If

                Session.Remove("ManualDelivery")
                Response.Redirect(MapVirtualPath("screens/" & "PCK" & ".aspx"))
            Else
                MessageQue.Enqueue(t.Translate("Please enter case container"))
            End If
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        'DO1.AddLabelLine("Note")
        DO1.AddLabelLine("SEQ")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOADID", "PalletID")
        ''DO1.AddLabelLine("CTLOADID", "Case Containers")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UOMUNITS")
        DO1.AddLabelLine("CASE_INSTR", "")
        DO1.AddSpacer()
        DO1.AddTextboxLine("HANDLINGUNIT")
        DO1.AddDropDown("HANDLINGUNITTYPE")
        DO1.AddTextboxLine("CONFIRM")
        DO1.AddTextboxLine("CASE_CONT", "Case Container")
        ''DO1.AddTextboxLine("CASE_CONFIRM")
    End Sub

    Private Sub ToggleVisibility(ByVal normalView As Boolean, ByVal caseScanView As Boolean)
        DO1.setVisibility("SEQ", normalView)
        DO1.setVisibility("SKU", normalView)
        DO1.setVisibility("SKUDESC", normalView)
        DO1.setVisibility("LOADID", normalView)
        DO1.setVisibility("LOCATION", normalView)
        DO1.setVisibility("UOM", normalView)
        DO1.setVisibility("UOMUNITS", normalView)
        DO1.setVisibility("HANDLINGUNIT", normalView)
        DO1.setVisibility("HANDLINGUNITTYPE", normalView)
        DO1.setVisibility("CONFIRM", normalView)
        DO1.setVisibility("CASE_CONT", caseScanView)
        'DO1.setVisibility("CASE_CONFIRM", caseScanView)
        DO1.setVisibility("CASE_INSTR", caseScanView)
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "override"
                doNext(True)
            Case "back"
                doBack()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
