Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic

Partial Public Class MovYardEquipmentTask2
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("YardEquipmentMoveTask") Is Nothing OrElse Session("YardEquipmentMoveJob") Is Nothing Then
                GoBack()
            End If
            SetScreen()
        End If
    End Sub

    Private Sub GoBack()
        Response.Redirect(MapVirtualPath("Screens/MovYardEquipmentTask.aspx"))
    End Sub

    Private Sub NextClicked()
        Dim oTranslator As New Made4Net.Shared.Translation.Translator
        Try
            If DO1.Value("Confirm") = String.Empty Then
                MessageQue.Enqueue(oTranslator.Translate("Confirmation location cannot be blank"))
                Return
            End If
            Dim oMoveJob As WMS.Logic.YardMovementJob = Session("YardEquipmentMoveJob")
            Dim oMoveTask As WMS.Logic.YardMovementTask = Session("YardEquipmentMoveTask")
            oMoveTask.Move(DO1.Value("Confirm"), oMoveJob, WMS.Logic.Common.GetCurrentUser)
            Session("YardEquipmentMoveTask") = Nothing
            Session("YardEquipmentMoveJob") = Nothing
        Catch ex As Made4Net.Shared.M4NException
            DO1.Value("Confirm") = ""
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            DO1.Value("Confirm") = ""
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        GoBack()
    End Sub

    Protected Sub SetScreen()
        Dim oMoveJob As WMS.Logic.YardMovementJob = Session("YardEquipmentMoveJob")
        DO1.Value("TaskId") = oMoveJob.TaskId
        DO1.Value("TaskType") = oMoveJob.TaskType
        DO1.Value("VehicleId") = oMoveJob.VehicleId
        DO1.Value("Trailer") = oMoveJob.TrailerId
        DO1.Value("FromYardLocation") = oMoveJob.FromYardLocation
        DO1.Value("ToYardLocation") = oMoveJob.ToYardLocation
        DO1.FocusField = "Confirm"
        DO1.DefaultButton = "Next"
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("TaskId")
        DO1.AddLabelLine("TaskType")
        DO1.AddLabelLine("VehicleId")
        DO1.AddLabelLine("Trailer")
        DO1.AddLabelLine("FromYardLocation")
        DO1.AddLabelLine("ToYardLocation")
        DO1.AddSpacer()
        DO1.AddTextboxLine("Confirm", "Confirm (Target location)")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                NextClicked()
            Case "back"
                GoBack()
        End Select
    End Sub
End Class