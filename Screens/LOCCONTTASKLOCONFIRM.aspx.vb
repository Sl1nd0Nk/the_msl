Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

Partial Public Class LOCCONTTASKLOCONFIRM
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim loadObj As WMS.Logic.Load = Session("LOCVRFI_LOADOBJ")
        DO1.Value("LOADLOCATION") = loadObj.LOCATION
        DO1.Value("LOADID") = Session("LocationCNTLoadId")
        DO1.Value("LOCATION") = Session("TaskLocationCNTLocationId")


    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "approve"
                doApprove()
        End Select
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("LOADLOCATION")
        DO1.AddLabelLine("LOCATION")
    End Sub

    Private Sub doBack()
        Session.Remove("LOCVRFI_LOADOBJ")
        If Request.QueryString("countsourcescreen") <> "" Then
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
        Else
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
        End If
    End Sub

    Private Sub doApprove()
        'Dim loadObj As WMS.Logic.Load = Session("LOCVRFI_LOADOBJ")

        'Dim dt As System.Data.DataTable = Session("TaskLocationCNTLoadsDT")
        'dt.Rows.Add(loadObj.LOADID, loadObj.CONSIGNEE, loadObj.SKU, loadObj.UNITS, 0, 0)
        'Session("TaskLocationCNTLoadsDT") = dt

        Session.Remove("LOCVRFI_LOADOBJ")
        Session("ToLocationForVerification") = DO1.Value("LOCATION")
        If Request.QueryString("countsourcescreen") <> "" Then
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK2.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
        Else
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK2.aspx"))
        End If
    End Sub



End Class