Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess


<CLSCompliant(False)> Public Class LOCCONTTASK1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    'Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If Not IsPostBack Then
        DO1.Value("LOCATION") = Session("TaskLocationCNTLocationId")
        'End If

        If Session("SELECTEDSKU") <> "" Then
            DO1.Value("SKU") = Session("SELECTEDSKU")
            ' Add all controls to session for restoring them when we back from that sreen
            DO1.Value("LOADID") = Session("SKUSEL_LOADID")
            DO1.Value("LOCATION") = Session("SKUSEL_LOCATION")
            DO1.Value("CONSIGNEE") = Session("SELECTEDCONSIGNEE")

            Session.Remove("SKUSEL_LOADID")
            Session.Remove("SKUSEL_LOCATION")
            'Session.Remove("SKUSEL_CONSIGNEE")
            Session.Remove("SELECTEDSKU")
            Session.Remove("SELECTEDCONSIGNEE")
            Session.Remove("SKUSEL_SKUSDT")

            Session("SkipSKUCheck") = "1"

        End If


    End Sub
    Private Sub createNewLoadFromCounting()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If DO1.Value("SKU") = String.Empty Or DO1.Value("LOCATION") = String.Empty Then
            MessageQue.Enqueue(trans.Translate("Please scan a SKU and Location"))
            Return
        End If
        If String.IsNullOrEmpty(DO1.Value("CONSIGNEE").ToString()) Then


            MessageQue.Enqueue(trans.Translate("Invalid Consignee"))
            Return

        End If
        Dim sql As String = String.Format("select count(1) from sku where sku = '{0}'", DO1.Value("SKU"))
        If Made4Net.DataAccess.DataInterface.ExecuteScalar(sql) = 0 Then
            MessageQue.Enqueue(trans.Translate("SKU does not exist"))
            DO1.Value("SKU") = ""
            DO1.FocusField = "SKU"
            Return
        End If
        If Not WMS.Logic.Location.Exists(DO1.Value("LOCATION")) Then
            MessageQue.Enqueue(trans.Translate("Location does not exist"))
            DO1.Value("LOCATION") = ""
            DO1.FocusField = "LOCATION"
            Return
        End If
        Session("CNTCreateLoadConsignee") = DO1.Value("CONSIGNEE")
        Session("CNTCreateLoadSku") = DO1.Value("SKU")
        Session("CNTCreateLoadLocation") = DO1.Value("LOCATION")
        Session("CNTCreateLoadSRCSCREEN") = "LOCCONTTASK1"
        Response.Redirect(MapVirtualPath("Screens/CNTCreateLoad.aspx"))
    End Sub
    Private Sub doBack()
        Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSrcScreen") & ".aspx"))
    End Sub

    Private Sub doEndCount()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            If CanCompleteCount() Or LocationIsEmpty(Session("TaskLocationCNTLocationId")) Then
                Dim toqty, fromQty As Decimal
                If Not LocationIsEmpty(Session("TaskLocationCNTLocationId")) Then
                    CalcQty(fromQty, toqty)
                End If

                Dim oCntTask As WMS.Logic.CountTask
                If Not Session("LocationBulkCountTask") Is Nothing Then
                    oCntTask = Session("LocationBulkCountTask")
                Else
                    oCntTask = Session("LocationCountTask")
                End If
                Dim oCounting As New WMS.Logic.Counting(oCntTask.COUNTID)
                'Build and fill count job object
                Dim oCountJob As WMS.Logic.CountingJob = oCntTask.getCountJob(oCounting)
                If Not LocationIsEmpty(Session("TaskLocationCNTLocationId")) Then
                    oCountJob.ExpectedQty = fromQty
                    oCountJob.CountedQty = toqty
                    oCountJob.CountedLoads = Session("TaskLocationCNTLoadsDT")
                    oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser)
                Else
                    oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser) '.Complete()
                End If

                'End If
                Session.Remove("LocationCNTLoadId")
                Session.Remove("TaskLocationCNTLocationId")
                Session.Remove("TSKTaskId")
                Session.Remove("LocationBulkCountTask")
                Session.Remove("LocationCountTask")
                Session("TaskID") = oCntTask.TASK
                MessageQue.Enqueue(trans.Translate("Location Count Completed"))
            Else
                MessageQue.Enqueue(trans.Translate("Not All Loads Counted for the current Location. Please Confirm"))
                If Request.QueryString("countsourcescreen") <> "" Then
                    Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK3.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
                Else
                    Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK3.aspx"))
                End If
            End If
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            MessageQue.Enqueue(ex.ToString)
        End Try
        ''check if need to redirect to task summery

        If MobileUtils.ShouldRedirectToTaskSummary() Then
            If Session("MobileSourceScreen") <> "" Then
                Dim strScreen As String = Session("MobileSourceScreen")
                Session.Remove("MobileSourceScreen")
                ' Response.Redirect(MapVirtualPath("Screens/" & strScreen & ".aspx"))
                MobileUtils.RedirectToTaskSummary(strScreen,  Session("TaskID"))
            Else
                If Request.QueryString("countsourcescreen") <> "" Then
                    Response.Redirect(MapVirtualPath("Screens/" & Request.QueryString("countsourcescreen") & ".aspx?countcomplete=1"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSrcScreen") & ".aspx"))
                End If
            End If
        Else 'no need to redirect to task summery
            If Session("MobileSourceScreen") <> "" Then
                Dim strScreen As String = Session("MobileSourceScreen")
                Session.Remove("MobileSourceScreen")
                Response.Redirect(MapVirtualPath("Screens/" & strScreen & ".aspx"))
            Else
                If Request.QueryString("countsourcescreen") <> "" Then
                    Response.Redirect(MapVirtualPath("Screens/" & Request.QueryString("countsourcescreen") & ".aspx?countcomplete=1"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/" & Session("CountingSrcScreen") & ".aspx"))
                End If
            End If
        End If
    End Sub

    Private Sub doNext()
        'If Page.IsValid Then
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim errStr As String = getConsigneeSku()
        If Not String.IsNullOrEmpty(errStr) Then
            MessageQue.Enqueue(trans.Translate(errStr))
            Return
        End If
        Dim dt As New DataTable
        Dim sql As String
        Dim LoadId, Loc, Cons, Sku As String
        LoadId = DO1.Value("LOADID")
        Loc = Session("TaskLocationCNTLocationId")
        Cons = DO1.Value("CONSIGNEE")
        Sku = DO1.Value("SKU")

        Select Case LoadFindType(LoadId, Loc, Cons, Sku)
            Case LoadSearchType.ByLoad
                'sql = String.Format("SELECT LOADID FROM invload WHERE LOADID LIKE '{0}' and location = '{1}'", LoadId.Trim(), Loc.Trim())
                sql = String.Format("SELECT LOADID FROM invload WHERE LOADID LIKE '{0}' and units > 0 ", LoadId.Trim())
            Case LoadSearchType.ByLocationAndSku
                sql = String.Format("SELECT LOADID FROM invload INNER JOIN SKU ON invload.CONSIGNEE = SKU.CONSIGNEE AND invload.SKU = SKU.SKU WHERE invload.LOCATION LIKE '{0}%' AND (SKU.SKU like '{1}%' or SKU.MANUFACTURERSKU like '{1}%' or SKU.VENDORSKU ='{1}' or SKU.OTHERSKU ='{1}')", Loc.Trim(), Sku.Trim())
            Case LoadSearchType.ByLocationAndConsigneeAndSku
                sql = String.Format("SELECT LOADID FROM invload INNER JOIN SKU ON invload.CONSIGNEE = SKU.CONSIGNEE AND invload.SKU = SKU.SKU WHERE invload.LOCATION LIKE '{0}%' AND SKU.CONSIGNEE LIKE '{1}%' AND (SKU.SKU like '{2}%' or SKU.MANUFACTURERSKU like '{2}%' or SKU.VENDORSKU ='{2}' or SKU.OTHERSKU ='{2}')", Loc.Trim(), Cons.Trim(), Sku.Trim())
            Case LoadSearchType.ByLocation
                sql = String.Format("SELECT LOADID FROM invload WHERE LOCATION LIKE '{0}%'", Loc.Trim())
            Case LoadSearchType.BySku
                sql = String.Format("SELECT LOADID FROM invload INNER JOIN SKU ON invload.CONSIGNEE = SKU.CONSIGNEE AND invload.SKU = SKU.SKU WHERE SKU.CONSIGNEE LIKE '{0}%' and location = '{2}' AND (SKU.SKU like '{1}%' or SKU.MANUFACTURERSKU like '{1}%' or SKU.VENDORSKU ='{1}' or SKU.OTHERSKU ='{1}')", Cons.Trim(), Sku.Trim(), Loc.Trim)
        End Select

        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
        If dt.Rows.Count > 1 Then
            MessageQue.Enqueue(trans.Translate("More than 1 load in location, enter loadid"))
            Return
        ElseIf dt.Rows.Count = 0 Then
            MessageQue.Enqueue(trans.Translate("No Load Found"))
            Return
        End If
        Dim ld As String
        Try
            ld = dt.Rows(0)(0)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage())
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            Return
        End Try
        Session("LocationCNTLoadId") = ld
        Dim destScreen As String = ""

        Dim ldObj As New WMS.Logic.Load(ld)

        If ldObj.hasActivity AndAlso _
        Not (ldObj.ACTIVITYSTATUS.Equals(WMS.Lib.Statuses.ActivityStatus.PUTAWAYPEND, StringComparison.OrdinalIgnoreCase) OrElse _
        ldObj.ACTIVITYSTATUS.Equals(WMS.Lib.Statuses.ActivityStatus.REPLPENDING, StringComparison.OrdinalIgnoreCase)) Then
            MessageQue.Enqueue(trans.Translate("Cannot create count task - Load already assigned to another activity"))
            Return
        End If

        If Not ldObj.LOCATION.Equals(Loc, StringComparison.OrdinalIgnoreCase) Then
            MessageQue.Enqueue(trans.Translate("Load location is not the same as counting location. Please Confirm"))
            Session("LOCVRFI_LOADOBJ") = ldObj
            destScreen = "Screens/LOCCONTTASKLOCONFIRM.aspx"
        Else
            destScreen = "Screens/LOCCONTTASK2.aspx"
        End If

        If Request.QueryString("countsourcescreen") <> "" Then
            destScreen = destScreen & "?countsourcescreen=" & Request.QueryString("countsourcescreen")
        End If
        Response.Redirect(MapVirtualPath(destScreen))


        'If Request.QueryString("countsourcescreen") <> "" Then
        '    Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK2.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
        'Else
        '    Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK2.aspx"))
        'End If
        ''End If
    End Sub

    Private Function getConsigneeSku() As String
        ' First Check if Consignee and Sku is full , if yes get check from vSKUCODE
        If DO1.Value("LOADID").Trim = "" AndAlso String.IsNullOrEmpty(Session("SkipSKUCheck")) Then
            If DO1.Value("SKU").Trim <> "" Then
                ' Check for sku
                Dim sql As String = "SELECT DISTINCT (vSC.CONSIGNEE+' '+ vSC.SKU) as SKU, case isnull(SKU.STATUS,0) when 0 then '-- ' else '' end +vSC.CONSIGNEE + ' ' + vSC.SKU + ' ' + SKUDESC AS DESCR FROM vSKUCODE vSC INNER JOIN SKU ON vSC.CONSIGNEE=SKU.CONSIGNEE AND vSC.SKU=SKU.SKU WHERE vSC.CONSIGNEE like '%" + DO1.Value("CONSIGNEE") + "' AND (SKUCODE LIKE '" & DO1.Value("SKU") & "' OR vSC.SKU LIKE '" & DO1.Value("SKU") & "')"
                Dim skusDT As New DataTable()
                Made4Net.DataAccess.DataInterface.FillDataset(sql, skusDT)
                If skusDT.Rows.Count = 0 Then
                    'MessageQue.Enqueue(trans.Translate("SKU does not exist"))
                    DO1.Value("SKU") = ""
                    DO1.FocusField = "SKU"
                    Return "SKU does not exist"
                ElseIf skusDT.Rows.Count = 1 Then
                    DO1.Value("CONSIGNEE") = skusDT.Rows(0)("SKU").ToString().Split(" ")(0)
                    DO1.Value("SKU") = skusDT.Rows(0)("SKU").ToString().Split(" ")(1)

                    'sql = "SELECT count(1) FROM INVLOAD WHERE INVLOAD.CONSIGNEE = '" + DO1.Value("CONSIGNEE") + "' AND SKU = '" & DO1.Value("SKU") & "' AND LOCATION ='" & DO1.Value("LOCATION") & "'"
                    'If CType(Made4Net.DataAccess.DataInterface.ExecuteScalar(sql).ToString(), Integer) = 0 Then
                    '    MessageQue.Enqueue(trans.Translate("A load with entered sku does not exist in entered location"))
                    '    DO1.Value("LOCATION") = ""
                    '    DO1.FocusField = "LOCATION"
                    '    Return
                    'End If

                Else
                    If String.IsNullOrEmpty(Session("SELECTEDSKU")) Then
                        ' Go to Sku select screen
                        Session("FROMSCREEN") = "LOCCONTTASK1"
                        Session("SKUCODE") = DO1.Value("SKU")
                        ' Add all controls to session for restoring them when we back from that sreen
                        Session("SKUSEL_LOADID") = DO1.Value("LOADID").Trim
                        Session("SKUSEL_SKUSDT") = skusDT
                        Session("SKUSEL_LOCATION") = DO1.Value("LOCATION").Trim
                        Response.Redirect(MapVirtualPath("Screens/MultiSelectForm.aspx")) ' Changed
                    End If
                End If
            End If
        End If
        Session.Remove("SkipSKUCheck")
        Return ""
    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOCATION")
        DO1.AddTextboxLine("LOADID")
        DO1.AddTextboxLine("CONSIGNEE")
        DO1.AddTextboxLine("SKU")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
            Case "endcount"
                doEndCount()
            Case "newload"
                createNewLoadFromCounting()
        End Select
    End Sub

    Private Function LoadFindType(ByVal LoadId As String, ByVal Loc As String, ByVal cons As String, ByVal sk As String) As Int32
        If LoadId <> "" Then Return LoadSearchType.ByLoad
        If Loc <> "" And sk <> "" Then Return LoadSearchType.ByLocationAndSku
        If Loc <> "" And cons <> "" And sk <> "" Then Return LoadSearchType.ByLocationAndConsigneeAndSku
        If cons <> "" And sk <> "" Then Return LoadSearchType.BySku
        If sk <> "" Then Return LoadSearchType.BySku
        If Loc <> "" Then Return LoadSearchType.ByLocation
    End Function

    Public Enum LoadSearchType
        ByLoad = 1
        ByLocationAndSku = 2
        ByLocationAndConsigneeAndSku = 3
        BySku = 4
        ByLocation = 6
    End Enum

    Private Function CanCompleteCount() As Boolean
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            If dr("counted") = 0 Then Return False
        Next
        Return True
    End Function

    Private Function CreateLimboLoadCollection() As WMS.Logic.LoadsCollection
        Dim ldColl As New WMS.Logic.LoadsCollection
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            If dr("counted") = 0 Then
                ldColl.Add(New WMS.Logic.Load(Convert.ToString(dr("loadid"))))
            End If
        Next
        Return ldColl
    End Function

    Private Sub CalcQty(ByRef pFromqty As Decimal, ByRef pToQty As Decimal)
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            pFromqty = pFromqty + Convert.ToDecimal(dr("fromqty"))
            pToQty = pToQty + Convert.ToDecimal(dr("toqty"))
        Next
        Session("TaskLocationCNTLoadsDT") = dt
    End Sub

    Private Function LocationIsEmpty(ByVal pLocation As String) As Boolean
        If DataInterface.ExecuteScalar("SELECT COUNT(1) FROM INVLOAD WHERE LOCATION='" & pLocation & "'") = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
