Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class Mov2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LoadMoveLoadId")))
            DO1.Value("CONSIGNEE") = ld.CONSIGNEE
            DO1.Value("SKU") = ld.SKU
            DO1.Value("UOM") = ld.LOADUOM
            DO1.Value("UNITS") = ld.UNITS
            DO1.Value("LOCATION") = ld.LOCATION
            DO1.Value("LOADID") = ld.LOADID
            Try
                DO1.Value("SKUDESC") = Made4Net.DataAccess.DataInterface.ExecuteScalar(String.Format("select skudesc from sku where sku = '{0}' and consignee = '{1}'", ld.SKU, ld.CONSIGNEE))
                DO1.Value("TOQTY") = ld.UNITS
            Catch ex As Exception
            End Try

            initStatusDropDown(ld.STATUS)
        End If
    End Sub

    Private Sub initStatusDropDown(ByVal pStatus As String)
        Dim dd As Made4Net.WebControls.MobileDropDown
        dd = DO1.Ctrl("TOSTATUS")
        dd.AllOption = False
        dd.TableName = "INVSTATUSES"
        dd.ValueField = "CODE"
        dd.TextField = "DESCRIPTION"
        dd.Where = "CODE <> 'LIMBO'"
        dd.DataBind()
        Try
            dd.SelectedValue = pStatus
        Catch ex As Exception
        End Try
    End Sub

    Private Sub doMenu()
        Session.Remove("LoadMoveLoadId")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doBack()
        Session.Remove("LoadMoveLoadId")
        Response.Redirect(MapVirtualPath("Screens/Mov.aspx"))
    End Sub

    Private Sub doNext()
        'If Page.IsValid Then
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        'Check if location is good for use
        Dim oloc As WMS.Logic.Location
        Try
            oloc = New WMS.Logic.Location(DO1.Value("TOLOCATION"))
        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            Return
        End Try

        Try
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LoadMoveLoadId")))
            Dim qtyToMove As Decimal = 0
            Try
                qtyToMove = Convert.ToDecimal(DO1.Value("TOQTY"))
                If qtyToMove <= 0 Then
                    MessageQue.Enqueue(trans.Translate("Quantity to move must be greater than 0"))
                    Return
                End If
            Catch ex As Exception
                MessageQue.Enqueue(trans.Translate("Invalid quantity."))
                Return
            End Try
            If qtyToMove > ld.UNITS Then
                MessageQue.Enqueue(trans.Translate("Quantity to move is greater than the load quantity."))
                Return
            End If
            'If Convert.ToDecimal(DO1.Value("TOQTY")) > 0 And Convert.ToDecimal(DO1.Value("TOQTY")) < ld.UNITS Then
            If qtyToMove > 0 AndAlso qtyToMove < ld.UNITS Then
                Dim newLoadid As String = Made4Net.Shared.Util.getNextCounter("LOAD")
                ld.Split(oloc.Location, DO1.Value("TOQTY"), newLoadid, WMS.Logic.GetCurrentUser)
            Else
                ld.Move(oloc.Location, "", WMS.Logic.GetCurrentUser)
            End If

            'if load wasnt merged with any other loads, and its status should changed..
            If ld.LOCATION <> String.Empty AndAlso Not ld.STATUS.Equals(DO1.Value("TOSTATUS"), StringComparison.OrdinalIgnoreCase) Then
                ld.setStatus(DO1.Value("TOSTATUS"), "", WMS.Logic.GetCurrentUser)
            End If

        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            Return
        End Try
        MessageQue.Enqueue(trans.Translate("Load moving process complete."))
        Session.Remove("LoadMoveLoadId")
        Response.Redirect(MapVirtualPath("Screens/Mov.aspx"))
        'End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UNITS")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        DO1.AddDropDown("TOSTATUS")
        DO1.AddTextboxLine("TOLOCATION")
        DO1.AddTextboxLine("TOQTY")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case ("back")
                doBack()
            Case "menu"
                doMenu()
        End Select
    End Sub

End Class
