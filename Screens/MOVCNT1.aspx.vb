Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic

<CLSCompliant(False)> Public Class MOVCNT1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            DO1.Value("CONTAINER") = Session("MoveContainerID")
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("CONTAINER")
        DO1.AddTextboxLine("TOLOCATION")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "move container"
                doMove()
            Case "menu"
                doMenu()
        End Select
    End Sub

    Private Sub doMove()
        Dim t As Made4Net.Shared.Translation.Translator

        If DO1.Value("TOLOCATION") <> "" Then
            Dim CheckCntSql As String = "SELECT * FROM LOCATION WHERE LOCATION='" & DO1.Value("TOLOCATION") & "'"
            Dim dt As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(CheckCntSql, dt)
            'Checking if container already exists, if not creating new one
            If dt.Rows.Count = 0 Then
                t = New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
                MessageQue.Enqueue(t.Translate("Location does not exists"))
                Return
            End If
        Else
            t = New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            MessageQue.Enqueue(t.Translate("Location does not exists"))
            Return
        End If
        Try
            Dim oCont As WMS.Logic.Container = New WMS.Logic.Container(Session("MoveContainerID"), True)
            oCont.Move(DO1.Value("TOLOCATION"), "", WMS.Logic.Common.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As ApplicationException
            MessageQue.Enqueue(ex.Message)
            Return
        End Try

        t = New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        MessageQue.Enqueue(t.Translate("Container Moved"))
        Response.Redirect(MapVirtualPath("Screens/MOVCNT.aspx"))
    End Sub

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

End Class
