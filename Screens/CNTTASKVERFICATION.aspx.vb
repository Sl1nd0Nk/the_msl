Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Partial Public Class CNTTASKVERFICATION
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Dim oLoad As WMS.Logic.Load
            If Not Session("TaskLoadCNTLoadId") Is Nothing Then
                oLoad = New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
            ElseIf Not Session("LocationCNTLoadId") Is Nothing Then
                oLoad = New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
            End If
            DO1.Value("LoadId") = oLoad.LOADID
            DO1.Value("SKU") = oLoad.SKU
            Dim oSku As New WMS.Logic.SKU(oLoad.CONSIGNEE, oLoad.SKU)
            DO1.Value("SKUDESC") = oSku.SKUDESC
            DO1.Value("UOM") = Session("UOMForVerification")
            DO1.Value("CountedUnits") = Session("CountedUnitsForVerification")

            If Not Session("ToLocationForVerification") Is Nothing Then
                DO1.Value("LOCATION") = Session("ToLocationForVerification")
            Else
                DO1.Value("LOCATION") = oLoad.LOCATION
            End If

        End If
    End Sub

    Private Sub doBack()
        If Not String.IsNullOrEmpty(Session("PCKSOURCESCREEN")) Then
            Response.Redirect(MapVirtualPath("Screens/CNTTASK1.aspx"))
        End If
        If Request.QueryString("SRCSCREEN") = "CNTTASK" Or Request.QueryString("SRCSCREEN") = "TaskManager" Then
            Response.Redirect(MapVirtualPath("Screens/CNTTASK1.aspx"))
        ElseIf Request.QueryString("SRCSCREEN") = "LOCCONTTASK" Then
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
        Else
            Response.Redirect(MapVirtualPath("Screens/" & Request.QueryString("SRCSCREEN") & ".aspx"))
        End If
    End Sub

    Private Sub doApprove()
        'If Page.IsValid Then
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ld As WMS.Logic.Load
            If Not Session("TaskLoadCNTLoadId") Is Nothing Then
                ld = New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
            ElseIf Not Session("LocationCNTLoadId") Is Nothing Then
                ld = New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
            End If
            Dim oCntTask As WMS.Logic.CountTask
            If Not Session("LocationBulkCountTask") Is Nothing Then
                oCntTask = Session("LocationBulkCountTask")
            ElseIf Not Session("LocationCountTask") Is Nothing Then
                oCntTask = Session("LocationCountTask")
            ElseIf Not Session("LoadCountTask") Is Nothing Then
                oCntTask = Session("LoadCountTask")
            End If
            Dim oCounting As New WMS.Logic.Counting(oCntTask.COUNTID)
            'Build and fill count job object
            Dim oCountJob As WMS.Logic.CountingJob = oCntTask.getCountJob(oCounting)
            oCountJob.CountedQty = DO1.Value("CountedUnits")
            oCountJob.UOM = DO1.Value("UOM")
            If Not Session("ToLocationForVerification") Is Nothing Then
                oCountJob.Location = Session("ToLocationForVerification")
            Else
                'oCountJob.Location = ld.LOCATION
                oCountJob.Location = oCounting.LOCATION
            End If
            oCountJob.LoadCountVerified = True
            oCountJob.LocationCountingLoadCounted = True
            oCountJob.LoadId = ld.LOADID
            oCountJob.CountingAttributes = Session("AttributesForVerification")
            oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser)
            UpdateLoadCount(ld.LOADID, oCountJob.CountedQty)
            Session.Remove("TaskLoadCNTLoadId")
            Session.Remove("ToLocationForVerification")
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        If Not String.IsNullOrEmpty(Session("PCKSOURCESCREEN")) Then
            Dim srcScreen As String = Session("PCKSOURCESCREEN")
            Session("PickListChanged") = "1"
            Session.Remove("PCKSOURCESCREEN")
            Response.Redirect(MapVirtualPath("Screens/" + srcScreen + ".aspx"))
        End If
        doBack()
        'End If
    End Sub

    Private Sub UpdateLoadCount(ByVal pLoadId As String, ByVal pToQty As Decimal)
        If Not Session("TaskLocationCNTLoadsDT") Is Nothing Then
            Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
            For Each dr As DataRow In dt.Rows
                If dr("loadid") = pLoadId Then
                    dr("counted") = 1
                    dr("toqty") = pToQty
                End If
            Next
            Session("TaskLocationCNTLoadsDT") = dt
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LoadId")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("CountedUnits")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("LOCATION")
        setAttributes()
        DO1.AddSpacer()
    End Sub

    Private Sub setAttributes()
        If Not Session("AttributesForVerification") Is Nothing Then
            Dim oAttCol As WMS.Logic.AttributesCollection = Session("AttributesForVerification")
            Dim ld As WMS.Logic.Load
            If Not Session("TaskLoadCNTLoadId") Is Nothing Then
                ld = New WMS.Logic.Load(Convert.ToString(Session("TaskLoadCNTLoadId")))
            ElseIf Not Session("LocationCNTLoadId") Is Nothing Then
                ld = New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
            End If
            Dim oSku As String = ld.SKU
            Dim oConsignee As String = ld.CONSIGNEE
            Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
            If Not objSkuClass Is Nothing Then
                If objSkuClass.CaptureAtCountingLoadAttributesCount > 0 Then
                    For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
                        If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required OrElse _
                        oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                            Try
                                If oAtt.Type = AttributeType.DateTime Then
                                    Dim dateTimeObj As DateTime = CType(oAttCol(oAtt.Name), DateTime)
                                    If dateTimeObj = DateTime.MinValue Then
                                        DO1.AddLabelLine(oAtt.Name, oAtt.Name)
                                    Else
                                        DO1.AddLabelLine(oAtt.Name, oAtt.Name, CType(oAttCol(oAtt.Name), DateTime).ToString(Made4Net.Shared.GetSystemParameterValue("System_DateFormat")))
                                    End If
                                ElseIf oAtt.Type = AttributeType.Decimal Then
                                    Dim dec As Decimal
                                    If Decimal.TryParse(oAttCol(oAtt.Name), dec) Then
                                        If dec = Decimal.MinValue Then
                                            DO1.AddLabelLine(oAtt.Name, oAtt.Name)
                                        Else
                                            DO1.AddLabelLine(oAtt.Name, oAtt.Name, oAttCol(oAtt.Name))
                                        End If
                                    Else
                                        DO1.AddLabelLine(oAtt.Name, oAtt.Name)
                                    End If
                                ElseIf oAtt.Type = AttributeType.Integer Then
                                    Dim int As Integer
                                    If Integer.TryParse(oAttCol(oAtt.Name), int) Then
                                        If int = Integer.MinValue Then
                                            DO1.AddLabelLine(oAtt.Name, oAtt.Name)
                                        Else
                                            DO1.AddLabelLine(oAtt.Name, oAtt.Name, oAttCol(oAtt.Name))
                                        End If
                                    Else
                                        DO1.AddLabelLine(oAtt.Name, oAtt.Name)
                                    End If
                                Else
                                    DO1.AddLabelLine(oAtt.Name, oAtt.Name, oAttCol(oAtt.Name))
                                End If
                            Catch ex As Exception
                                DO1.AddLabelLine(oAtt.Name, oAtt.Name)
                            End Try
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "approve"
                doApprove()
        End Select
    End Sub

End Class
