Imports System
Imports Made4Net.DataAccess
Imports Made4net.Shared
Imports Made4Net.Shared.Web

<CLSCompliant(False)> Public Class MobileUtils

    Public Shared Sub ClearCreateLoadProcessSession(Optional ByVal isDescreateReceiving As Boolean = False)
        If Not isDescreateReceiving Then
            HttpContext.Current.Session.Remove("CreateLoadRCN")
            HttpContext.Current.Session.Remove("CreateLoadRCNLine")
            HttpContext.Current.Session.Remove("CreateLoadConsignee")
            HttpContext.Current.Session.Remove("CreateLoadSKU")
            HttpContext.Current.Session.Remove("CreateLoadContainerID")
            HttpContext.Current.Session.Remove("CreateLoadLocation")
            HttpContext.Current.Session.Remove("CreateLoadLoadId")
            HttpContext.Current.Session.Remove("CreateLoadStatus")
            HttpContext.Current.Session.Remove("CreateLoadHoldRC")
            HttpContext.Current.Session.Remove("CreateLoadLabelPrinter")
            HttpContext.Current.Session.Remove("CreateLoadUnits")
            HttpContext.Current.Session.Remove("CreateLoadUOM")
            HttpContext.Current.Session.Remove("CreateLoadNumLoads")
            HttpContext.Current.Session.Remove("attributes")
        Else
            HttpContext.Current.Session.Remove("CreateLoadReciptId")
            HttpContext.Current.Session.Remove("CreateLoadReciptLine")
            HttpContext.Current.Session.Remove("CreateLoadConsignee")
            HttpContext.Current.Session.Remove("CreateLoadSKU")

            HttpContext.Current.Session.Remove("CreateLoadReceiptLocation")
            HttpContext.Current.Session.Remove("CreateLoadContainerID")
            HttpContext.Current.Session.Remove("SCANED")
            HttpContext.Current.Session.Remove("SKUCOLL")
        End If
    End Sub

    Public Shared Sub ClearCreateLoadChangeLineSession(Optional ByVal isDescreateReceiving As Boolean = False)
        If Not isDescreateReceiving Then
            HttpContext.Current.Session.Remove("CreateLoadRCNLine")
            HttpContext.Current.Session.Remove("CreateLoadConsignee")
            HttpContext.Current.Session.Remove("CreateLoadSKU")
            HttpContext.Current.Session.Remove("CreateLoadStatus")
            HttpContext.Current.Session.Remove("CreateLoadHoldRC")
            HttpContext.Current.Session.Remove("CreateLoadLabelPrinter")
            HttpContext.Current.Session.Remove("CreateLoadUnits")
            HttpContext.Current.Session.Remove("CreateLoadUOM")
            HttpContext.Current.Session.Remove("CreateLoadNumLoads")
            HttpContext.Current.Session.Remove("CreateLoadQty")
            HttpContext.Current.Session.Remove("CreateLoadSku")
            HttpContext.Current.Session.Remove("attributes")
            HttpContext.Current.Session("LineChanged") = 1
        End If
    End Sub

    Public Shared Sub ClearBlindReceivingSession()
        HttpContext.Current.Session.Remove("CreateLoadQty")
        HttpContext.Current.Session.Remove("CreateLoadSku")
        HttpContext.Current.Session.Remove("CreateLoadReciptId")
        HttpContext.Current.Session.Remove("CreateLoadConsignee")
        HttpContext.Current.Session.Remove("CreateLoadSKU")
        HttpContext.Current.Session.Remove("CreateLoadContainerID")
        HttpContext.Current.Session.Remove("CreateLoadLocation")
        HttpContext.Current.Session.Remove("CreateLoadLoadId")
    End Sub

    Public Shared Function GetURLByScreenCode(ByVal pScreenCode As String) As String
        Dim url As String = DataInterface.ExecuteScalar(String.Format("select url from sys_screen where  screen_id = '{0}'", pScreenCode), "Made4NetSchema")
        Return (MapVirtualPath(url))
    End Function

    Public Shared Function GetMHEDefaultPrinter(Optional ByVal pMHEId As String = "") As String
        Dim lblPrinter As String = ""
        If Not HttpContext.Current.Session("MHEIDLabelPrinter") Is Nothing Then
            lblPrinter = HttpContext.Current.Session("MHEIDLabelPrinter")
        ElseIf Not HttpContext.Current.Session("MHEID") Is Nothing Then
            lblPrinter = DataInterface.ExecuteScalar(String.Format("SELECT isnull(LABELPRINTER,'') as LABELPRINTER FROM MHE WHERE MHEID = '{0}'", HttpContext.Current.Session("MHEID")))
        Else
            lblPrinter = DataInterface.ExecuteScalar(String.Format("SELECT isnull(LABELPRINTER,'') as LABELPRINTER FROM MHE WHERE MHEID = '{0}'", pMHEId))
        End If
        Return lblPrinter
    End Function

    Public Shared Function LoggedInMHEID() As String
        If Not HttpContext.Current.Session("MHEID") Is Nothing Then
            Return HttpContext.Current.Session("MHEID")
        Else
            Return ""
        End If
    End Function

    Public Shared Function ExtractConfirmationValues(ByVal pConfirmType As String, ByVal pDO As Made4Net.Mobile.WebCtrls.DataObject) As System.Collections.Specialized.NameValueCollection
        Dim nvc As New System.Collections.Specialized.NameValueCollection
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                nvc.Add("LOAD", pDO.Value("CONFIRM(LOAD)"))
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                nvc.Add("LOCATION", pDO.Value("CONFIRM(LOCATION)"))
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                nvc.Add("SKU", pDO.Value("CONFIRM(SKU)"))
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                nvc.Add("SKU", pDO.Value("CONFIRM(SKU)"))
                nvc.Add("LOCATION", pDO.Value("CONFIRM(LOCATION)"))
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                nvc.Add("SKU", pDO.Value("CONFIRM(SKU)"))
                nvc.Add("LOCATION", pDO.Value("CONFIRM(LOCATION)"))
                nvc.Add("UOM", pDO.Value("CONFIRM(UOM)"))
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                nvc.Add("SKU", pDO.Value("CONFIRM(SKU)"))
                nvc.Add("UOM", pDO.Value("CONFIRM(UOM)"))
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                nvc.Add("UPC", pDO.Value("CONFIRM(UPC)"))
        End Select
        Return nvc
    End Function

    Public Shared Sub ExtractPickingAttributes(ByRef pPickJob As WMS.Logic.PickJob, ByVal pDO As Made4Net.Mobile.WebCtrls.DataObject)
        Dim Val As Object

        If pPickJob Is Nothing Then Return
        If pPickJob.oAttributeCollection Is Nothing Then Return

        Dim fromLoad As New WMS.Logic.Load(pPickJob.fromload)
        Dim skuObj As New WMS.Logic.SKU(pPickJob.consingee, pPickJob.sku)
        Dim skuCls As WMS.Logic.SkuClass = skuObj.SKUClass

        For Each attr As SkuClassLoadAttribute In skuCls.LoadAttributes
            If attr.CaptureAtPicking = SkuClassLoadAttribute.CaptureType.Required _
            OrElse attr.CaptureAtPicking = SkuClassLoadAttribute.CaptureType.Capture Then
                'Val = pDO.Value(attr.Name)
                Val = pDO.Value(String.Format("Attr_{0}", attr.Name))
                If Val = "" Then Val = Nothing

                'If Val Is Nothing AndAlso attr.CaptureAtPicking = SkuClassLoadAttribute.CaptureType.Capture Then
                If Val Is Nothing Then
                    Val = fromLoad.LoadAttributes.Attribute(attr.Name)
                    Select Case attr.Type
                        Case AttributeType.String
                            If Val = "" Then Val = Nothing
                    End Select
                End If
                pPickJob.oAttributeCollection(attr.Name) = Val
            End If
        Next
    End Sub

    Public Shared Function ShouldRedirectToTaskSummary() As Boolean
        If WMS.Logic.GetSysParam("ShowPerformanceOnTaskComplete") = 1 Then
            Return True
        End If
        Return False
    End Function

    Public Shared Sub RedirectToTaskSummary(ByVal pNextScreen As String, Optional ByVal pTaskId As String = "")
        If pTaskId <> "" Then
            HttpContext.Current.Session("TaskID") = pTaskId
        End If
        HttpContext.Current.Session("ScreenAfterTaskSummary") = pNextScreen
        HttpContext.Current.Response.Redirect(MapVirtualPath("screens/TaskSummary.aspx"))
    End Sub

    Public Shared Function GetSKUsDTFromSKUCODE(ByVal pConsignee As String, ByVal pSKUCode As String) As DataTable
        Dim dt As New DataTable
        If String.IsNullOrEmpty(pSKUCode) Then
            Return Nothing
        End If
        Dim sql As String = String.Format("SELECT DISTINCT CONSIGNEE, SKU from vSKUCODE vSC WHERE (SKUCODE like {0} OR vSC.SKU like {0})", Made4Net.Shared.FormatField(pSKUCode))
        If Not String.IsNullOrEmpty(pConsignee) Then
            sql = String.Format("{0} AND CONSIGNEE like {1}", sql, Made4Net.Shared.FormatField(pConsignee))
        End If
        DataInterface.FillDataset(sql, dt)
        Return dt
    End Function

End Class
