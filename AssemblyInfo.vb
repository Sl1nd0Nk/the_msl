Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("559C34C5-CFF6-4C57-BAB4-6B56642D061C")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("4.10.3.11")> 
'3.3.1
'Counting Module [CNT] : Trim function added when scaning LoadId
'Move Module [RPK] : Added buttons Override and Menu in RPK2 screen
'Move Module [RPK] : If Put Away task is assigned to someone and another user 
'                    requests the same task the system will give the 
'                    task to requsting user
'3.3.2
'Get Replenishment Task added [REPLT]
'Main screen added Request task functionality
'Descrete Receiving [BLKDRCV] 
'The procedure changed to count the goods in the container and after it to post 
'the results to receiving lines
'
'
'
'
'
'
'
'
'
'
'
'
'
'
