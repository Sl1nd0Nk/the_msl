﻿Imports System.Data.SqlClient 'Import SQL Capabilities

Public Class apexSerial
    Public Shared Sub APXSERIAL_AddSerialToLoad(ByVal SerialNumber As String, ByVal SKU As String, ByVal LoadId As String, ByVal UOM As String, ByVal User As String, ByVal weight As Double)
        Try
            Dim sqlComm As New SqlCommand
            sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            sqlComm.CommandText = "apx_serial_insert"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.AddWithValue("serial", SerialNumber)
            sqlComm.Parameters.AddWithValue("loadID", LoadId)
            sqlComm.Parameters.AddWithValue("SKU", SKU)
            sqlComm.Parameters.AddWithValue("ASNID", "")
            sqlComm.Parameters.AddWithValue("UOM", UOM)
            sqlComm.Parameters.AddWithValue("orderID", "")
            sqlComm.Parameters.AddWithValue("received", True)
            sqlComm.Parameters.AddWithValue("userid", User)
            sqlComm.Parameters.AddWithValue("serialweight", weight)
            sqlComm.Parameters.AddWithValue("ancestorID", 0)
            sqlComm.Parameters.AddWithValue("ancestorID", 0)
            sqlComm.Parameters("insertedID").Direction = ParameterDirection.InputOutput
            'sqlCon.Open()

            sqlComm.ExecuteNonQuery()
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXSERIAL_GetSerialData(ByVal SerialNumber As String) As DataTable
        Try
            Dim sqlComm As New SqlCommand
            sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            sqlComm.CommandText = "apx_serial_get"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.AddWithValue("serial", SerialNumber)

            Dim dt As New DataTable

            Dim reader As SqlDataReader
            reader = sqlComm.ExecuteReader()
            If reader.HasRows Then
                dt.Columns.Add("serial")
                dt.Columns.Add("serialCount")
                dt.Columns.Add("serialStatus")
                dt.Columns.Add("UOM")
                dt.Columns.Add("dispatched")
                dt.Columns.Add("received")
                dt.Columns.Add("SKU")

                While reader.Read()
                    dt.Rows.Add(reader(0), reader(1), reader(2), reader(3), reader(4), reader(5), reader(6))
                End While
            End If

            reader.Close()

            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Sub APXSERIAL_AddSerialToLoad1(ByVal SerialNumber As String, ByVal SKU As String, ByVal LoadId As String, ByVal UOM As String, ByVal User As String, ByVal weight As Double, ByVal ParentId As Integer, ByVal depth As Integer)
        Try
            Dim sqlComm As New SqlCommand
            sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            sqlComm.CommandText = "apx_serial_insert"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.AddWithValue("serial", SerialNumber)
            sqlComm.Parameters.AddWithValue("loadID", LoadId)
            sqlComm.Parameters.AddWithValue("SKU", SKU)
            sqlComm.Parameters.AddWithValue("ASNID", "")
            sqlComm.Parameters.AddWithValue("UOM", UOM)
            sqlComm.Parameters.AddWithValue("orderID", "")
            sqlComm.Parameters.AddWithValue("received", True)
            sqlComm.Parameters.AddWithValue("userid", User)
            sqlComm.Parameters.AddWithValue("serialweight", weight)
            sqlComm.Parameters.AddWithValue("ancestorID", ParentId)
            sqlComm.Parameters.AddWithValue("insertedID", 0)
            sqlComm.Parameters("insertedID").Direction = ParameterDirection.InputOutput


            'sqlComm.Parameters
            'sqlCon.Open()

            sqlComm.ExecuteNonQuery()
            'sqlComm.Parameters.a()
            Dim returnValue As Integer = sqlComm.Parameters("insertedID").Value

            Dim strTransIns As String
            strTransIns = String.Format("Select * From [apx_ReceiveStation] Where ParentSerial={0}", Made4Net.Shared.FormatField(SerialNumber))
            Dim DtLines As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

            Dim x As Integer = 0
            Dim dp As Integer = depth + 1
            While x < DtLines.Rows.Count
                APXSERIAL_AddSerialToLoad1(DtLines.Rows(x)("SerialNumber"), DtLines.Rows(x)("SKU"), LoadId, DtLines.Rows(x)("UOM"), User, weight, returnValue, 0)
                x = x + 1
            End While
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXSERIAL_FindSerialNumber(ByVal SerialNumber As String, ByVal SKU As String) As Integer
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [apx_serial] Where serial={0} and SKU={1}", Made4Net.Shared.FormatField(SerialNumber), Made4Net.Shared.FormatField(SKU))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        Return DtLines.Rows.Count
    End Function

    Public Shared Function APXSERIAL_UpdateASNSerialNumbers(ByVal ASNID As String, ByVal SKU As String, ByRef LoadId As String, ByVal weight As Double, ByVal user As String) As String
        Try
            Dim strTransIns As String
            strTransIns = String.Format("update [apx_serial] Set received=1, loadID={2}, serialweight={3}, userId={4} where ASNID={0} and SKU={1}", Made4Net.Shared.FormatField(ASNID), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(LoadId), Made4Net.Shared.FormatField(weight), Made4Net.Shared.FormatField(user))
            Made4Net.DataAccess.DataInterface.RunSQL(strTransIns)

            strTransIns = String.Format("select TOP (1) loadID FROM [apx_serial] where ASNID={0} and SKU={1}", Made4Net.Shared.FormatField(ASNID), Made4Net.Shared.FormatField(SKU))
            Dim DtLines As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)
            If DtLines.Rows.Count > 0 Then
                LoadId = DtLines.Rows(0)("loadID")
            End If

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Sub APXSERIAL_AbortASNSerialUpdate(ByVal ASNID As String, ByVal SKU As String)
        Try
            Dim strTransIns As String
            strTransIns = String.Format("update [apx_serial] Set received=0 where ASNID={0} and SKU={1}", Made4Net.Shared.FormatField(ASNID), Made4Net.Shared.FormatField(SKU))
            Made4Net.DataAccess.DataInterface.RunSQL(strTransIns)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXSERIAL_TransferSerial(ByVal SerialNumber As String, ByVal FromLoad As String, ByVal ToLoad As String, ByVal SKU As String, ByRef SerialQty As Integer) As String
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [apx_serial] Where serial={0} and SKU={1} and loadID={2}", Made4Net.Shared.FormatField(SerialNumber), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(FromLoad))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        If DtLines.Rows.Count >= 1 Then
            Dim DtLines1 As New DataTable
            strTransIns = String.Format("Select * From [SKUUOM] Where SKU={0} and UOM={1}", Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(DtLines.Rows(0)("UOM")))
            Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines1)
            If (DtLines1.Rows.Count >= 1) Then
                'SerialQty = DtLines1.Rows(0)("UNITSPERLOWESTUOM")
                If IsDBNull(DtLines1.Rows(0)("UNITSPERLOWESTUOM")) Then
                    SerialQty = CInt(DtLines1.Rows(0)("UNITSPERMEASURE"))
                Else
                    SerialQty = CInt(DtLines1.Rows(0)("UNITSPERLOWESTUOM"))
                End If
            End If
            Try
                Dim sqlComm As New SqlCommand
                sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

                sqlComm.CommandText = "apx_serial_update_load"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("currentLoadID", FromLoad)
                sqlComm.Parameters.AddWithValue("newLoadID", ToLoad)
                'sqlCon.Open()

                sqlComm.ExecuteNonQuery()
                Return ""
            Catch ex As Exception
                Return ex.Message
            End Try
        Else
            Return "Scanned Serialnumber is not found on load " + FromLoad
        End If
    End Function

    Public Shared Sub APXSERIAL_UpdateSerialsIntoDB(ByVal LoadId As String, ByVal SKU As String, ByVal Container As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Weight As String, ByVal UOM As String, ByVal user As String)
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_serial_returns] Where containerID={0} and SKU={1} and receipt={2} and receiptline={3}", Made4Net.Shared.FormatField(Container), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(Receipt), Made4Net.Shared.FormatField(ReceiptLine))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        If dtLines.Rows.Count > 0 Then
            For Each dr As DataRow In dtLines.Rows
                Dim serial As String = dr("serial")

                'Dim RetDtLines As New DataTable
                'Dim strTransIns As String = String.Format("Select * From [apx_serial] Where serial={0} and SKU={1}", Made4Net.Shared.FormatField(serial), Made4Net.Shared.FormatField(SKU))
                'Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, RetDtLines)
                'If RetDtLines.Rows.Count > 0 Then
                'Dim updateQuery As String
                'updateQuery = String.Format("update [apx_serial] Set dispatched={0}, loadID={1}, SKU={2}, ASNID="", orderID="", userid={3}, serialweight={4} where serial={5}", Made4Net.Shared.FormatField(False), Made4Net.Shared.FormatField(LoadId), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(user), Made4Net.Shared.FormatField(Weight), Made4Net.Shared.FormatField(serial))
                'Made4Net.DataAccess.DataInterface.RunSQL(updateQuery)
                'Else

                'End If

                APXSERIAL_AddSerialToLoad(serial, SKU, LoadId, UOM, user, CDbl(Weight))
            Next
        End If

    End Sub

End Class
