﻿Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

Public Class apexInventory
    Public Shared Function APXINV_CheckActiveReplens(ByVal user As String, ByRef Depositing As Boolean) As Boolean

        Dim sql As String = String.Format("select * from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(user))

        'Dim dtReplen As New DataTable
        'Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        'If dtReplen.Rows.Count <= 0 Then
        '    Depositing = False
        '    Return True
        'Else
        '    Dim x As Integer = 0
        '    While x < dtReplen.Rows.Count
        '        Dim query As String = String.Format("select * from [REPLENISHMENT] where REPLID={0}", Made4Net.Shared.FormatField(dtReplen.Rows(x)("ReplenId")))
        '        Dim DtQuery As New DataTable
        '        Made4Net.DataAccess.DataInterface.FillDataset(query, DtQuery)
        '        If DtQuery.Rows.Count <= 0 Then
        '            APXINV_DequeueReplen(dtReplen.Rows(x)("ReplenId"))
        '        Else
        '            If DtQuery.Rows(0)("STATUS") = "CANCELED" Or DtQuery.Rows(0)("STATUS") = "COMPLETE" Then
        '                APXINV_DequeueReplen(dtReplen.Rows(x)("ReplenId"))
        '            End If
        '        End If
        '    End While
        'End If

        'sql = String.Format("select * from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(user))

        Dim NdtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, NdtReplen)

        If NdtReplen.Rows.Count <= 0 Then
            Depositing = False
            Return True
        Else
            Dim x As Integer = 0
            While x < NdtReplen.Rows.Count And Depositing = False
                If Not IsDBNull(NdtReplen.Rows(x)("STATUS")) Then
                    If NdtReplen.Rows(x)("STATUS") = "DEPOSIT" Then
                        Depositing = True
                    End If
                End If
                x = x + 1
            End While
        End If

        Return False
    End Function

    Public Shared Sub APXINV_UpdateReplenStatus(ByVal ReplenId As String)
        Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_ReplenRdt] Set Status={0} where ReplenId={1}", Made4Net.Shared.FormatField("DEPOSIT"), Made4Net.Shared.FormatField(WMS.Logic.GetCurrentUser), Made4Net.Shared.FormatField(ReplenId))
        Try
            Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub APXINV_QueueReplen(ByVal User As String, ByVal Replen As String, ByVal Units As Integer)
        Try
            Dim strUnverSerTransIns As String = "INSERT INTO [apx_ReplenRdt]([UserId],[ReplenId],[Qty],[Status])" & _
                                              "VALUES('" & User & "','" & Replen & "', " & Units & ",'NEW')"
            Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)

            Try
                strUnverSerTransIns = String.Format("UPDATE [TASKS] Set ASSIGNED=1, STATUS={0}, USERID={1} where REPLENISHMENT={2}", Made4Net.Shared.FormatField("ASSIGNED"), Made4Net.Shared.FormatField(WMS.Logic.GetCurrentUser), Made4Net.Shared.FormatField(Replen))
                Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
            Catch ex As Exception
            End Try
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub APXINV_DequeueReplen(ByVal ReplenId As String)
        Try
            Dim sql As String = String.Format("delete from apx_ReplenRdt where UserId={0} and ReplenId={1}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(ReplenId))
            Made4Net.DataAccess.DataInterface.RunSQL(sql)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXINV_FindReplenByUser(ByVal User As String) As DataTable
        Dim sql As String = String.Format("select * from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(User))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        Return dtReplen
    End Function

    Public Shared Function APXINV_GetReplenToLoad(ByVal ToLocation As String) As String
        Dim LoadId As String = ""
        Dim sql As String = String.Format("select * from LOADS where LOCATION={0}", Made4Net.Shared.FormatField(ToLocation))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        If dtReplen.Rows.Count >= 1 Then
            LoadId = dtReplen.Rows(0)("LOADID")
        End If

        Return LoadId
    End Function

    Public Shared Function APXINV_ScanSerialAtDeposit() As Boolean

        Dim Value As String = "NO"
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ScanSerialReplen'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value = "YES" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function APXINV_CheckWHASuitableMultiRePlenProcess(ByVal WH_area As String) As Boolean
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReplenPickFaceKey'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)


        Dim whas As String() = Value.Split(New Char() {","c})

        ' Use For Each loop over words and display them

        Dim wha As String
        For Each wha In whas
            If WH_area = wha Then
                Return True
            End If
        Next

        Return False
    End Function
End Class
